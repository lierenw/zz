# ![logo](https://www.zframeworks.com/img/web/logo_w300.png)
### 平台简述 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Z平台是开源免费的JAVA快速开发平台，并且承诺永久开源免费。通过Z平台集成开发环境，以零编码、动态配置的方式能够快速开发BS管理系统。同时该平台还可以做为APP、微信、各种小程序等项目的服务端来使用，为前端项目提供数据接口。并且Z平台也内置了代码生成器组件，可以通过生成代码方式来完成项目的客户化的开发工作。另外，Z平台所用到的各种功能组件与框架，都是开源免费的，不涉及到版权问题，商业与非商业项目都可以放心使用。    

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Z平台是本人利用业余时间开发的，完全出于兴趣爱好，在不影响本人正常工作的情况下，会定期进行迭代升级。在平台使用过程中，如果您有些什么建议，欢迎您到Z平台QQ群与我交流。谢谢！【交流QQ群：685040231】

### 官方网站
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://www.zframeworks.com  

### 帮助文档
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://blog.csdn.net/qq_38056435/article/details/103386195  

### 视频教程
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://edu.csdn.net/course/detail/29220  

### 平台价值
一、提升软件开发速度，缩短软件开发周期。

二、降低软件开发BUG率，缩短软件测试周期。

三、降低项目所需高级开发人员比例，减少项目用工成本支出。
### 平台特点
一、永久开源免费，Z平台为开源免费项目，可以应用在所有商业或非商业项目中进行使用。   

二、学习成本低，Z平台所使用的框架都是热门的开源框架。学习资料丰富。核心框架为Spring + SpringMVC + Mybatis组成。    

三、技术成熟稳定，Z平台所应用的基础框架都是经过长时间沉淀成熟稳定的开源框架。在稳定性方面值得信赖。    