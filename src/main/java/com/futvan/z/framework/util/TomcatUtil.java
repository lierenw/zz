package com.futvan.z.framework.util;

import java.io.File;

import org.apache.catalina.startup.Bootstrap;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;

/**
 * Tomcat工具类
 * @author zj
 *
 */
public class TomcatUtil {

	/**
	 * 启动Tomcat
	 * @param tomcat_path
	 * @return
	 */
	public static Result startup(File tomcat_path) {
		Result result = new Result();
		try {
			Bootstrap bs = new Bootstrap();
			bs.start();
		} catch (Exception e) {
			result.setCode(Code.ERROR);
			result.setMsg("Tomcat Startup Error|"+e.getMessage());
		}
		return result;
	}
	
	/**
	 * 关闭Tomcat
	 * @param tomcat_path
	 * @return
	 */
	public static Result shutdown(File tomcat_path) {
		Result result = new Result();
		try {
			Bootstrap bs = new Bootstrap();
			bs.stop();
		} catch (Exception e) {
			result.setCode(Code.ERROR);
			result.setMsg("Tomcat shutdown Error|"+e.getMessage());
		}
		return result;
	}
	
}
