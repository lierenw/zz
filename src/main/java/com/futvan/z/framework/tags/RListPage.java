package com.futvan.z.framework.tags;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.beanutils.BeanUtils;

import com.alibaba.fastjson.JSONArray;
import com.futvan.z.framework.common.bean.QueryTerms;
import com.futvan.z.system.zform.z_form_table;
import com.futvan.z.system.zform.z_form_table_button;
import com.futvan.z.system.zform.z_form_table_column;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.SuperTag;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.system.zreport.z_report;
import com.futvan.z.system.zreport.z_report_button;
import com.futvan.z.system.zreport.z_report_column;

public class RListPage<E> extends SuperTag{
	private List<HashMap<String,String>> list;
	private HashMap<String,String> parameter;
	private String userid;

	@Override
	public int doEndTag() throws JspException {
		try {
			StringBuffer out_html = new StringBuffer();
			//获取报表ID
			String zid = parameter.get("zid");
			z_report r = z.reports.get(zid);
			List<z_report_column> columnList = r.getZ_report_column_list();
			List<z_report_button> buttonList = r.getZ_report_button_list();

			//创建功能代码
			int buttonCount = 0;
			StringBuffer buttonHTML = new StringBuffer();
			buttonHTML.append("<div data-options=\"region:'north'\" class=\"border-top-0 border-right-0 border-left-0\" style=\"height:"+getNorthHeight(parameter.get("is_mobile"))+"px;overflow:hidden;\">").append("\r\n");
//			buttonHTML.append("<div data-options=\"region:'north',border:false\" style=\"height:38px;overflow:hidden\">").append("\r\n");
			
			buttonHTML.append("<div id=\"ButtonHR\" class=\"btn-group\" role=\"group\">").append("\r\n");
			
			//如果是手机访问列表页面，添加返回按钮
			if("1".equals(parameter.get("is_mobile"))) {
				String return_mobile_view = parameter.get("return_mobile_view");
				if(z.isNull(return_mobile_view)) {
					return_mobile_view = "nav_mobile";
				}
				buttonHTML.append("<button id=\"return_nav_mobile_button\" type=\"button\" class=\"btn btn-light\" onclick=\"window.location.href='"+return_mobile_view+"';\"><i class=\"fa fa-hand-o-left\"></i>&nbsp;返回</button>").append("\r\n");
			}
			
			for (z_report_button button : buttonList) {
				if(!"1".equals(button.getIs_hidden())  || userid.equals(z.sp.get("super_user"))) {
					String buttonId = button.getZid();
					//判读用户是否有该按钮权限
					String isAvailable = z.UserReportButtons.get(userid+buttonId);
					if(z.isNotNull(isAvailable) || userid.equals(z.sp.get("super_user"))) {
						//判读是否是移动端
						if("1".equals(parameter.get("is_mobile"))) {
							//不是打印按钮
							if(button.getButton_id().indexOf("_print_list_button")<0) {
								
								//如果，是查询按钮，并且启动了快速查询框，就不显示查询按钮
								if(button.getButton_id().indexOf("select_button")>=0 && "1".equals(r.getIsStartQuickQuery())) {
									
								}else {
									buttonCount = buttonCount+1;
									buttonHTML.append("<button id=\""+button.getButton_id()+"\" type=\"button\" class=\"btn btn-light\" onclick=\""+button.getButton_id()+"();\"><i class=\""+button.getButton_icon()+"\"></i> "+button.getButton_name()+"</button>").append("\r\n");
								}
								
							}
						}else {
							//如果，是查询按钮，并且启动了快速查询框，就不显示查询按钮
							if(button.getButton_id().indexOf("select_button")>=0 && "1".equals(r.getIsStartQuickQuery())) {
								
							}else {
								buttonCount = buttonCount+1;
								buttonHTML.append("<button id=\""+button.getButton_id()+"\" type=\"button\" class=\"btn btn-light\" onclick=\""+button.getButton_id()+"();\"><i class=\""+button.getButton_icon()+"\"></i> "+button.getButton_name()+"</button>").append("\r\n");
							
							}
						}
					}
				}
			}
			buttonHTML.append("</div>").append("\r\n");
			
			//创建ListSearchBoxForm + 设置快速查询框信息
			CreateListQueryFormR(buttonHTML,columnList,"rlist",r,parameter);
			
			buttonHTML.append("</div>").append("\r\n");
			out_html.append(buttonHTML);

			//创建查询域名
			CreateSelectHtmlR("rlist",parameter,out_html,r,columnList);

			//创建表格代码
			out_html.append("<div data-options=\"region:'center',border:false\">").append("\r\n");
			CreateTableHtmlR(out_html,columnList,list,parameter);
			out_html.append("</div>").append("\r\n");

			//创建分页代码
			out_html.append("<div data-options=\"region:'south',border:false\" style=\"height:40px;overflow:hidden\">").append("\r\n");
			CreatePagingHtml("list",parameter,out_html);
			out_html.append("</div>").append("\r\n");
			
			//创建JS方法
			CreateJSR(out_html,buttonList,userid);
			
			//输出HTML
			outHTML(out_html.toString());
		} catch (Exception e) {
			z.Error("RListPage:自定义标签构建错误：", e);
		}
		return super.doEndTag();
	}

	

	public List<HashMap<String, String>> getList() {
		return list;
	}

	public void setList(List<HashMap<String, String>> list) {
		this.list = list;
	}

	public HashMap<String, String> getParameter() {
		return parameter;
	}

	public void setParameter(HashMap<String, String> parameter) {
		this.parameter = parameter;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}

}
