package com.futvan.z.framework.common.bean;

import java.util.List;
/**
 * Java类对象
 * @author zj
 */
public class JavaClass {
	private String className;//类名
	private String extendsClass;//继承类
	private String title;//中文注释
	private String packageName;//包名
	private String prefix;//前缀
	private String filePath;//类文件目录
	private List<String> importList;//引用列表
	private List<JavaVariable> variableList;//成员变量列表
	private List<JavaMethod> methodList;//方法列表
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getExtendsClass() {
		return extendsClass;
	}
	public void setExtendsClass(String extendsClass) {
		this.extendsClass = extendsClass;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public List<String> getImportList() {
		return importList;
	}
	public void setImportList(List<String> importList) {
		this.importList = importList;
	}
	public List<JavaVariable> getVariableList() {
		return variableList;
	}
	public void setVariableList(List<JavaVariable> variableList) {
		this.variableList = variableList;
	}
	public List<JavaMethod> getMethodList() {
		return methodList;
	}
	public void setMethodList(List<JavaMethod> methodList) {
		this.methodList = methodList;
	}
	
}