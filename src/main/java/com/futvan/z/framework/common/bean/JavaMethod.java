package com.futvan.z.framework.common.bean;

import java.util.List;
/**
 * JAVA方法对象
 * @author zz
 *
 */
public class JavaMethod {
	private String modifier = "public";//修饰符 
	private String prefix;//前缀
	private String name;//方法名称
	private String isStatic = "";//也可以设置为static
	private String MethodType = "void";//也可以设置为返回类型
	private List<JavaMethodParameter> parameterList;//参数列表
	private String code;//方法代码
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getIsStatic() {
		return isStatic;
	}
	public void setIsStatic(String isStatic) {
		this.isStatic = isStatic;
	}
	public String getMethodType() {
		return MethodType;
	}
	public void setMethodType(String methodType) {
		MethodType = methodType;
	}
	public List<JavaMethodParameter> getParameterList() {
		return parameterList;
	}
	public void setParameterList(List<JavaMethodParameter> parameterList) {
		this.parameterList = parameterList;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}
