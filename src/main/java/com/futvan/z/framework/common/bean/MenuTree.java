package com.futvan.z.framework.common.bean;

import java.util.List;

import com.futvan.z.framework.core.SuperBean;
/**
 * 菜单树对象
 * @author zz
 *
 */
public class MenuTree extends SuperBean{
	private String text;
	private String iconCls;
	private String state;
	private String url;
	private List<MenuTree> children;
	/**
	 * @return text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text 要设置的 text
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return iconCls
	 */
	public String getIconCls() {
		return iconCls;
	}
	/**
	 * @param iconCls 要设置的 iconCls
	 */
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
	/**
	 * @return state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state 要设置的 state
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url 要设置的 url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return children
	 */
	public List<MenuTree> getChildren() {
		return children;
	}
	/**
	 * @param children 要设置的 children
	 */
	public void setChildren(List<MenuTree> children) {
		this.children = children;
	}

}
