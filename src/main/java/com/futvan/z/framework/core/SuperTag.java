package com.futvan.z.framework.core;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.session.SqlSession;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.futvan.z.framework.common.bean.QueryTerms;
import com.futvan.z.system.zcode.z_code;
import com.futvan.z.system.zcode.z_code_detail;
import com.futvan.z.system.zformquery.z_form_query;
import com.futvan.z.system.zreport.z_report;
import com.futvan.z.system.zreport.z_report_button;
import com.futvan.z.system.zreport.z_report_column;
import com.futvan.z.system.zworkflow.z_workflow;
import com.futvan.z.system.zform.z_form;
import com.futvan.z.system.zform.z_form_table;
import com.futvan.z.system.zform.z_form_table_button;
import com.futvan.z.system.zform.z_form_table_column;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.framework.util.JsonUtil;
import com.futvan.z.framework.util.SpringUtil;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.framework.util.SystemUtil;
public class SuperTag extends BodyTagSupport{

	/**
	 * 列表页面创建 列表卡片组
	 * @param out_html
	 * @param columnList
	 * @param parameter2
	 * @param table
	 * @param userid 
	 * @param list 
	 * @throws Exception 
	 */
	protected void CreateCardGroup(StringBuffer out_html, List<z_form_table_column> columnList,
			HashMap<String, String> parameter, z_form_table table, String userid, List<HashMap<String, String>> list) throws Exception {

		out_html.append(" <div class=\"container-fluid\"> ").append("\r\n");
		out_html.append(" <div class=\"row\"> ").append("\r\n");

		//遍历所有记录行
		for (HashMap<String,String> bean : list) {
			//如果卡片大小为空，默认为4号
			if(z.isNotNull(table.getCard_size())) {
				out_html.append(" <div class=\"col-"+table.getCard_size()+" padding-bottom-20\"> ").append("\r\n");
			}else {
				out_html.append(" <div class=\"col-4 padding-bottom-20\"> ").append("\r\n");
			}
			//卡片头
			out_html.append(" <div class=\"card\"> ").append("\r\n");

			//生成按钮行
			out_html.append(" <div class=\"card-header text-right\" style=\"padding: 3px;\"> ").append("\r\n");
			out_html.append(" <div id=\"ButtonHR\" class=\"btn-group\" role=\"group\"> ").append("\r\n");
			for (z_form_table_button button : table.getZ_form_table_button_list()) {
				if("list_card".equals(button.getPage_type())) {
					//按钮未隐藏，或当前用户为管理员
					if(!"1".equals(button.getIs_hidden()) || userid.equals(z.sp.get("super_user"))) {
						String buttonId = button.getZid();
						//判读用户是否有该按钮权限
						String isAvailable = z.UserFunctionButtons.get(userid+buttonId);
						if(z.isNotNull(isAvailable) || userid.equals(z.sp.get("super_user"))) {
							out_html.append(" <button id=\""+button.getButton_id()+"\" value="+String.valueOf(bean.get("zid"))+" type=\"button\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'"+button.getButton_icon()+"'\"  onclick=\""+button.getButton_id()+"(this);\">"+button.getButton_name()+"</button>").append("\r\n");
						}
					}
				}
			}
			out_html.append(" </div> ").append("\r\n");
			out_html.append(" </div> ").append("\r\n");

			//生成表单控件
			CreateCardGroupForm(out_html,columnList,bean,table,userid,list);

			//卡片头 div 闭合标签
			out_html.append(" </div> ").append("\r\n");

			//col-？ div 闭合标签
			out_html.append(" </div> ").append("\r\n");
		}

		//row div 闭合标签
		out_html.append(" </div> ").append("\r\n");
		//container-fluid div 闭合标签
		out_html.append(" </div> ").append("\r\n");
	}

	/**
	 * 生成卡片内容
	 * @param out_html
	 * @param columnList
	 * @param bean 
	 * @param parameter
	 * @param table
	 * @param userid
	 * @param list 
	 * @throws Exception 
	 */
	private void CreateCardGroupForm(StringBuffer out_html, List<z_form_table_column> columnList, HashMap<String, String> bean, z_form_table table, String userid, List<HashMap<String, String>> list) throws Exception {
		out_html.append(" <div class=\"card-body\"> ").append("\r\n");
		out_html.append(" <form id=\""+String.valueOf(bean.get("zid"))+"_form\" enctype=\"multipart/form-data\"> ").append("\r\n");
		out_html.append(" <div class=\"container-fluid\"><div class=\"row\"> ").append("\r\n");
		String PageType = "list_card";
		//遍历所有字段
		for (z_form_table_column column : table.getZ_form_table_column_list()) {

			//判读是否主键或外键
			if(!"zid".equals(column.getColumn_id()) && !"pid".equals(column.getColumn_id()))
				//判读字段是否隐藏
				if("0".equals(column.getIs_hidden())) {//判读是否隐藏
					if(!"1".equals(column.getIs_hidden_list())){//判读是否列表隐藏
						//创建字段
						out_html.append("<div class=\"col-12\">").append("\r\n");
						out_html.append("<fieldset class=\"form-group\">").append("\r\n");

						//添加标题
						out_html.append("<label data-toggle=\"tooltip\" class=\"margin-bottom: 0px;"+ColumnIsNull(column.getIs_null())+"\" data-placement=\"top\" title=\""+column.getColumn_help()+"\">【"+column.getColumn_name()+"】</label>").append("\r\n");

						if("0".equals(column.getColumn_type())) {//文本
							out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(bean,column.getColumn_id(),PageType)+"\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
						}else if("1".equals(column.getColumn_type())) {//多行文本
							String textarea_height = "300";
							if(column.getTextarea_height()!=null && !"".equals(column.getTextarea_height())) {
								textarea_height = column.getTextarea_height();
							}
							out_html.append("<textarea placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" style=\"height: "+textarea_height+"px\" class=\"form-control\" "+ColumnIsReadonly(column.getIs_readonly())+">"+getColumnValue(bean,column.getColumn_id(),PageType)+"</textarea>").append("\r\n");
						}else if("2".equals(column.getColumn_type())) {//数字
							out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"number\" class=\"form-control\" value=\""+getColumnValue(bean,column.getColumn_id(),PageType)+"\"  "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
						}else if("3".equals(column.getColumn_type())) {//文件
							out_html.append("<div class=\"input-group\">").append("\r\n");
							String ColumnValue = getColumnValue(bean,column.getColumn_id(),PageType);
							out_html.append("<input  placeholder=\""+column.getColumn_help()+"\"  id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"url\" class=\"form-control marginleft1\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
							out_html.append("<span class=\"input-group-btn btn-group btn-group-sm marginright1\">").append("\r\n");
							out_html.append("<button class=\"btn btn-light\" onclick=\"uploadFile('"+column.getColumn_id()+"','"+bean.get("zid")+"')\" type=\"button\"><i class=\"fa fa-upload\"></i></button>").append("\r\n");
							out_html.append("<button class=\"btn btn-light\" onclick=\"downloadFile('"+column.getColumn_id()+"')\" type=\"button\"><i class=\"fa fa-download\"></i></button>").append("\r\n");
							out_html.append("</span>").append("\r\n");
							out_html.append("</div>").append("\r\n");
						}else if("4".equals(column.getColumn_type())) {//图片
							out_html.append("<div class=\"input-group\">").append("\r\n");
							String ColumnValue = getColumnValue(bean,column.getColumn_id(),PageType);
							out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"url\" class=\"form-control marginleft1\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
							out_html.append("<span class=\"input-group-btn btn-group btn-group-sm marginright1\">").append("\r\n");
							out_html.append("<button class=\"btn btn-light\" onclick=\"uploadFile('"+column.getColumn_id()+"','"+bean.get("zid")+"')\" type=\"button\"><i class=\"fa fa-upload\"></i></button>").append("\r\n");
							out_html.append("<button class=\"btn btn-light\" onclick=\"lookupImg('"+column.getColumn_id()+"')\" type=\"button\"><i class=\"fa fa-picture-o\"></i></button>").append("\r\n");
							out_html.append("</span>").append("\r\n");
							out_html.append("</div>").append("\r\n");
						}else if("5".equals(column.getColumn_type())) {//多选
							//获取Code
							if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
								z_code code = z.code.get(column.getP_code_id());
								if(code!=null && code.getZ_code_detail_list().size()>0) {
									List<z_code_detail> delailList = code.getZ_code_detail_list();
									out_html.append("<div class=\"zcheckbox_div\">").append("\r\n");
									String value = getColumnValue(bean,column.getColumn_id(),PageType);
									out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+value+"\"/>").append("\r\n");
									for (z_code_detail code_d : delailList) {
										boolean ic = isChecked(value,code_d.getZ_key());
										if(ic) {
											out_html.append("<div class=\""+GetArrangementDirectionCss(column.getArrangement_direction())+"\"><input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_checked_id')\" type=\"checkbox\" checked=\"checked\" id=\""+column.getColumn_id()+"_checked_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(column.getIs_readonly())+"> "+code_d.getZ_value()+" </div>").append("\r\n");
										}else {
											out_html.append("<div class=\""+GetArrangementDirectionCss(column.getArrangement_direction())+"\"><input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_checked_id')\" type=\"checkbox\"                     id=\""+column.getColumn_id()+"_checked_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(column.getIs_readonly())+"> "+code_d.getZ_value()+" </div>").append("\r\n");
										}
									}
									out_html.append("</div>").append("\r\n");
								}
							}
						}else if("6".equals(column.getColumn_type())) {//单选
							//获取Code
							if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
								z_code code = z.code.get(column.getP_code_id());
								if(code!=null && code.getZ_code_detail_list().size()>0) {
									List<z_code_detail> delailList = code.getZ_code_detail_list();
									out_html.append("<div class=\"zcheckbox_div\">").append("\r\n");
									for (z_code_detail code_d : delailList) {
										String value = getColumnValue(bean,column.getColumn_id(),PageType);
										if(value.equals(code_d.getZ_key())) {
											out_html.append("<div class=\""+GetArrangementDirectionCss(column.getArrangement_direction())+"\"><input class=\"zcheckbox\" type=\"radio\" checked=\"checked\" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(column.getIs_readonly())+"> "+code_d.getZ_value()+" </div>").append("\r\n");
										}else {
											out_html.append("<div class=\""+GetArrangementDirectionCss(column.getArrangement_direction())+"\"><input class=\"zcheckbox\" type=\"radio\" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(column.getIs_readonly())+"> "+code_d.getZ_value()+" </div>").append("\r\n");
										}

									}
									out_html.append("</div>").append("\r\n");
								}

							}
						}else if("7".equals(column.getColumn_type())) {//下拉框
							out_html.append("<select class=\"form-control\" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" "+ColumnIsReadonly(column.getIs_readonly())+">").append("\r\n");
							out_html.append("<option value=\"\">请选择</option>").append("\r\n");
							//获取Code
							if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
								z_code code = z.code.get(column.getP_code_id());
								if(code!=null && code.getZ_code_detail_list().size()>0) {
									List<z_code_detail> delailList = code.getZ_code_detail_list();
									for (z_code_detail code_d : delailList) {
										String value = getColumnValue(bean,column.getColumn_id(),PageType);
										if(value.equals(code_d.getZ_key())) {
											out_html.append("<option value=\""+code_d.getZ_key()+"\" selected=\"selected\">"+code_d.getZ_value()+"</option>").append("\r\n");
										}else {
											out_html.append("<option value=\""+code_d.getZ_key()+"\" >"+code_d.getZ_value()+"</option>").append("\r\n");
										}
									}
								}
							}
							out_html.append("</select>").append("\r\n");
						}else if("8".equals(column.getColumn_type())) {//Z5
							out_html.append("<div class=\"input-group\">").append("\r\n");
							String ColumnValue = getColumnValue(bean,column.getColumn_id(),PageType);
							String SelectDataOnClick = "onclick=\"Z5list('"+table.getTable_id()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display',0)\"";
							//如果字段是只读，删除输入框点击事件
							if("1".equals(column.getIs_readonly())) {
								SelectDataOnClick = "";
							}
							out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+ColumnValue+"\"/>").append("\r\n");
							out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_display\" "+SelectDataOnClick+" name=\""+column.getColumn_id()+"_display\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"text\" class=\"form-control marginleft1\" "+" readonly />").append("\r\n");
							out_html.append("<span class=\"input-group-prepend marginright1\">").append("\r\n");
							out_html.append("<button class=\"btn btn-light\" onclick=\"Z5Clear('"+table.getTable_id()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display')\" type=\"button\" "+ColumnIsDisabled(column.getIs_readonly())+"><i class=\"fa fa-trash-o\"></i></button>").append("\r\n");
							out_html.append("<button id=\""+column.getColumn_id()+"_selectbutton_id\" class=\"btn btn-light\" "+SelectDataOnClick+" type=\"button\" "+ColumnIsDisabled(column.getIs_readonly())+"><i class=\"fa fa-search\"></i></button>").append("\r\n");
							out_html.append("</span>").append("\r\n");
							out_html.append("</div>").append("\r\n");

						}else if("9".equals(column.getColumn_type())) {//日期
							if("1".equals(column.getIs_readonly())) {
								//只读
								out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(bean,column.getColumn_id(),PageType)+"\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
							}else {
								//可以
								out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+getColumnValue(bean,column.getColumn_id(),PageType)+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
							}

						}else if("10".equals(column.getColumn_type())) {//日期时间
							if("1".equals(column.getIs_readonly())) {
								//只读
								out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(bean,column.getColumn_id(),PageType)+"\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
							}else {
								//可以
								out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+getColumnValue(bean,column.getColumn_id(),PageType)+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
							}

						}else if("11".equals(column.getColumn_type())) {//HTML输入框
							String textarea_height = "300";
							if(column.getTextarea_height()!=null && !"".equals(column.getTextarea_height())) {
								textarea_height = column.getTextarea_height();
							}
							out_html.append("<textarea  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" style=\"height: "+textarea_height+"px\" "+ColumnIsReadonly(column.getIs_readonly())+">"+getColumnValue(bean,column.getColumn_id(),PageType)+"</textarea>").append("\r\n");
							out_html.append("<script type=\"text/javascript\">InitHTMLColumn('"+column.getColumn_id()+"_id');</script>").append("\r\n");
						}else if("12".equals(column.getColumn_type())) {//源码输入框
							String textarea_height = "300";
							if(column.getTextarea_height()!=null && !"".equals(column.getTextarea_height())) {
								textarea_height = column.getTextarea_height();
							}
							out_html.append("<textarea  placeholder=\""+column.getColumn_help()+"\"  id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" style=\"height: "+textarea_height+"px\" "+ColumnIsReadonly(column.getIs_readonly())+">"+getColumnValue(bean,column.getColumn_id(),PageType)+"</textarea>").append("\r\n");
							out_html.append("<script type=\"text/javascript\">InitCodeColumn('"+column.getColumn_id()+"_id','"+column.getMode_type()+"','"+textarea_height+"');</script>").append("\r\n");
						}


						out_html.append("</fieldset>").append("\r\n");
						out_html.append("</div>").append("\r\n");
					}
				}
		}

		out_html.append(" </div></div> ").append("\r\n");
		out_html.append(" </form> ").append("\r\n");
		out_html.append(" </div> ").append("\r\n");
	}

	/**
	 * 获取多选框值[表单]
	 * @param column 字段
	 * @param string values
	 * @return
	 */
	public String CheckedValue(z_form_table_column column, String values) {
		String returnvalue = "|";
		if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
			z_code code = z.code.get(column.getP_code_id());
			if(code!=null && code.getZ_code_detail_list().size()>0) {
				List<z_code_detail> delailList = code.getZ_code_detail_list();
				for (z_code_detail code_d : delailList) {
					if(isChecked(values,code_d.getZ_key())) {
						//判读是否有自定义颜色设置
						if(z.isNotNull(code_d.getDisplay_color())) {
							returnvalue = returnvalue + "<span style=\"color:#"+code_d.getDisplay_color()+";\">" +code_d.getZ_value()+"</span>|";
						}else {
							returnvalue = returnvalue + code_d.getZ_value()+"|";
						}

					}
				}
			}
		}
		//去掉结尾|
		if(returnvalue.lastIndexOf("|")==returnvalue.length()) {
			returnvalue = returnvalue.substring(0,returnvalue.length()-1);
		}
		return returnvalue;
	}

	/**
	 * 获取多选框值[报表]
	 * @param column 字段
	 * @param string values
	 * @return
	 */
	public String CheckedValueR(z_report_column column, String values) {
		String returnvalue = " | ";
		if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
			z_code code = z.code.get(column.getP_code_id());
			if(code!=null && code.getZ_code_detail_list().size()>0) {
				List<z_code_detail> delailList = code.getZ_code_detail_list();
				for (z_code_detail code_d : delailList) {
					if(isChecked(values,code_d.getZ_key())) {
						returnvalue = returnvalue + code_d.getZ_value()+" | ";
					}
				}
			}
		}
		return returnvalue;
	}

	/**
	 * 判读多选框是否选中
	 * @param values
	 * @param z_key
	 * @return
	 */
	public boolean isChecked(String values, String z_key) {
		boolean returnvalue = false;
		//值转为数据
		String[] valuearray = values.split(",");
		for (int i = 0; i < valuearray.length; i++) {
			String v = valuearray[i];
			//判读数组中每个值是否选择
			if(v.equals(z_key)) {
				returnvalue = true;
			}
		}
		return returnvalue;
	}

	/**
	 * 获取Z5显示值【表单】
	 * @param table_id
	 * @param column_id
	 * @param columnValue
	 * @return
	 */
	public String getZ5DisplayValue(z_form_table_column column, String columnValue) {
		if(!"".equals(column.getZ5_table()) && column.getZ5_table()!=null && !"".equals(column.getZ5_key()) && column.getZ5_table()!=null && !"".equals(columnValue) && columnValue!=null) {
			String key = column.getZ5_table()+column.getZ5_key()+columnValue;
			String DisplayValue = z.Z5DisplayValue.get(key);
			if(!"".equals(DisplayValue) && DisplayValue!=null) {
				return DisplayValue;
			}else {

				//判读是否有辅助显示字段
				if(z.isNotNull(column.getZ5_value2())) {
					//有辅助显示字段
					String sql = "select concat(IFNULL("+column.getZ5_value()+",''),'|',IFNULL("+column.getZ5_value2()+",'')) z5value from "+column.getZ5_table()+" where "+column.getZ5_key() +" = '"+columnValue+"'";
					List<String> list = z.getSqlSession().selectList("selectone", sql);
					if(list.size()>0) {
						String DValue = list.get(0);
						if(z.isNotNull(DValue) && "|".equals(DValue.substring(DValue.length()-1))) {
							DValue = DValue.substring(0, DValue.length()-1);
						}else {
							//如果辅助字段需要关联其他表，在这处理该信息
							z_form_table_column target_column = z.columns.get(column.getZ5_table()+"_"+column.getZ5_value2());
							if(z.isNotNull(target_column)) {
								String [] DValueArray = DValue.split("\\|");
								if(DValueArray.length==2 && z.isNotNull(DValueArray[1])) {
									String DValue_key = DValueArray[1];
									if("5".equals(target_column.getColumn_type())) {//多选
										String CheckedValue = CheckedValue(target_column,DValue_key);
										DValue =  DValueArray[0]+"|"+CheckedValue;
									}else if("6".equals(target_column.getColumn_type())) {//单选
										String cv = z.codeValue.get(target_column.getP_code_id()+"_"+DValue_key);
										DValue =  DValueArray[0]+"|"+cv;
									}else if("7".equals(target_column.getColumn_type())) {//下拉框
										String cv = z.codeValue.get(target_column.getP_code_id()+"_"+DValue_key);
										DValue =  DValueArray[0]+"|"+cv;
									}else if("8".equals(target_column.getColumn_type())) {//Z5
										String target_z5_sql = "select "+target_column.getZ5_value() +" from "+target_column.getZ5_table()+" where "+target_column.getZ5_key() +" = '"+DValue_key+"'";
										List<String> target_z5_list = z.getSqlSession().selectList("selectone", target_z5_sql);
										if(target_z5_list.size()>0) {
											DValue =  DValueArray[0]+"|"+target_z5_list.get(0);
										}else {
											DValue =  DValueArray[0];
										}
									}
								}
							}
						}
						z.Z5DisplayValue.put(key, DValue);
						return DValue;
					}else {
						return columnValue;
					}

				}else {
					//无辅助显示
					String sql = "select "+column.getZ5_value() +" from "+column.getZ5_table()+" where "+column.getZ5_key() +" = '"+columnValue+"'";
					List<String> list = z.getSqlSession().selectList("selectone", sql);
					if(list.size()>0) {
						String DValue = list.get(0);
						z.Z5DisplayValue.put(key, DValue);
						return DValue;
					}else {
						return columnValue;
					}
				}



			}
		}else {
			return columnValue;
		}


	}

	/**
	 * 获取Z5显示值【表单】
	 * @param table_id
	 * @param column_id
	 * @param columnValue
	 * @return
	 */
	public String getZ5DisplayValue(String z5_table,String z5_key,String z5_value, String columnValue) {
		z_form_table_column column = new z_form_table_column();
		column.setZ5_table(z5_table);
		column.setZ5_key(z5_key);
		column.setZ5_value(z5_value);
		return getZ5DisplayValue(column,columnValue);
	}

	/**
	 * 获取Z5显示值【报表】
	 * @param table_id
	 * @param column_id
	 * @param columnValue
	 * @return
	 */
	public String getZ5DisplayValueR(z_report_column column, String columnValue) {
		if(!"".equals(column.getZ5_table()) && column.getZ5_table()!=null && !"".equals(column.getZ5_key()) && column.getZ5_table()!=null && !"".equals(columnValue) && columnValue!=null) {
			String key = column.getZ5_table()+column.getZ5_key()+columnValue;
			String DisplayValue = z.Z5DisplayValue.get(key);
			if(!"".equals(DisplayValue) && DisplayValue!=null) {
				return DisplayValue;
			}else {
				//判读是否有辅助显示字段
				if(z.isNotNull(column.getZ5_value2())) {
					//有辅助显示字段
					String sql = "select concat(IFNULL("+column.getZ5_value()+",''),'|',IFNULL("+column.getZ5_value2()+",'')) z5value from "+column.getZ5_table()+" where "+column.getZ5_key() +" = '"+columnValue+"'";
					List<String> list = z.getSqlSession().selectList("selectone", sql);
					if(list.size()>0) {
						String DValue = list.get(0);
						if(z.isNotNull(DValue) && "|".equals(DValue.substring(DValue.length()-1))) {
							DValue = DValue.substring(0, DValue.length()-1);
						}else {
							//如果辅助字段需要关联其他表，在这处理该信息
							z_form_table_column target_column = z.columns.get(column.getZ5_table()+"_"+column.getZ5_value2());
							if(z.isNotNull(target_column)) {
								String [] DValueArray = DValue.split("\\|");
								if(DValueArray.length==2 && z.isNotNull(DValueArray[1])) {
									String DValue_key = DValueArray[1];
									if("5".equals(target_column.getColumn_type())) {//多选
										String CheckedValue = CheckedValue(target_column,DValue_key);
										DValue =  DValueArray[0]+"|"+CheckedValue;
									}else if("6".equals(target_column.getColumn_type())) {//单选
										String cv = z.codeValue.get(target_column.getP_code_id()+"_"+DValue_key);
										DValue =  DValueArray[0]+"|"+cv;
									}else if("7".equals(target_column.getColumn_type())) {//下拉框
										String cv = z.codeValue.get(target_column.getP_code_id()+"_"+DValue_key);
										DValue =  DValueArray[0]+"|"+cv;
									}else if("8".equals(target_column.getColumn_type())) {//Z5
										String target_z5_sql = "select "+target_column.getZ5_value() +" from "+target_column.getZ5_table()+" where "+target_column.getZ5_key() +" = '"+DValue_key+"'";
										List<String> target_z5_list = z.getSqlSession().selectList("selectone", target_z5_sql);
										if(target_z5_list.size()>0) {
											DValue =  DValueArray[0]+"|"+target_z5_list.get(0);
										}else {
											DValue =  DValueArray[0];
										}
									}
								}
							}
						}
						z.Z5DisplayValue.put(key, DValue);
						return DValue;
					}else {
						return columnValue;
					}
				}else {
					//无辅助显示
					String sql = "select "+column.getZ5_value() +" from "+column.getZ5_table()+" where "+column.getZ5_key() +" = '"+columnValue+"'";
					List<String> list = z.getSqlSession().selectList("selectone", sql);
					if(list.size()>0) {
						String DValue = list.get(0);
						z.Z5DisplayValue.put(key, DValue);
						return DValue;
					}else {
						return columnValue;
					}
				}


			}
		}else {
			return columnValue;
		}


	}

	/**
	 * 创建JS方法
	 * @param out_html
	 * @param pageType edit and list
	 * @param table
	 * @param userid 
	 */
	protected void CreateJS(StringBuffer out_html, String pageType, z_form_table table,HashMap<String,String> parameter, String userid) {
		out_html.append(" <script type=\"text/javascript\"> ").append("\r\n");

		//Onload
		out_html.append(" $(document).ready(function(){ ").append("\r\n");

		//添加ready默认JS代码
		AddReadyJSCode(out_html,pageType,table,parameter);

		out_html.append(" }); ").append("\r\n");

		//添加默认JS方法
		AddBaseJSCode(out_html,pageType,table,userid);


		out_html.append(" </script> ").append("\r\n");
	}

	//创建Form代码
	protected void CreateForm(StringBuffer out_html,z_form_table table,String PageType,HashMap<String,String> parameter) throws Exception{
		//设置页面结构
		if("1".equals(table.getEdit_page_layout())) {
			//左右结构
			out_html.append("<div title=\"主 表 记 录\" data-options=\"region:'center',border:true\">").append("\r\n");
		}else {
			//上下结构
			out_html.append("<div data-options=\"region:'center',border:false\">").append("\r\n");
		}

		//如果是新增页面，判读是否包含主键
		if("add".equals(PageType) && z.isNull(parameter.get("zid"))) {
			parameter.put("zid", z.newZid(table.getTable_id()));
		}
		//创建FORM
		out_html.append("<form id=\"main_form\" class=\"was-validated\" style=\"padding:10px;\" enctype=\"multipart/form-data\">").append("\r\n");
		out_html.append("<div class=\"container-fluid\">").append("\r\n");
		out_html.append("<div class=\"row\">").append("\r\n");
		//遍历所有字段
		for (z_form_table_column column : table.getZ_form_table_column_list()) {
			//判读是否主键或外键
			if(!"zid".equals(column.getColumn_id()) && !"pid".equals(column.getColumn_id())) {
				//判读字段是否隐藏
				if(!"1".equals(column.getIs_hidden()) && !"1".equals(column.getIs_hidden_edit())) {//判读是否隐藏
					//创建字段
					out_html.append("<div class=\"col-sm-"+column.getColumn_size()+" mb-2 d-flex align-items-start\">").append("\r\n");
					out_html.append("<div class=\"input-group\">").append("\r\n");
					//标题
					out_html.append("<div class=\"input-group-prepend\"><div class=\"input-group-text "+ColumnIsNull(column.getIs_null())+"\">"+column.getColumn_name()+"：</div></div>").append("\r\n");
					if("0".equals(column.getColumn_type())) {//文本
						out_html.append("<input placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
					}else if("1".equals(column.getColumn_type())) {//多行文本
						String textarea_height = "300";
						if(z.isNotNull(column.getTextarea_height())) {
							textarea_height = column.getTextarea_height();
						}
						out_html.append("<textarea placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" style=\"height: "+textarea_height+"px;width:100%;\" class=\"form-control\" "+ColumnIsReadonly(column.getIs_readonly())+">"+getColumnValue(parameter,column.getColumn_id(),PageType)+"</textarea>").append("\r\n");
					}else if("2".equals(column.getColumn_type())) {//数字
						out_html.append("<input placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"number\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\"  "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
					}else if("3".equals(column.getColumn_type())) {//文件
						String ColumnValue = getColumnValue(parameter,column.getColumn_id(),PageType);
						out_html.append("<input placeholder=\""+StringUtil.toString(column.getColumn_help())+"\"  id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"url\" class=\"form-control marginleft1 border-right-0\" readonly=\"readonly\"/>").append("\r\n");
						out_html.append("<span class=\"input-group-prepend marginright1 border-top border-right border-bottom\">").append("\r\n");
						if(!"look".equals(PageType)) {
							out_html.append("<button class=\"btn btn-light\" onclick=\"uploadFile('"+column.getColumn_id()+"','"+parameter.get("zid")+"')\" type=\"button\"><i class=\"fa fa-upload\"></i></button>").append("\r\n");
						}
						out_html.append("<button class=\"btn btn-light\" onclick=\"downloadFile('"+column.getColumn_id()+"')\" type=\"button\"><i class=\"fa fa-download\"></i></button>").append("\r\n");
						if(!"look".equals(PageType)) {
							out_html.append("<button class=\"btn btn-light\" onclick=\"deleteFile('"+column.getColumn_id()+"')\" type=\"button\"><i class=\"fa fa-trash-o\"></i></button>").append("\r\n");
						}
						out_html.append("</span>").append("\r\n");
					}else if("4".equals(column.getColumn_type())) {//图片
						String ColumnValue = getColumnValue(parameter,column.getColumn_id(),PageType);
						out_html.append("<input placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"url\" class=\"form-control marginleft1 border-right-0\" readonly=\"readonly\"/>").append("\r\n");
						out_html.append("<span class=\"input-group-prepend marginright1 border-top border-right border-bottom\">").append("\r\n");
						if(!"look".equals(PageType)) {
							out_html.append("<button class=\"btn btn-light\" onclick=\"uploadFile('"+column.getColumn_id()+"','"+parameter.get("zid")+"')\" type=\"button\"><i class=\"fa fa-upload\"></i></button>").append("\r\n");
						}
						out_html.append("<button class=\"btn btn-light\" onclick=\"lookupImg('"+column.getColumn_id()+"')\" type=\"button\"><i class=\"fa fa-picture-o\"></i></button>").append("\r\n");
						if(!"look".equals(PageType)) {
							out_html.append("<button class=\"btn btn-light\" onclick=\"deleteFile('"+column.getColumn_id()+"')\" type=\"button\"><i class=\"fa fa-trash-o\"></i></button>").append("\r\n");
						}
						out_html.append("</span>").append("\r\n");
					}else if("5".equals(column.getColumn_type())) {//多选
						//获取Code
						if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
							z_code code = z.code.get(column.getP_code_id());
							if(code!=null && code.getZ_code_detail_list().size()>0) {
								List<z_code_detail> delailList = code.getZ_code_detail_list();
								out_html.append("<div class=\"zcheckbox_div\">").append("\r\n");
								String value = getColumnValue(parameter,column.getColumn_id(),PageType);
								out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+value+"\"/>").append("\r\n");
								for (z_code_detail code_d : delailList) {
									//如果字段值与Key相同，选中状态
									boolean ic = isChecked(value,code_d.getZ_key());
									String ischecked = "";
									if(ic) {
										ischecked = "checked=\"checked\"";
									}
									//如果只读换成隐藏域
									String type = "checkbox";
									String hidden_ico = "";
									if("1".equals(column.getIs_readonly())) {
										type = "hidden";
										if(z.isNotNull(ischecked)) {
											hidden_ico = "<i class=\"fa fa-check-square-o\"></i> ";
										}else {
											hidden_ico = "<i class=\"fa fa-square-o\"></i> ";
										}
									}
									out_html.append("<div class=\"form-check "+GetArrangementDirectionCss(column.getArrangement_direction())+"\">").append("\r\n");
									out_html.append("	<input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_name')\" type=\""+type+"\" "+ischecked+" name=\""+column.getColumn_id()+"_name\" id=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(column.getIs_readonly())+">").append("\r\n");
									out_html.append("	<label class=\"form-check-label\" for=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\">"+hidden_ico+code_d.getZ_value()+"</label>").append("\r\n");
									out_html.append("</div>").append("\r\n");
								}
								out_html.append("</div>").append("\r\n");
							}
						}
					}else if("6".equals(column.getColumn_type())) {//单选
						//获取Code
						if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
							z_code code = z.code.get(column.getP_code_id());
							if(code!=null && code.getZ_code_detail_list().size()>0) {
								List<z_code_detail> delailList = code.getZ_code_detail_list();
								out_html.append("<div class=\"zcheckbox_div\" "+ColumnIsReadonly(column.getIs_readonly())+">").append("\r\n");
								for (z_code_detail code_d : delailList) {
									String value = getColumnValue(parameter,column.getColumn_id(),PageType);
									//如果字段值与Key相同，选中状态
									String ischecked = "";
									if(value.equals(code_d.getZ_key())) {
										ischecked = "checked=\"checked\"";
									}
									String type = "radio";
									//如果只读换成隐藏域
									String hidden_ico = "";
									if("1".equals(column.getIs_readonly())) {
										type = "hidden";
										if(z.isNotNull(ischecked)) {
											hidden_ico = "<i class=\"fa fa-dot-circle-o\"></i> ";
										}else {
											hidden_ico = "<i class=\"fa fa-circle-o\"></i> ";
										}
									}
									out_html.append("<div class=\"form-check "+GetArrangementDirectionCss(column.getArrangement_direction())+"\">").append("\r\n");
									out_html.append("	<input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+code_d.getZ_key()+"_checked_id')\" type=\""+type+"\" "+ischecked+" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(column.getIs_readonly())+">").append("\r\n");
									out_html.append("	<label class=\"form-check-label\" for=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\">"+hidden_ico+code_d.getZ_value()+"</label>").append("\r\n");
									out_html.append("</div>").append("\r\n");
									//out_html.append("<div class=\""+GetArrangementDirectionCss(column.getArrangement_direction())+"\"><input class=\"zcheckbox\" type=\""+type+"\" "+ischecked+" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" value=\""+code_d.getZ_key()+"\">"+hidden_ico+code_d.getZ_value()+" </div>").append("\r\n");
								}
								out_html.append("</div>").append("\r\n");
							}

						}
					}else if("7".equals(column.getColumn_type())) {//下拉框
						out_html.append("<select class=\"form-control\" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" "+ColumnIsReadonly(column.getIs_readonly())+">").append("\r\n");
						out_html.append("<option value=\"\">请选择</option>").append("\r\n");
						//获取Code
						if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
							z_code code = z.code.get(column.getP_code_id());
							if(code!=null && code.getZ_code_detail_list().size()>0) {
								List<z_code_detail> delailList = code.getZ_code_detail_list();
								for (z_code_detail code_d : delailList) {
									String value = getColumnValue(parameter,column.getColumn_id(),PageType);
									if(value.equals(code_d.getZ_key())) {
										out_html.append("<option value=\""+code_d.getZ_key()+"\" selected=\"selected\">"+code_d.getZ_value()+"</option>").append("\r\n");
									}else {
										out_html.append("<option value=\""+code_d.getZ_key()+"\" >"+code_d.getZ_value()+"</option>").append("\r\n");
									}
								}
							}
						}
						out_html.append("</select>").append("\r\n");
					}else if("8".equals(column.getColumn_type())) {//Z5
						String ColumnValue = getColumnValue(parameter,column.getColumn_id(),PageType);
						String SelectDataOnClick = "onclick=\"Z5list('"+table.getTable_id()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display',0)\"";
						//如果字段是只读，删除输入框点击事件
						if("1".equals(column.getIs_readonly())) {
							SelectDataOnClick = "";
						}
						out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+ColumnValue+"\"/>").append("\r\n");
						out_html.append("<input placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_display\" "+SelectDataOnClick+" name=\""+column.getColumn_id()+"_display\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"text\" class=\"form-control marginleft1 border-right-0\" "+" readonly />").append("\r\n");
						out_html.append("<span class=\"input-group-prepend marginright1 border-top border-right border-bottom\">").append("\r\n");
						if(!"look".equals(PageType)) {
							out_html.append("<button class=\"btn btn-light\" onclick=\"Z5Clear('"+table.getTable_id()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display')\" type=\"button\" "+ColumnIsDisabled(column.getIs_readonly())+"><i class=\"fa fa-trash-o\"></i></button>").append("\r\n");
							out_html.append("<button id=\""+column.getColumn_id()+"_selectbutton_id\" class=\"btn btn-light\" "+SelectDataOnClick+" type=\"button\" "+ColumnIsDisabled(column.getIs_readonly())+"><i class=\"fa fa-search\"></i></button>").append("\r\n");
						}
						out_html.append("</span>").append("\r\n");
					}else if("9".equals(column.getColumn_type())) {//日期
						if("1".equals(column.getIs_readonly())) {
							//只读
							out_html.append("<input placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id(),PageType), "yyyy-MM-dd") +"\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
						}else {
							//可以
							out_html.append("<input  placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id(),PageType), "yyyy-MM-dd") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
						}

					}else if("10".equals(column.getColumn_type())) {//日期时间
						if("1".equals(column.getIs_readonly())) {
							//只读
							out_html.append("<input placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id(),PageType), "yyyy-MM-dd HH:mm:ss") +"\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
						}else {
							//可以
							out_html.append("<input  placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id(),PageType), "yyyy-MM-dd HH:mm:ss") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" "+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
						}

					}else if("11".equals(column.getColumn_type())) {//HTML输入框
						String textarea_height = "300";
						if(column.getTextarea_height()!=null && !"".equals(column.getTextarea_height())) {
							textarea_height = column.getTextarea_height();
						}
						out_html.append("<textarea  placeholder=\""+StringUtil.toString(column.getColumn_help())+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" style=\"height: "+textarea_height+"px;\" "+ColumnIsReadonly(column.getIs_readonly())+">"+getColumnValue(parameter,column.getColumn_id(),PageType)+"</textarea>").append("\r\n");
						out_html.append("<script type=\"text/javascript\">InitHTMLColumn('"+column.getColumn_id()+"_id');</script>").append("\r\n");
					}else if("12".equals(column.getColumn_type())) {//源码输入框
						String textarea_height = "300";
						if(column.getTextarea_height()!=null && !"".equals(column.getTextarea_height())) {
							textarea_height = column.getTextarea_height();
						}
						out_html.append("<textarea placeholder=\""+StringUtil.toString(column.getColumn_help())+"\"  id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" style=\"height: "+textarea_height+"px;\" "+ColumnIsReadonly(column.getIs_readonly())+">"+getColumnValue(parameter,column.getColumn_id(),PageType)+"</textarea>").append("\r\n");
						out_html.append("<script type=\"text/javascript\">InitCodeColumn('"+column.getColumn_id()+"_id','"+column.getMode_type()+"','"+textarea_height+"');</script>").append("\r\n");
					}
					out_html.append("</div>").append("\r\n");
					out_html.append("</div>").append("\r\n");

					//添加换行
					if("1".equals(column.getIsbr())) {
						out_html.append("<div class=\"col-12\"></div>").append("\r\n");
					}

					//添加分割线
					if("1".equals(column.getIshr())) {
						out_html.append("<div class=\"col-12\"><hr/></div>").append("\r\n");
					}

				}

			}
		}
		out_html.append("</div>").append("\r\n");
		out_html.append("</div>").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"tableId_id\" name=\"tableId\" value=\""+parameter.get("tableId")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"zid_id\" name=\"zid\" value=\""+parameter.get("zid")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"pid_id\" name=\"pid\" value=\""+parameter.get("pid")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"PageType\" name=\"PageType\" value=\""+PageType+"\" />").append("\r\n");
		//上级表参数
		out_html.append("<input type=\"hidden\" id=\"zversion_id\" name=\"zversion\" value=\""+String.valueOf(parameter.get("zversion")) +"\" />").append("\r\n");
		out_html.append("</form>").append("\r\n");
		out_html.append("</div>").append("\r\n");
	}

	/**
	 * 创建下级表列表
	 * @param detail 
	 * @param out_html
	 * @param table
	 * @param editType 
	 */
	protected void CreateDetailList(Map<String, List<HashMap>> detail, StringBuffer out_html, z_form_table table, String PageType) {
		if(detail!=null && detail.size()>0) {
			String is_collapsed = "false";//编辑页面是否默认展开 十默认展开
			if("1".equals(table.getIs_collapsed())) {
				is_collapsed = "true";
			}
			if("1".equals(table.getEdit_page_layout())) {
				//左右结构
				out_html.append("<div title=\"明 细 表 记 录\"  data-options=\"region:'east',border:true,hideCollapsedContent:false,split:true,collapsed:"+is_collapsed+"\" style=\"width:50%\">").append("\r\n");
			}else {
				//上下结构
				out_html.append("<div title=\"明 细 表 记 录\"  data-options=\"region:'south',border:true,hideCollapsedContent:false,split:true,collapsed:"+is_collapsed+"\" style=\"height:50%\">").append("\r\n");
			}

			out_html.append("<div id=\"DetailTableTab\" class=\"easyui-tabs\"  style=\"height: 100%\">").append("\r\n");
			for (z_form_table detailtable : table.getZ_form_table_detail_list()) {//便利所有子表
				//生成Tab
				out_html.append("<div id=\""+detailtable.getTable_id()+"\" title=\""+detailtable.getTable_title()+"\" data-options=\"tools:'#"+detailtable.getTable_id()+"_tools'\"  >").append("\r\n");

				//创建内部布局开始================================================================================
				out_html.append("<div class=\"easyui-layout\" data-options=\"fit:true\">").append("\r\n");
				//按钮行
				out_html.append("<div data-options=\"region:'north',border:false\">").append("\r\n");
				//判读，如果页面是编辑页面，明细列表设置为list,如果页面是查看状态，设置明细表为查看列表
				String detailTageType = "list";
				if("look".equals(PageType)) {
					detailTageType = "list_look";
				}
				CreateDetailList_Buttons(out_html,detailtable,detailTageType);

				//添加变量
				out_html.append("<input type=\"hidden\" id=\""+detailtable.getTable_id()+"_list_dblclick_id\" name=\"list_dblclick\" value=\""+detailtable.getList_dblclick()+"\" />").append("\r\n");//双击行执行动作



				out_html.append("</div>").append("\r\n");
				//表格行
				out_html.append("<div data-options=\"region:'center',border:false\">").append("\r\n");
				CreateDetailList_Table(detail,out_html,detailtable);
				out_html.append("</div>").append("\r\n");
				out_html.append("</div>").append("\r\n");
				//创建内部布局结束================================================================================

				out_html.append("</div>").append("\r\n");
			}
			out_html.append("</div>").append("\r\n");
			out_html.append("</div>").append("\r\n");
		}
	}

	/**
	 * 创建下级表列表——生成按钮行
	 * @param out_html
	 * @param editType 
	 */
	private void CreateDetailList_Buttons(StringBuffer out_html,z_form_table detailtable, String editType) {
		List<z_form_table_button> buttonList = detailtable.getZ_form_table_button_list();
		out_html.append("<div id=\"ButtonHRDetail\" class=\"btn-group\" role=\"group\">").append("\r\n");
		for (z_form_table_button button : buttonList) {
			if(!"1".equals(button.getIs_hidden()) && editType.equals(button.getPage_type())) {
				out_html.append("<button id=\""+button.getButton_id()+"\" type=\"button\" class=\"btn btn-light btn-sm\" onclick=\""+button.getButton_id()+"();\"><i class=\""+button.getButton_icon()+"\"></i> "+button.getButton_name()+"</button>").append("\r\n");
			}
		}
		out_html.append("</div>").append("\r\n");
	}

	/**
	 * 创建下级表列表——生成Table行
	 * @param out_html
	 */
	private void CreateDetailList_Table(Map<String, List<HashMap>> detail,StringBuffer out_html,z_form_table detailtable) {
		//创建表格
		out_html.append("<table id=\""+detailtable.getTable_id()+"_detail_table\" class=\"easyui-datagrid\" data-options=\"fit:true,ctrlSelect:true,nowrap:false,onDblClickCell:DblClickDetailList\">").append("\r\n");
		//添加checkbox
		out_html.append("<thead data-options=\"frozen:true\">").append("\r\n");
		out_html.append("<tr>").append("\r\n");
		out_html.append("<th data-options=\"field:'zid',checkbox:true\">主键</th>").append("\r\n");
		out_html.append("</tr>").append("\r\n");
		out_html.append("</thead>").append("\r\n");

		//添加表头
		out_html.append("<thead>").append("\r\n");
		out_html.append("<tr>").append("\r\n");
		for (z_form_table_column column : detailtable.getZ_form_table_column_list()) {
			if(!"zid".equals(column.getColumn_id()) && !"pid".equals(column.getColumn_id())) {
				if("0".equals(column.getIs_hidden())) {//判读是否隐藏
					if(!"1".equals(column.getIs_hidden_list())){//判读是否列表隐藏
						if(!"".equals(column.getColunm_length_list()) && column.getColunm_length_list()!=null && !"null".equals(column.getColunm_length_list())) {
							out_html.append("<th data-options=\"field:'"+column.getColumn_id()+"',width:"+column.getColunm_length_list()+"\">"+column.getColumn_name()+"</th>").append("\r\n");
						}else {
							out_html.append("<th data-options=\"field:'"+column.getColumn_id()+"'\">"+column.getColumn_name()+"</th>").append("\r\n");
						}
					}
				}
			}

		}
		out_html.append("</tr>").append("\r\n");
		out_html.append("</thead>").append("\r\n");

		//添加表体
		out_html.append("<tbody>").append("\r\n");
		List<HashMap>  detailTableDataList = detail.get(detailtable.getTable_id());
		for (HashMap detailTableData : detailTableDataList) {
			out_html.append("<tr>").append("\r\n");
			out_html.append("<td>"+detailTableData.get("zid")+"</td>").append("\r\n");
			for (z_form_table_column column : detailtable.getZ_form_table_column_list()) {
				if(!"zid".equals(column.getColumn_id()) && !"pid".equals(column.getColumn_id())) {
					if("0".equals(column.getIs_hidden())) {//判读是否隐藏
						if(!"1".equals(column.getIs_hidden_list())){//判读是否列表隐藏
							if("0".equals(column.getColumn_type())) {//文本
								out_html.append("<td>"+StringUtil.toString(detailTableData.get(column.getColumn_id()))+"</td>").append("\r\n");
							}else if("1".equals(column.getColumn_type())) {//多行文本
								out_html.append("<td>"+StringUtil.toString(detailTableData.get(column.getColumn_id()))+"</td>").append("\r\n");
							}else if("2".equals(column.getColumn_type())) {//数字
								out_html.append("<td>"+StringUtil.toString(detailTableData.get(column.getColumn_id()))+"</td>").append("\r\n");
							}else if("3".equals(column.getColumn_type())) {//文件
								String url = StringUtil.toString(detailTableData.get(column.getColumn_id()));
								if(z.isNotNull(url)) {
									out_html.append("<td><a class=\"btn btn-outline-secondary btn-sm\" href=\""+url+"\" download=\"\">文件下载<a></td>").append("\r\n");
								}else {
									out_html.append("<td></td>").append("\r\n");
								}
							}else if("4".equals(column.getColumn_type())) {//图片
								String url = StringUtil.toString(detailTableData.get(column.getColumn_id()));
								if(z.isNotNull(url)) {
									out_html.append("<td><img src=\""+StringUtil.toString(detailTableData.get(column.getColumn_id()))+"\" style=\"height:32px;\" onclick=\"lookupImg2('"+StringUtil.toString(detailTableData.get(column.getColumn_id()))+"')\"></td>").append("\r\n");
								}else {
									out_html.append("<td></td>").append("\r\n");
								}
							}else if("5".equals(column.getColumn_type())) {//多选
								String CheckedValue = CheckedValue(column,StringUtil.toString(detailTableData.get(column.getColumn_id())));
								out_html.append("<td>"+printTd(CheckedValue,column,detailTableData)+"</td>").append("\r\n");
							}else if("6".equals(column.getColumn_type())) {//单选
								String cv = z.codeValue.get(column.getP_code_id()+"_"+detailTableData.get(column.getColumn_id()));
								if("".equals(cv) || cv==null) {
									cv = StringUtil.toString(detailTableData.get(column.getColumn_id()));
								}
								out_html.append("<td>"+  CodeValueColor(column.getP_code_id()+"_"+detailTableData.get(column.getColumn_id()),StringUtil.toString(cv))+"</td>").append("\r\n");
							}else if("7".equals(column.getColumn_type())) {//下拉框
								String cv = z.codeValue.get(column.getP_code_id()+"_"+detailTableData.get(column.getColumn_id()));
								if("".equals(cv) || cv==null) {
									cv = StringUtil.toString(detailTableData.get(column.getColumn_id()));
								}
								out_html.append("<td>"+  CodeValueColor(column.getP_code_id()+"_"+detailTableData.get(column.getColumn_id()),StringUtil.toString(cv))+"</td>").append("\r\n");
							}else if("8".equals(column.getColumn_type())) {//Z5
								String columnValue = StringUtil.toString(detailTableData.get(column.getColumn_id()));
								out_html.append("<td>"+getZ5DisplayValue(column,columnValue)+"</td>").append("\r\n");
							}else if("9".equals(column.getColumn_type())) {//日期时间
								out_html.append("<td>"+DateUtil.FormatDate(detailTableData.get(column.getColumn_id()), "yyyy-MM-dd")+"</td>").append("\r\n");
								//out_html.append("<td>"+StringUtil.toString(detailTableData.get(column.getColumn_id()))+"</td>").append("\r\n");
							}else if("10".equals(column.getColumn_type())) {//日期时间
								out_html.append("<td>"+DateUtil.FormatDate(detailTableData.get(column.getColumn_id()), "yyyy-MM-dd HH:mm:ss")+"</td>").append("\r\n");
								//out_html.append("<td>"+StringUtil.toString(detailTableData.get(column.getColumn_id()))+"</td>").append("\r\n");
							}else if("11".equals(column.getColumn_type())) {//HTML输入框
								out_html.append("<td>"+StringUtil.toString(detailTableData.get(column.getColumn_id()))+"</td>").append("\r\n");
							}else if("12".equals(column.getColumn_type())) {//源码输入框
								out_html.append("<td>"+StringUtil.toString(detailTableData.get(column.getColumn_id()))+"</td>").append("\r\n");
							}
						}
					}
				}
			}
			out_html.append("</tr>").append("\r\n");
		}
		out_html.append("</tbody>").append("\r\n");
		out_html.append("</table>").append("\r\n");
	}


	/**
	 * 创建报表JS方法
	 * @param out_html
	 * @param pageType edit and list
	 * @param table
	 */
	protected void CreateJSR(StringBuffer out_html, List<z_report_button> buttonList,String userid) {
		out_html.append(" <script type=\"text/javascript\"> ").append("\r\n");
		//添加初始化JS
		out_html.append(" $(document).ready(function(){ ").append("\r\n");
		out_html.append("//设置快速查询框高度").append("\r\n");
		out_html.append("initSearchBoxHeightR();").append("\r\n");
		out_html.append(" }); ").append("\r\n");

		//添加JS方法
		for (z_report_button button : buttonList) {
			if(!"1".equals(button.getIs_hidden())) {
				//判读用户是否有该按钮权限
				String isAvailable = z.UserReportButtons.get(userid+button.getButton_id());
				if(z.isNotNull(isAvailable) || z.sp.get("super_user").equals(userid)) {
					out_html.append(" function "+button.getButton_id()+"(){ ").append("\r\n");
					out_html.append(button.getJs_onclick()).append("\r\n");
					out_html.append(" } ").append("\r\n");
				}
			}
		}
		out_html.append(" </script> ").append("\r\n");
	}

	/**
	 * 添加ready默认JS代码
	 * @param out_html
	 * @param pageType
	 * @param table
	 */
	private void AddReadyJSCode(StringBuffer out_html,String pageType,z_form_table table,HashMap<String,String> parameter) {

		if(z.isNotNull(table)) {
			//列表页面或查看状态的列表
			if("list".equals(pageType) || "list_look".equals(pageType)|| "list_card".equals(pageType)) {
				//添加上移下移页面刷新后选中行
				out_html.append(" checkRowForCheckedZid('"+parameter.get("checked_zid")+"','"+pageType+"','MainTable',''); ").append("\r\n");

				//添加默认JS代码
				if(!"".equals(table.getList_ready_js()) && table.getList_ready_js()!=null && !"null".equals(table.getList_ready_js())) {
					out_html.append(table.getList_ready_js());
				}

				//设置快速查询框高度
				out_html.append("//设置快速查询框高度").append("\r\n");
				out_html.append("initSearchBoxHeight();").append("\r\n");
			}
			if("edit".equals(pageType) || "look".equals(pageType)) {
				//添加上移下移页面刷新后选中行
				out_html.append(" checkRowForCheckedZid('"+parameter.get("checked_zid")+"','"+pageType+"','"+parameter.get("checked_TableID")+"','"+parameter.get("checked_TableTitle")+"'); ").append("\r\n");

				//添加默认JS代码
				if(!"".equals(table.getEdit_ready_js()) && table.getEdit_ready_js()!=null && !"null".equals(table.getEdit_ready_js())) {
					out_html.append(table.getEdit_ready_js());
				}

			}
		}

	}

	/**
	 * 添加默认的JS方法
	 * @param userid 
	 */
	private void AddBaseJSCode(StringBuffer out_html,String pageType,z_form_table table, String userid) {
		//列表页面
		if("list".equals(pageType) && table!=null) {
			//添加默认JS
			if(!"".equals(table.getList_body_js()) && table.getList_body_js()!=null && !"null".equals(table.getList_body_js())) {
				out_html.append(table.getList_body_js());
			}

			//添加默认按钮JS
			List<z_form_table_button> buttonList = table.getZ_form_table_button_list();
			for (z_form_table_button button : buttonList) {
				if("list".equals(button.getPage_type())) {
					if(!"1".equals(button.getIs_hidden())   ||  z.sp.get("super_user").equals(userid)  ) {
						out_html.append(" function "+button.getButton_id()+"(){ ").append("\r\n");
						out_html.append(button.getJs_onclick()).append("\r\n");
						out_html.append(" } ").append("\r\n");
					}
				}

			}
		}

		//卡片组页面
		if("list_card".equals(pageType) && table!=null) {
			//添加默认JS
			if(!"".equals(table.getList_body_js()) && table.getList_body_js()!=null && !"null".equals(table.getList_body_js())) {
				out_html.append(table.getList_body_js());
			}

			//添加默认按钮JS
			List<z_form_table_button> buttonList = table.getZ_form_table_button_list();
			for (z_form_table_button button : buttonList) {
				if("list".equals(button.getPage_type())) {
					if(!"1".equals(button.getIs_hidden())   ||  z.sp.get("super_user").equals(userid)  ) {
						out_html.append(" function "+button.getButton_id()+"(){ ").append("\r\n");
						out_html.append(button.getJs_onclick()).append("\r\n");
						out_html.append(" } ").append("\r\n");
					}
				}
				if("list_card".equals(button.getPage_type())) {
					if(!"1".equals(button.getIs_hidden())   ||  z.sp.get("super_user").equals(userid)  ) {
						out_html.append(" function "+button.getButton_id()+"(obj){ ").append("\r\n");
						out_html.append(button.getJs_onclick()).append("\r\n");
						out_html.append(" } ").append("\r\n");
					}
				}

			}
		}

		//查看列表页面
		if("list_look".equals(pageType) && table!=null) {
			//添加默认JS
			if(!"".equals(table.getList_body_js()) && table.getList_body_js()!=null && !"null".equals(table.getList_body_js())) {
				out_html.append(table.getList_body_js());
			}

			//添加默认按钮JS
			List<z_form_table_button> buttonList = table.getZ_form_table_button_list();
			for (z_form_table_button button : buttonList) {
				if("list_look".equals(button.getPage_type())) {
					if(!"1".equals(button.getIs_hidden()) || z.sp.get("super_user").equals(userid)  ) {
						out_html.append(" function "+button.getButton_id()+"(){ ").append("\r\n");
						out_html.append(button.getJs_onclick()).append("\r\n");
						out_html.append(" } ").append("\r\n");
					}
				}

			}
		}

		//编辑页面
		if(z.isNotNull(table)) {
			if("edit".equals(pageType) || "add".equals(pageType)) {
				//添加默认JS
				if(!"".equals(table.getEdit_body_js()) && table.getEdit_body_js()!=null && !"null".equals(table.getEdit_body_js())) {
					out_html.append(table.getEdit_body_js());
				}

				//添加默认按钮JS
				List<z_form_table_button> buttonList = table.getZ_form_table_button_list();
				for (z_form_table_button button : buttonList) {
					if("edit".equals(button.getPage_type())) {
						if(!"1".equals(button.getIs_hidden()) || z.sp.get("super_user").equals(userid)  ) {
							out_html.append(" function "+button.getButton_id()+"(){ ").append("\r\n");

							//添加字段非空校验---只有保存方法才添加
							if(button.getButton_id().indexOf("save_button")>=0 ||
									button.getButton_id().indexOf("save_and_add_button")>=0 ||
									button.getButton_id().indexOf("save_and_return_button")>=0 ){

								List<z_form_table_column> columnList = table.getZ_form_table_column_list();
								for (z_form_table_column col : columnList) {
									if("1".equals(col.getIs_null()) 
											&& !"zid".equals(col.getColumn_id()) 
											&& !"seq".equals(col.getColumn_id())
											&& !"pid".equals(col.getColumn_id())
											&& !"create_user".equals(col.getColumn_id())
											&& !"create_time".equals(col.getColumn_id())
											&& !"update_user".equals(col.getColumn_id())
											&& !"update_time".equals(col.getColumn_id())
											&& !"zversion".equals(col.getColumn_id())
											&& !"orgid".equals(col.getColumn_id())
											&& !"remarks".equals(col.getColumn_id())) {

										if("0".equals(col.getColumn_type())) {//文本

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("1".equals(col.getColumn_type())) {//多行文本

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("2".equals(col.getColumn_type())) {//数字

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("3".equals(col.getColumn_type())) {//文件

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("4".equals(col.getColumn_type())) {//图片

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("5".equals(col.getColumn_type())) {//多选

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("6".equals(col.getColumn_type())) {//单选

											out_html.append("if(isNull(typeof($(\"input[name='"+col.getColumn_id()+"']:checked\").val()))){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("7".equals(col.getColumn_type())) {//下拉框

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("8".equals(col.getColumn_type())) {//Z5

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("9".equals(col.getColumn_type())) {//日期

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("10".equals(col.getColumn_type())) {//日期时间

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("11".equals(col.getColumn_type())) {//HTML输入框

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}else if("12".equals(col.getColumn_type())) {//源码输入框

											out_html.append("if(document.getElementById('"+col.getColumn_id()+"_id').value==''){").append("\r\n");
											out_html.append("	alert('"+col.getColumn_name()+"不可为空');").append("\r\n");
											out_html.append("	document.getElementById('"+col.getColumn_id()+"_id').focus();").append("\r\n");
											out_html.append("	return;").append("\r\n");
											out_html.append("}").append("\r\n");

										}


									}
								}


							}

							out_html.append(button.getJs_onclick()).append("\r\n");
							out_html.append(" } ").append("\r\n");
						}
					}

				}

				//添加当前表子表的list按钮
				List<z_form_table> dtablelist = table.getZ_form_table_detail_list();
				for (z_form_table dtable : dtablelist) {
					List<z_form_table_button> dtablebuttonList = dtable.getZ_form_table_button_list();
					for (z_form_table_button dtablebutton : dtablebuttonList) {
						if(!"1".equals(dtablebutton.getIs_hidden()) && "list".equals(dtablebutton.getPage_type())) {
							out_html.append(" function "+dtablebutton.getButton_id()+"(){ ").append("\r\n");
							out_html.append(dtablebutton.getJs_onclick()).append("\r\n");
							out_html.append(" } ").append("\r\n");
						}
					}
				}
			}
		}



		//查看页面
		if("look".equals(pageType) && table!=null) {
			//添加默认JS
			if(!"".equals(table.getEdit_body_js()) && table.getEdit_body_js()!=null && !"null".equals(table.getEdit_body_js())) {
				out_html.append(table.getEdit_body_js());
			}

			//添加默认按钮JS
			List<z_form_table_button> buttonList = table.getZ_form_table_button_list();
			for (z_form_table_button button : buttonList) {
				if("look".equals(button.getPage_type())) {
					if(!"1".equals(button.getIs_hidden()) || z.sp.get("super_user").equals(userid)  ) {
						out_html.append(" function "+button.getButton_id()+"(){ ").append("\r\n");
						out_html.append("		"+button.getJs_onclick()).append("\r\n");
						out_html.append(" } ").append("\r\n");
					}
				}

			}

			//添加当前表子表的list按钮
			List<z_form_table> dtablelist = table.getZ_form_table_detail_list();
			for (z_form_table dtable : dtablelist) {
				List<z_form_table_button> dtablebuttonList = dtable.getZ_form_table_button_list();
				for (z_form_table_button dtablebutton : dtablebuttonList) {
					if(!"1".equals(dtablebutton.getIs_hidden()) && "list_look".equals(dtablebutton.getPage_type())) {
						out_html.append(" function "+dtablebutton.getButton_id()+"(){ ").append("\r\n");
						out_html.append("		"+dtablebutton.getJs_onclick()).append("\r\n");
						out_html.append(" } ").append("\r\n");
					}
				}
			}
		}
	}

	//创建功能代码
	protected void CreateMFunctionHtml(List<z_form_table_button> buttonList,StringBuffer out_html,String pageType,String userid) throws Exception{
		int buttonCount = 0;
		StringBuffer buttonHTML = new StringBuffer();
		buttonHTML.append("<div class=\"mhead_button_box\">").append("\r\n");
		for (z_form_table_button button : buttonList) {
			if(!"1".equals(button.getIs_hidden()) && pageType.equals(button.getPage_type())) {
				String buttonId = button.getZid();
				//判读用户是否有该按钮权限
				String isAvailable = z.UserFunctionButtons.get(userid+buttonId);
				if(z.isNotNull(isAvailable) || z.sp.get("super_user").equals(userid)) {
					buttonCount = buttonCount+1;
					buttonHTML.append("<div class=\"mhead_button\" onclick=\""+button.getButton_id()+"();\">").append("\r\n");
					buttonHTML.append("<i class=\""+button.getButton_icon()+"\"></i>").append("\r\n");
					buttonHTML.append("<span>"+button.getButton_name()+"</span>").append("\r\n");
					buttonHTML.append("</div>").append("\r\n");
				}
			}
		}
		buttonHTML.append("</div>").append("\r\n");

		if(buttonCount>0) {
			out_html.append(buttonHTML);
		}
	}

	/**
	 * 创建移动端List页面标题行
	 * @param out_html
	 * @param table
	 */
	protected void CreateMListTitleHTML(StringBuffer out_html, z_form_table table) throws Exception{
		out_html.append("<div class=\"mhead_title row justify-content-start align-items-center\">").append("\r\n");
		out_html.append("<div class=\"col-4\"><a href=\"main_mobile_function\"><i class=\"fa fa-angle-left\"></i></a></div>").append("\r\n");
		out_html.append("<div class=\"col-4\">"+table.getTable_title()+"</div>").append("\r\n");
		out_html.append("</div>").append("\r\n");
	}

	//创建功能代码
	protected void CreateFunctionHtml(List<z_form_table_button> buttonList,StringBuffer out_html,String PageType,String userid,z_form_table table, HashMap<String, String> parameter) throws Exception{
		int buttonCount = 0;
		StringBuffer buttonHTML = new StringBuffer();

		buttonHTML.append("<div id=\"ButtonHR\" class=\"row\"><div class=\"col-sm border-bottom\">").append("\r\n");

		//如果是手机访问列表页面，添加返回按钮
		if(PageType.equals("list") || PageType.equals("list_card")) {
			if("1".equals(parameter.get("is_mobile"))) {
				String return_mobile_view = parameter.get("return_mobile_view");
				if(z.isNull(return_mobile_view)) {
					return_mobile_view = "nav_mobile";
				}
				buttonHTML.append("<button id=\"return_nav_mobile_button\" type=\"button\" class=\"btn btn-light\" onclick=\"window.location.href='"+return_mobile_view+"';\"><i class=\"fa fa-hand-o-left\"></i>&nbsp;返回</button>").append("\r\n");
			}
		}


		for (z_form_table_button button : buttonList) {
			//页面类型为edit
			if(PageType.equals("add")) {
				PageType = "edit";
			}
			if(PageType.equals(button.getPage_type())) {
				//卡片列表
				if(PageType.equals("list_card")) {
					//按钮未隐藏，或当前用户为管理员
					if(!"1".equals(button.getIs_hidden()) || userid.equals(z.sp.get("super_user"))) {
						String buttonId = button.getZid();
						//判读用户是否有该按钮权限
						String isAvailable = z.UserFunctionButtons.get(userid+buttonId);
						if(z.isNotNull(isAvailable) || userid.equals(z.sp.get("super_user"))) {
							if(button.getButton_id().indexOf(table.getTable_id()+"_add_button")>=0 || button.getButton_id().indexOf(table.getTable_id()+"_select_button")>=0) {
								buttonCount = buttonCount+1;
								buttonHTML.append("<button id=\""+button.getButton_id()+"\" type=\"button\" class=\"btn btn-light\" onclick=\""+button.getButton_id()+"();\"><i class=\""+button.getButton_icon()+"\"></i> "+button.getButton_name()+"</button>").append("\r\n");
							}
						}
					}
				}else {
					//列表/编辑/查看
					//按钮未隐藏，或当前用户为管理员
					if(!"1".equals(button.getIs_hidden()) || userid.equals(z.sp.get("super_user"))) {
						String buttonId = button.getZid();
						//判读用户是否有该按钮权限
						String isAvailable = z.UserFunctionButtons.get(userid+buttonId);
						if(z.isNotNull(isAvailable) || userid.equals(z.sp.get("super_user"))) {
							//如果按钮是提交流程按钮，
							if(button.getButton_id().indexOf("oa_submit_button")>=0 || button.getJs_onclick().indexOf("oa_submit()")>=0) {
								//判读表单是否绑定工作流程，如果未绑定不显示【提交申请按钮】
								List<z_workflow> wflist = z.wfListForTableid.get(table.getTable_id());
								if(z.isNotNull(wflist) && wflist.size()>0) {
									buttonCount = buttonCount+1;
									buttonHTML.append("<button id=\""+button.getButton_id()+"\" type=\"button\" class=\"btn btn-light\" onclick=\""+button.getButton_id()+"();\"><i class=\""+button.getButton_icon()+"\"></i> "+button.getButton_name()+"</button>").append("\r\n");
								}
							}else {
								//判读是否是移动端
								if("1".equals(parameter.get("is_mobile"))) {
									//不是打印按钮
									if(button.getButton_id().indexOf("_print_list_button")<0) {
										buttonCount = buttonCount+1;
										buttonHTML.append("<button id=\""+button.getButton_id()+"\" type=\"button\" class=\"btn btn-light\" onclick=\""+button.getButton_id()+"();\"><i class=\""+button.getButton_icon()+"\"></i> "+button.getButton_name()+"</button>").append("\r\n");
									}
								}else {
									//判读是否显示查询按钮，如果启动了快速查询就隐藏查询按钮
									if(button.getButton_id().indexOf("_select_button")>=0) {
										z_form zf = z.form_tableid.get(table.getTable_id());
										if(z.isNotNull(zf) && "1".equals(zf.getIsStartQuickQuery())) {
											//隐藏查询按钮
										}else {
											buttonCount = buttonCount+1;
											buttonHTML.append("<button id=\""+button.getButton_id()+"\" type=\"button\" class=\"btn btn-light\" onclick=\""+button.getButton_id()+"();\"><i class=\""+button.getButton_icon()+"\"></i> "+button.getButton_name()+"</button>").append("\r\n");
										}
									}else {
										buttonCount = buttonCount+1;
										buttonHTML.append("<button id=\""+button.getButton_id()+"\" type=\"button\" class=\"btn btn-light\" onclick=\""+button.getButton_id()+"();\"><i class=\""+button.getButton_icon()+"\"></i> "+button.getButton_name()+"</button>").append("\r\n");
									}
								}
							}
						}
					}
				}

			}

		}
		buttonHTML.append("</div></div>").append("\r\n");

		//创建工作流程选择框
		buttonHTML.append("<div id=\"oa_wid_windows\" class=\"easyui-window\" title=\"请选择申请的流程\" style=\"width:500px;height:300px\" data-options=\"iconCls:'fa fa-server',closed:true,modal:true,shadow:false,minimizable:false,collapsible:false\">").append("\r\n");
		buttonHTML.append("</div>").append("\r\n");

		if(buttonCount>0) {
			out_html.append(buttonHTML);
		}
	}


	/**
	 * 创建查询域代码
	 * @param out_html
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws Exception 
	 */
	protected void CreateSelectHtml(String pageType,HashMap<String,String> parameter,StringBuffer out_html,z_form_table table,List<z_form_table_column> columnList) throws Exception{

		//查询域
		//如果是手机访问，去掉查询关闭按钮，并初始化时最大化
		String c_dataoptions = "";
		if("1".equals(parameter.get("is_mobile"))) {
			c_dataoptions = ",closable:false,maximized:true";
		}else {
			c_dataoptions = ",closable:true";
		}
		out_html.append(" <div id=\"select_windows\" class=\"easyui-window\" title=\"查询条件\" style=\"width:900px;height:600px\" data-options=\"iconCls:'fa fa-filter',modal:true,closed:true,shadow:false,minimizable:false,maximizable:false,collapsible:false"+c_dataoptions+"\"> ").append("\r\n");
		out_html.append(" <div class=\"easyui-layout\" data-options=\"fit:true,border:false\"> ").append("\r\n");

		//常用查询条件
		if(!"1".equals(parameter.get("is_mobile"))) {
			//获取用户该表单的常用查询条件
			List<z_form_query> fqlist = JsonUtil.jsonToList2(parameter.get("queryinfolist"), z_form_query.class);
			out_html.append(" <div data-options=\"region:'west',split:false\" style=\"width:260px;\"> ").append("\r\n");
			out_html.append(" <table id=\"QueryInfoList\" class=\"easyui-datagrid\" data-options=\"singleSelect:true,onClickRow:SelectQueryInfoOnDblClickRow,fit:true,nowrap:false,border:false\"> ").append("\r\n");
			out_html.append(" <thead> ").append("\r\n");
			out_html.append(" 	<tr>").append("\r\n");
			out_html.append(" 		<th data-options=\"field:'zid',hidden:true\">zid</th>").append("\r\n");
			out_html.append(" 		<th data-options=\"field:'name',width:258\">常用查询条件</th>").append("\r\n");
			out_html.append(" 		<th data-options=\"field:'queryinfo',hidden:true\">queryinfo</th>").append("\r\n");
			out_html.append(" 	</tr> ").append("\r\n");
			out_html.append(" </thead> ").append("\r\n");
			out_html.append(" <tbody> ").append("\r\n");
			for (z_form_query fq : fqlist) {
				out_html.append(" 	<tr> ").append("\r\n");
				out_html.append(" 		<td>"+fq.getZid()+"</td> ").append("\r\n");
				out_html.append(" 		<td>"+fq.getName()+"</td> ").append("\r\n");
				out_html.append(" 		<td>"+fq.getQueryinfo()+"</td> ").append("\r\n");
				out_html.append(" 	</tr> ").append("\r\n");
			}
			out_html.append(" </tbody> ").append("\r\n");
			out_html.append(" </table> ").append("\r\n");
			out_html.append(" </div> ").append("\r\n");
		}

		//查询条件
		out_html.append(" <div data-options=\"region:'center',border:false\" > ").append("\r\n");
		out_html.append(" 	<div data-options=\"region:'north',border:false\"  >").append("\r\n");
		out_html.append(" 		<div class=\"easyui-panel\" style=\"padding:2px;\">").append("\r\n");
		out_html.append(" 			<a id=\"AddQueryRomButton\" href=\"javascript:void(0);\" class=\"easyui-linkbutton\" onclick=\"AddQueryRom()\" data-options=\"plain:true,iconCls:'fa fa-plus-circle'\">增加行</a>").append("\r\n");
		out_html.append(" 			<a id=\"DeleteQueryTermsButton\" href=\"javascript:void(0);\" class=\"easyui-linkbutton\" onclick=\"DeleteQueryInfo()\" data-options=\"plain:true,iconCls:'fa fa-trash-o'\">删除常用查询条件</a>").append("\r\n");
		out_html.append(" 			<a id=\"SaveQueryTermsButton\" href=\"javascript:void(0);\" class=\"easyui-linkbutton\" onclick=\"SaveQueryTerms()\" data-options=\"plain:true,iconCls:'fa fa-floppy-o'\">保存查询条件</a>").append("\r\n");
		out_html.append(" 		</div>").append("\r\n");
		out_html.append(" 	</div>").append("\r\n");


		out_html.append(" <table id=\"QueryTable\"  class=\"table table-sm table-bordered\"> ").append("\r\n");
		out_html.append(" <thead> ").append("\r\n");
		out_html.append(" 	<tr>").append("\r\n");
		out_html.append(" 		<th width=\"30%\">查询列</th>").append("\r\n");
		out_html.append(" 		<th width=\"15%\">比较符</th>").append("\r\n");
		out_html.append(" 		<th width=\"35%\">查询内容</th>").append("\r\n");
		out_html.append(" 		<th width=\"15%\">条件连接</th>").append("\r\n");
		out_html.append(" 		<th width=\"5%\"></th>").append("\r\n");
		out_html.append(" 	</tr> ").append("\r\n");
		out_html.append(" </thead> ").append("\r\n");
		out_html.append(" <tbody> ").append("\r\n");
		String query_terms = parameter.get("query_terms");
		if(!"".equals(query_terms) &&  query_terms!=null && !"null".equals(query_terms)) {
			List<QueryTerms> query_terms_list = JSONArray.parseArray(query_terms, QueryTerms.class);
			for (QueryTerms queryTerms : query_terms_list) {
				out_html.append(" <tr> ").append("\r\n");

				//添加列名
				StringBuffer ComparisonColumnIdHTML = new StringBuffer();
				String column_type = "";
				z_form_table_column column_obj = null;
				ComparisonColumnIdHTML.append("<select name=\"ComparisonColumnId\" class=\"form-control form-control-sm\">").append("\r\n");
				for (int i = 0; i < columnList.size(); i++) {
					z_form_table_column column = columnList.get(i);
					if(column.getIs_hidden().equals("0")) {
						if(queryTerms.getComparisonColumnId().equals(column.getColumn_id())) {
							//保存列类型，供创建比较值列使用
							column_type = column.getColumn_type();
							column_obj = column;
							ComparisonColumnIdHTML.append("<option value=\""+column.getColumn_id()+"\" selected=\"selected\">"+column.getColumn_name()+"</option>';").append("\r\n");
						}else {
							ComparisonColumnIdHTML.append("<option value=\""+column.getColumn_id()+"\">"+column.getColumn_name()+"</option>';").append("\r\n");
						}
					}
				}
				ComparisonColumnIdHTML.append("</select>").append("\r\n");
				out_html.append(" <td>"+ComparisonColumnIdHTML+"</td> ").append("\r\n");

				//添加比较符
				StringBuffer ComparisonTypeHTML = new StringBuffer();
				ComparisonTypeHTML.append("<select name=\"ComparisonType\" class=\"form-control form-control-sm\">").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"1\" "+isSelected("1",queryTerms.getComparisonType())+">等于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"2\" "+isSelected("2",queryTerms.getComparisonType())+">包含</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"3\" "+isSelected("3",queryTerms.getComparisonType())+">大于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"4\" "+isSelected("4",queryTerms.getComparisonType())+">大于等于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"5\" "+isSelected("5",queryTerms.getComparisonType())+">小于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"6\" "+isSelected("6",queryTerms.getComparisonType())+">小于等于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"7\" "+isSelected("7",queryTerms.getComparisonType())+">不等于</option>").append("\r\n");
				ComparisonTypeHTML.append("</select>").append("\r\n");
				out_html.append(" <td>"+ComparisonTypeHTML+"</td> ").append("\r\n");

				//添加比较值
				//判读列字段类型
				if("0".equals(column_type)) {//文本
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("1".equals(column_type)) {//多行文本
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("2".equals(column_type)) {//数字
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"number\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td>").append("\r\n");
				}else if("3".equals(column_type)) {//文件
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"url\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("4".equals(column_type)) {//图片
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"url\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("5".equals(column_type)) {//多选
					String trNum = z.newNumber();
					out_html.append(" <td>").append("\r\n");
					out_html.append("<input name=\"ComparisonValue\" id=\""+column_obj.getColumn_id()+trNum+"_key\" type=\"hidden\"  value=\""+queryTerms.getComparisonValue()+"\"/>").append("\r\n");
					//获取Code
					if(!"".equals(column_obj.getP_code_id()) && column_obj.getP_code_id()!=null) {
						z_code code = z.code.get(column_obj.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							for (z_code_detail code_d : delailList) {
								out_html.append("<div class=\"form-check float_left\">").append("\r\n");
								boolean ic = isChecked(queryTerms.getComparisonValue(),code_d.getZ_key());
								if(ic) {
									out_html.append("<input id=\""+column_obj.getColumn_id()+trNum+code_d.getZ_key()+"_value\" name=\"ComparisonValue_"+trNum+"_name\" onchange=\"getCheckedValue('"+column_obj.getColumn_id()+trNum+"_key','ComparisonValue_"+trNum+"_name')\" type=\"checkbox\" checked=\"checked\" value=\""+code_d.getZ_key()+"\">").append("\r\n");
								}else {
									out_html.append("<input id=\""+column_obj.getColumn_id()+trNum+code_d.getZ_key()+"_value\" name=\"ComparisonValue_"+trNum+"_name\" onchange=\"getCheckedValue('"+column_obj.getColumn_id()+trNum+"_key','ComparisonValue_"+trNum+"_name')\" type=\"checkbox\"                     value=\""+code_d.getZ_key()+"\">").append("\r\n");
								}
								out_html.append("<label class=\"form-check-label\" for=\""+column_obj.getColumn_id()+trNum+code_d.getZ_key()+"_value\">"+code_d.getZ_value()+"</label>").append("\r\n");
								out_html.append("</div>").append("\r\n");
							}
						}else {
							out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
						}
					}else {
						out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
					}
					out_html.append("</td> ").append("\r\n");
				}else if("6".equals(column_type)) {//单选
					out_html.append(" <td>").append("\r\n");
					//获取Code
					if(!"".equals(column_obj.getP_code_id()) && column_obj.getP_code_id()!=null) {
						z_code code = z.code.get(column_obj.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							for (z_code_detail code_d : delailList) {
								out_html.append("<input name=\"ComparisonValue\" type=\"radio\" "+isRadioChecked(queryTerms.getComparisonValue(),code_d.getZ_key())+" value=\""+code_d.getZ_key()+"\"> "+code_d.getZ_value()+" ").append("\r\n");
							}
						}else {
							out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
						}
					}else {
						out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
					}
					out_html.append("</td> ").append("\r\n");
				}else if("7".equals(column_type)) {//下拉框
					out_html.append(" <td>").append("\r\n");
					//获取Code
					if(!"".equals(column_obj.getP_code_id()) && column_obj.getP_code_id()!=null) {
						z_code code = z.code.get(column_obj.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							out_html.append(" <select name=\"ComparisonValue\" class=\"form-control form-control-sm\">").append("\r\n");
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							for (z_code_detail code_d : delailList) {
								out_html.append("<option value=\""+code_d.getZ_key()+"\" "+isSelected(queryTerms.getComparisonValue(),code_d.getZ_key())+">"+code_d.getZ_value()+"</option>").append("\r\n");
							}
							out_html.append("</select> ").append("\r\n");
						}else {
							out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
						}
					}else {
						out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
					}
					out_html.append("</td> ").append("\r\n");
				}else if("8".equals(column_type)) {//Z5
					String trNum = z.newNumber();
					String SelectDataOnClick = "onclick=\"Z5list('"+parameter.get("tableId")+"_"+column_obj.getColumn_id()+"','"+column_obj.getColumn_id() + trNum+"_key','"+column_obj.getColumn_id() + trNum+"_display',0)\"";
					out_html.append("<td>").append("\r\n");
					out_html.append("<div class=\"input-group input-group-sm\">").append("\r\n");
					out_html.append("<input id=\""+column_obj.getColumn_id() + trNum+"_key\" name=\"ComparisonValue\" type=\"hidden\"  value=\""+queryTerms.getComparisonValue()+"\"/>").append("\r\n");
					out_html.append("<input readonly=\"true\" id=\""+column_obj.getColumn_id() + trNum+"_display\"  "+SelectDataOnClick+" value=\""+getZ5DisplayValue(column_obj,queryTerms.getComparisonValue())+"\" type=\"text\" class=\"form-control form-control-sm\" />").append("\r\n");
					out_html.append("<span class=\"input-group-append\">").append("\r\n");
					out_html.append("<button class=\"btn btn-light\" "+SelectDataOnClick+" type=\"button\"><i class=\"fa fa-search\"></i></button>").append("\r\n");
					out_html.append("</span>").append("\r\n");
					out_html.append("</div>").append("\r\n");
					out_html.append("</td>").append("\r\n");
				}else if("9".equals(column_type)) {//日期
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"Wdate form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" /></td> ").append("\r\n");
				}else if("10".equals(column_type)) {//日期时间
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"Wdate form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" /></td> ").append("\r\n");
				}else if("11".equals(column_type)) {//HTML输入框
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("12".equals(column_type)) {//源码输入框
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else {
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}

				//添加连接符
				StringBuffer ConnectorTypeHTML = new StringBuffer();
				ConnectorTypeHTML.append("<select name=\"ConnectorType\" class=\"form-control form-control-sm\">").append("\r\n");
				ConnectorTypeHTML.append("<option value=\"0\" "+isSelected("0",queryTerms.getConnectorType())+">&nbsp;</option>").append("\r\n");
				ConnectorTypeHTML.append("<option value=\"1\" "+isSelected("1",queryTerms.getConnectorType())+">并且</option>").append("\r\n");
				ConnectorTypeHTML.append("<option value=\"2\" "+isSelected("2",queryTerms.getConnectorType())+">或者</option>").append("\r\n");
				ConnectorTypeHTML.append("</select>").append("\r\n");
				out_html.append(" <td>"+ConnectorTypeHTML+"</td> ").append("\r\n");

				//添加删除按键
				out_html.append(" <td><button type=\"button\" class=\"btn btn-light\" onclick=\"DeleteQueryRom(this)\"><i class=\"fa fa-trash-o\"></i></button></td> ").append("\r\n");

				out_html.append(" </tr> ").append("\r\n");
			}
		}
		out_html.append(" </tbody> ").append("\r\n");
		out_html.append(" </table> ").append("\r\n");




		out_html.append(" </div> ").append("\r\n");
		out_html.append(" <div data-options=\"region:'south'\" style=\"height:60px; text-align:center;padding-top: 10px\"> ").append("\r\n");
		if("1".equals(parameter.get("is_mobile"))) {
			out_html.append(" <button type=\"button\" class=\"btn btn-light\" onclick=\"$('#select_windows').window('close');\"><i class=\"fa fa-window-close\"></i> 关闭</button>").append("\r\n");
		}
		out_html.append(" <button type=\"button\" class=\"btn btn-light\" onclick=\"form_clear()\"><i class=\"fa fa-trash-o\"></i> 清空</button>").append("\r\n");
		out_html.append(" <button type=\"button\" class=\"btn btn-light\" onclick=\"form_query(1)\"><i class=\"fa fa-search\"></i> 查询</button>").append("\r\n");
		out_html.append(" </div> ").append("\r\n");
		out_html.append(" </div> ").append("\r\n");
		out_html.append(" </div> ").append("\r\n");
	}

	/**
	 * 创建查询域代码
	 * @param out_html
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws Exception 
	 */
	protected void CreateSelectHtmlR(String pageType,HashMap<String,String> parameter,StringBuffer out_html,z_report r,List<z_report_column> columnList) throws Exception{
		//查询域
		//如果是手机访问，去掉查询关闭按钮，并初始化时最大化
		String c_dataoptions = "";
		if("1".equals(parameter.get("is_mobile"))) {
			c_dataoptions = ",closable:false,maximized:true";
		}else {
			c_dataoptions = ",closable:true";
		}
		out_html.append(" <div id=\"select_windows\" class=\"easyui-window\" title=\"查询条件\" style=\"width:800px;height:600px\" data-options=\"iconCls:'fa fa-filter',closed:true,modal:true,shadow:false,minimizable:false,maximizable:false,collapsible:false"+c_dataoptions+"\"> ").append("\r\n");
		out_html.append(" <div class=\"easyui-layout\" data-options=\"fit:true,border:false\"> ").append("\r\n");

		//常用查询条件
		if(!"1".equals(parameter.get("is_mobile"))) {
			//获取用户该表单的常用查询条件
			List<z_form_query> fqlist = JsonUtil.jsonToList2(parameter.get("queryinfolist"), z_form_query.class);
			out_html.append(" <div data-options=\"region:'west',split:false\" style=\"width:260px;\"> ").append("\r\n");
			out_html.append(" <table id=\"QueryInfoList\" class=\"easyui-datagrid\" data-options=\"singleSelect:true,onClickRow:SelectQueryInfoOnDblClickRowR,fit:true,nowrap:false,border:false\"> ").append("\r\n");
			out_html.append(" <thead> ").append("\r\n");
			out_html.append(" 	<tr>").append("\r\n");
			out_html.append(" 		<th data-options=\"field:'zid',hidden:true\">zid</th>").append("\r\n");
			out_html.append(" 		<th data-options=\"field:'name',width:258\">常用查询条件</th>").append("\r\n");
			out_html.append(" 		<th data-options=\"field:'queryinfo',hidden:true\">queryinfo</th>").append("\r\n");
			out_html.append(" 	</tr> ").append("\r\n");
			out_html.append(" </thead> ").append("\r\n");
			out_html.append(" <tbody> ").append("\r\n");
			for (z_form_query fq : fqlist) {
				out_html.append(" 	<tr> ").append("\r\n");
				out_html.append(" 		<td>"+fq.getZid()+"</td> ").append("\r\n");
				out_html.append(" 		<td>"+fq.getName()+"</td> ").append("\r\n");
				out_html.append(" 		<td>"+fq.getQueryinfo()+"</td> ").append("\r\n");
				out_html.append(" 	</tr> ").append("\r\n");
			}
			out_html.append(" </tbody> ").append("\r\n");
			out_html.append(" </table> ").append("\r\n");
			out_html.append(" </div> ").append("\r\n");
		}

		out_html.append(" <div data-options=\"region:'center',border:false\" > ").append("\r\n");
		out_html.append(" 	<div data-options=\"region:'north',border:false\"  >").append("\r\n");
		out_html.append(" 		<div class=\"easyui-panel\" style=\"padding:2px;\">").append("\r\n");
		out_html.append(" 			<a id=\"AddQueryRomButton\" href=\"javascript:void(0);\" class=\"easyui-linkbutton\" onclick=\"AddQueryRomR()\" data-options=\"plain:true,iconCls:'fa fa-plus-circle'\">增加行</a>").append("\r\n");
		out_html.append(" 			<a id=\"DeleteQueryTermsButton\" href=\"javascript:void(0);\" class=\"easyui-linkbutton\" onclick=\"DeleteQueryInfoR()\" data-options=\"plain:true,iconCls:'fa fa-trash-o'\">删除常用查询条件</a>").append("\r\n");
		out_html.append(" 			<a id=\"SaveQueryTermsButton\" href=\"javascript:void(0);\" class=\"easyui-linkbutton\" onclick=\"SaveQueryTermsR()\" data-options=\"plain:true,iconCls:'fa fa-floppy-o'\">保存查询条件</a>").append("\r\n");
		out_html.append(" 		</div>").append("\r\n");
		out_html.append(" 	</div>").append("\r\n");


		out_html.append(" <table id=\"QueryTable\"  class=\"table table-sm table-bordered\"> ").append("\r\n");
		out_html.append(" <thead> ").append("\r\n");
		out_html.append(" 	<tr>").append("\r\n");
		out_html.append(" 		<th width=\"30%\">查询列</th>").append("\r\n");
		out_html.append(" 		<th width=\"15%\">比较符</th>").append("\r\n");
		out_html.append(" 		<th width=\"35%\">查询内容</th>").append("\r\n");
		out_html.append(" 		<th width=\"15%\">条件连接</th>").append("\r\n");
		out_html.append(" 		<th width=\"5%\"></th>").append("\r\n");
		out_html.append(" 	</tr> ").append("\r\n");
		out_html.append(" </thead> ").append("\r\n");
		out_html.append(" <tbody> ").append("\r\n");
		String query_terms = parameter.get("query_terms");
		if(!"".equals(query_terms) &&  query_terms!=null && !"null".equals(query_terms)) {
			List<QueryTerms> query_terms_list = JSONArray.parseArray(query_terms, QueryTerms.class);
			for (QueryTerms queryTerms : query_terms_list) {
				out_html.append(" <tr> ").append("\r\n");

				//添加列名
				StringBuffer ComparisonColumnIdHTML = new StringBuffer();
				String column_type = "";
				z_report_column column_obj = null;
				ComparisonColumnIdHTML.append("<select name=\"ComparisonColumnId\" class=\"form-control form-control-sm\">").append("\r\n");
				for (int i = 0; i < columnList.size(); i++) {
					z_report_column column = columnList.get(i);
					if(queryTerms.getComparisonColumnId().equals(column.getColumn_id())) {
						//保存列类型，供创建比较值列使用
						column_type = column.getColumn_type();
						column_obj = column;
						ComparisonColumnIdHTML.append("<option value=\""+column.getColumn_id()+"\" selected=\"selected\">"+column.getColumn_name()+"</option>';").append("\r\n");
					}else {
						ComparisonColumnIdHTML.append("<option value=\""+column.getColumn_id()+"\">"+column.getColumn_name()+"</option>';").append("\r\n");
					}
				}
				ComparisonColumnIdHTML.append("</select>").append("\r\n");
				out_html.append(" <td>"+ComparisonColumnIdHTML+"</td> ").append("\r\n");

				//添加比较符
				StringBuffer ComparisonTypeHTML = new StringBuffer();
				ComparisonTypeHTML.append("<select name=\"ComparisonType\" class=\"form-control form-control-sm\">").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"1\" "+isSelected("1",queryTerms.getComparisonType())+">等于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"2\" "+isSelected("2",queryTerms.getComparisonType())+">包含</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"3\" "+isSelected("3",queryTerms.getComparisonType())+">大于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"4\" "+isSelected("4",queryTerms.getComparisonType())+">大于等于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"5\" "+isSelected("5",queryTerms.getComparisonType())+">小于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"6\" "+isSelected("6",queryTerms.getComparisonType())+">小于等于</option>").append("\r\n");
				ComparisonTypeHTML.append("<option value=\"7\" "+isSelected("7",queryTerms.getComparisonType())+">不等于</option>").append("\r\n");
				ComparisonTypeHTML.append("</select>").append("\r\n");
				out_html.append(" <td>"+ComparisonTypeHTML+"</td> ").append("\r\n");

				//添加比较值
				//判读列字段类型
				if("0".equals(column_type)) {//文本
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("1".equals(column_type)) {//多行文本
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("2".equals(column_type)) {//数字
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"number\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td>").append("\r\n");
				}else if("3".equals(column_type)) {//文件
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"url\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("4".equals(column_type)) {//图片
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"url\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("5".equals(column_type)) {//多选
					String trNum = z.newNumber();
					out_html.append(" <td>").append("\r\n");
					out_html.append("<input name=\"ComparisonValue\" id=\""+column_obj.getColumn_id()+trNum+"_key\" type=\"hidden\"  value=\""+queryTerms.getComparisonValue()+"\"/>").append("\r\n");
					//获取Code
					if(!"".equals(column_obj.getP_code_id()) && column_obj.getP_code_id()!=null) {
						z_code code = z.code.get(column_obj.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							for (z_code_detail code_d : delailList) {
								out_html.append("<div class=\"form-check float_left\">").append("\r\n");
								boolean ic = isChecked(queryTerms.getComparisonValue(),code_d.getZ_key());
								if(ic) {
									out_html.append("<input id=\""+column_obj.getColumn_id()+trNum+code_d.getZ_key()+"_value\" name=\"ComparisonValue_"+trNum+"_name\" onchange=\"getCheckedValue('"+column_obj.getColumn_id()+trNum+"_key','ComparisonValue_"+trNum+"_name')\" type=\"checkbox\" checked=\"checked\" value=\""+code_d.getZ_key()+"\">").append("\r\n");
								}else {
									out_html.append("<input id=\""+column_obj.getColumn_id()+trNum+code_d.getZ_key()+"_value\" name=\"ComparisonValue_"+trNum+"_name\" onchange=\"getCheckedValue('"+column_obj.getColumn_id()+trNum+"_key','ComparisonValue_"+trNum+"_name')\" type=\"checkbox\"                     value=\""+code_d.getZ_key()+"\">").append("\r\n");
								}
								out_html.append("<label class=\"form-check-label\" for=\""+column_obj.getColumn_id()+trNum+code_d.getZ_key()+"_value\">"+code_d.getZ_value()+"</label>").append("\r\n");
								out_html.append("</div>").append("\r\n");
							}
						}else {
							out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
						}
					}else {
						out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
					}
					out_html.append("</td> ").append("\r\n");
				}else if("6".equals(column_type)) {//单选
					out_html.append(" <td>").append("\r\n");
					//获取Code
					if(!"".equals(column_obj.getP_code_id()) && column_obj.getP_code_id()!=null) {
						z_code code = z.code.get(column_obj.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							for (z_code_detail code_d : delailList) {
								out_html.append("<input name=\"ComparisonValue\" type=\"radio\" "+isRadioChecked(queryTerms.getComparisonValue(),code_d.getZ_key())+" value=\""+code_d.getZ_key()+"\"> "+code_d.getZ_value()+" ").append("\r\n");
							}
						}else {
							out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
						}
					}else {
						out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
					}
					out_html.append("</td> ").append("\r\n");
				}else if("7".equals(column_type)) {//下拉框
					out_html.append(" <td>").append("\r\n");
					//获取Code
					if(!"".equals(column_obj.getP_code_id()) && column_obj.getP_code_id()!=null) {
						z_code code = z.code.get(column_obj.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							out_html.append(" <select name=\"ComparisonValue\" class=\"form-control form-control-sm\">").append("\r\n");
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							for (z_code_detail code_d : delailList) {
								out_html.append("<option value=\""+code_d.getZ_key()+"\" "+isSelected(queryTerms.getComparisonValue(),code_d.getZ_key())+">"+code_d.getZ_value()+"</option>").append("\r\n");
							}
							out_html.append("</select> ").append("\r\n");
						}else {
							out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
						}
					}else {
						out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
					}
					out_html.append("</td> ").append("\r\n");
				}else if("8".equals(column_type)) {//Z5
					String trNum = z.newNumber();
					String SelectDataOnClick = "onclick=\"Z5list('"+column_obj.getZ5_table()+"_"+column_obj.getColumn_id()+"','"+column_obj.getColumn_id() + trNum+"_key','"+column_obj.getColumn_id() + trNum+"_display',1)\"";
					out_html.append("<td>").append("\r\n");
					out_html.append("<div class=\"input-group input-group-sm\">").append("\r\n");
					out_html.append("<input id=\""+column_obj.getColumn_id() + trNum+"_key\" name=\"ComparisonValue\" type=\"hidden\"  value=\""+queryTerms.getComparisonValue()+"\"/>").append("\r\n");
					out_html.append("<input readonly=\"true\" id=\""+column_obj.getColumn_id() + trNum+"_display\"  "+SelectDataOnClick+" value=\""+getZ5DisplayValueR(column_obj,queryTerms.getComparisonValue())+"\" type=\"text\" class=\"form-control form-control-sm\" />").append("\r\n");
					out_html.append("<span class=\"input-group-append\">").append("\r\n");
					out_html.append("<button class=\"btn btn-light\" "+SelectDataOnClick+" type=\"button\"><i class=\"fa fa-search\"></i></button>").append("\r\n");
					out_html.append("</span>").append("\r\n");
					out_html.append("</div>").append("\r\n");
					out_html.append("</td>").append("\r\n");
				}else if("9".equals(column_type)) {//日期
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"Wdate form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" /></td> ").append("\r\n");
				}else if("10".equals(column_type)) {//日期时间
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"Wdate form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" /></td> ").append("\r\n");
				}else if("11".equals(column_type)) {//HTML输入框
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else if("12".equals(column_type)) {//源码输入框
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}else {
					out_html.append(" <td><input name=\"ComparisonValue\" type=\"text\" class=\"form-control form-control-sm\" value=\""+queryTerms.getComparisonValue()+"\"/></td> ").append("\r\n");
				}

				//添加连接符
				StringBuffer ConnectorTypeHTML = new StringBuffer();
				ConnectorTypeHTML.append("<select name=\"ConnectorType\" class=\"form-control form-control-sm\">").append("\r\n");
				ConnectorTypeHTML.append("<option value=\"0\" "+isSelected("0",queryTerms.getConnectorType())+">&nbsp;</option>").append("\r\n");
				ConnectorTypeHTML.append("<option value=\"1\" "+isSelected("1",queryTerms.getConnectorType())+">并且</option>").append("\r\n");
				ConnectorTypeHTML.append("<option value=\"2\" "+isSelected("2",queryTerms.getConnectorType())+">或者</option>").append("\r\n");
				ConnectorTypeHTML.append("</select>").append("\r\n");
				out_html.append(" <td>"+ConnectorTypeHTML+"</td> ").append("\r\n");

				//添加删除按键
				out_html.append(" <td><button type=\"button\" class=\"btn btn-light\" onclick=\"DeleteQueryRom(this)\"><i class=\"fa fa-trash-o\"></i></button></td> ").append("\r\n");

				out_html.append(" </tr> ").append("\r\n");
			}
		}
		out_html.append(" </tbody> ").append("\r\n");
		out_html.append(" </table> ").append("\r\n");


		out_html.append(" </div> ").append("\r\n");
		out_html.append(" <div data-options=\"region:'south'\" style=\"height:60px; text-align:center;padding-top: 10px\"> ").append("\r\n");
		if("1".equals(parameter.get("is_mobile"))) {
			out_html.append(" <button type=\"button\" class=\"btn btn-light\" onclick=\"$('#select_windows').window('close');\"><i class=\"fa fa-window-close\"></i> 关闭</button>").append("\r\n");
		}
		out_html.append(" <button type=\"button\" class=\"btn btn-light\" onclick=\"form_clear()\"><i class=\"fa fa-trash-o\"></i> 清空</button>  ").append("\r\n");
		out_html.append(" <button type=\"button\" class=\"btn btn-light\" onclick=\"form_query(1)\"><i class=\"fa fa-search\"></i> 查询</button> ").append("\r\n");
		out_html.append(" </div> ").append("\r\n");
		out_html.append(" </div> ").append("\r\n");
		out_html.append(" </div> ").append("\r\n");
	}

	/**
	 * 创建表格代码
	 * @param out_html
	 * @throws JspException 
	 */
	protected void CreateTableHtml(StringBuffer out_html,List<z_form_table_column> columnList,List<HashMap<String,String>> list,HashMap<String,String> parameter) throws Exception {
		out_html.append("<table id=\"MainTable\" class=\"easyui-datagrid\" data-options=\"fit:true,ctrlSelect:true,resizeEdge:20,nowrap:false,onDblClickCell:DblClickList\">").append("\r\n");
		out_html.append("<thead data-options=\"frozen:true\">").append("\r\n");
		out_html.append("<tr>").append("\r\n");
		out_html.append("<th data-options=\"field:'zid',checkbox:true\">主键</th>").append("\r\n");
		out_html.append("</tr>").append("\r\n");
		out_html.append("</thead>").append("\r\n");
		out_html.append("<thead>").append("\r\n");
		out_html.append("<tr>").append("\r\n");
		//循环所有表字段。生成表头
		for (z_form_table_column column : columnList) {
			if(!"zid".equals(column.getColumn_id()) && !"pid".equals(column.getColumn_id())){
				if("0".equals(column.getIs_hidden())) {//判读是否隐藏
					if(!"1".equals(column.getIs_hidden_list())){//判读是否列表隐藏


						String sortThSymbol = SetSortTH(column.getColumn_id(),parameter.get("orderby"),parameter.get("orderby_pattern"));

						if(!"".equals(column.getColunm_length_list()) && column.getColunm_length_list()!=null && !"null".equals(column.getColunm_length_list())) {
							out_html.append("<th  data-options=\"field:'"+column.getColumn_id()+"',width:"+column.getColunm_length_list()+"\"><button type=\"button\" onClick=\"SortList('"+column.getColumn_id()+"')\" class=\"btn btn-sm btn-light btn-block\">"+column.getColumn_name()+sortThSymbol+"</button></th>").append("\r\n");
						}else {
							out_html.append("<th  data-options=\"field:'"+column.getColumn_id()+"'\"><button type=\"button\" class=\"btn btn-sm btn-light btn-block\" onClick=\"SortList('"+column.getColumn_id()+"')\">"+column.getColumn_name()+sortThSymbol+"</button></th>").append("\r\n");
						}
					}
				}

			}
		}
		out_html.append("</tr>").append("\r\n");
		out_html.append("</thead>").append("\r\n");
		out_html.append("<tbody>").append("\r\n");
		try {
			//循环查询结果集List  
			for (HashMap<String,String> bean : list) {

				out_html.append("<tr>").append("\r\n");
				out_html.append("<td>"+bean.get("zid")+"</td>").append("\r\n");
				//循环表字段
				for (z_form_table_column column : columnList) {
					if(!"zid".equals(column.getColumn_id()) && !"pid".equals(column.getColumn_id())){

						if("0".equals(column.getIs_hidden())) {//判读是否隐藏

							if(!"1".equals(column.getIs_hidden_list())){//判读是否列表隐藏

								if("0".equals(column.getColumn_type())) {//文本
									out_html.append("<td>"+printTd(StringUtil.toString(bean.get(column.getColumn_id())),column,bean)+"</td>").append("\r\n");
								}else if("1".equals(column.getColumn_type())) {//多行文本
									out_html.append("<td>"+printTd(StringUtil.toString(bean.get(column.getColumn_id())),column,bean)+"</td>").append("\r\n");
								}else if("2".equals(column.getColumn_type())) {//数字
									out_html.append("<td>"+printTd(StringUtil.toString(bean.get(column.getColumn_id())),column,bean)+"</td>").append("\r\n");
								}else if("3".equals(column.getColumn_type())) {//文件
									//if(bean.get(column.getColumn_id())!=null && "".equals(bean.get(column.getColumn_id()))) {
									if(z.isNotNull(bean.get(column.getColumn_id()))) {
										out_html.append("<td><a class=\"btn btn-outline-secondary btn-sm\" href=\""+StringUtil.toString(bean.get(column.getColumn_id()))+"\" download=\"\">文件下载<a></td>").append("\r\n");
									}else {
										out_html.append("<td>"+StringUtil.toString(bean.get(column.getColumn_id()))+"</td>").append("\r\n");
									}
								}else if("4".equals(column.getColumn_type())) {//图片
									if(z.isNotNull(bean.get(column.getColumn_id()))) {
										//if(bean.get(column.getColumn_id())!=null || "".equals(bean.get(column.getColumn_id()))) {
										out_html.append("<td><img src=\""+StringUtil.toString(bean.get(column.getColumn_id()))+"\" style=\"height:32px;\" onclick=\"lookupImg2('"+StringUtil.toString(bean.get(column.getColumn_id()))+"')\"></td>").append("\r\n");
									}else {
										out_html.append("<td>"+StringUtil.toString(bean.get(column.getColumn_id()))+"</td>").append("\r\n");
									}
								}else if("5".equals(column.getColumn_type())) {//多选
									String CheckedValue = CheckedValue(column,StringUtil.toString(bean.get(column.getColumn_id())));
									out_html.append("<td>"+printTd(CheckedValue,column,bean)+"</td>").append("\r\n");
								}else if("6".equals(column.getColumn_type())) {//单选
									String cv = z.codeValue.get(column.getP_code_id()+"_"+bean.get(column.getColumn_id()));
									if("".equals(cv) || cv==null) {
										cv = StringUtil.toString(bean.get(column.getColumn_id()));
									}
									out_html.append("<td>"+  CodeValueColor(column.getP_code_id()+"_"+bean.get(column.getColumn_id()),printTd(cv,column,bean))+"</td>").append("\r\n");
								}else if("7".equals(column.getColumn_type())) {//下拉框
									String cv = z.codeValue.get(column.getP_code_id()+"_"+bean.get(column.getColumn_id()));
									if("".equals(cv) || cv==null) {
										cv = StringUtil.toString(bean.get(column.getColumn_id()));
									}
									out_html.append("<td>"+  CodeValueColor(column.getP_code_id()+"_"+bean.get(column.getColumn_id()),printTd(cv,column,bean))+"</td>").append("\r\n");
								}else if("8".equals(column.getColumn_type())) {//Z5
									String columnValue = StringUtil.toString(bean.get(column.getColumn_id()));
									out_html.append("<td>"+printTd(getZ5DisplayValue(column,columnValue),column,bean)+"</td>").append("\r\n");
								}else if("9".equals(column.getColumn_type())) {//日期
									out_html.append("<td>"+printTd(DateUtil.FormatDate(bean.get(column.getColumn_id()), "yyyy-MM-dd"),column,bean)+"</td>").append("\r\n");
								}else if("10".equals(column.getColumn_type())) {//日期时间
									out_html.append("<td>"+printTd(DateUtil.FormatDate(bean.get(column.getColumn_id()), "yyyy-MM-dd HH:mm:ss"),column,bean)+"</td>").append("\r\n");
								}else if("11".equals(column.getColumn_type())) {//HTML输入框
									out_html.append("<td>"+StringUtil.toString(bean.get(column.getColumn_id()))+"</td>").append("\r\n");
								}else if("12".equals(column.getColumn_type())) {//源码输入框
									out_html.append("<td>"+StringUtil.toString(bean.get(column.getColumn_id()))+"</td>").append("\r\n");
								}
							}


						}

					}
				}
				out_html.append("</tr>").append("\r\n");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		out_html.append("</tbody>").append("\r\n");
		out_html.append("</table>").append("\r\n");
	}

	/**
	 * 获取Code显示颜色
	 * @param p_code_id 代码编号
	 * @return 文字颜色CSS代码
	 */
	protected String CodeValueColor(String codeid_key,String displayValue) {
		z_code_detail  zcd = z.code_detail.get(codeid_key);
		if(z.isNotNull(zcd) && z.isNotNull(zcd.getDisplay_color())) {
			return "<span style=\"color:#"+zcd.getDisplay_color()+";\">"+displayValue+"</span>";
		}else {
			return displayValue;
		}
	}

	/**
	 * 显示TD
	 * @param column
	 * @param bean
	 * @return
	 */
	protected String printTd(String displayValue,z_form_table_column column, HashMap<String, String> bean) {
		String tdValue = "";
		String tr_href = column.getTr_href();//获取列表行链接
		if(z.isNotNull(tr_href)) {
			//解析链接变量
			String tr_href_parse_value = StringUtil.parseExpression(tr_href, bean);
			//如果链接中包括http，自动以新窗口找开
			String target = "";
			if(tr_href_parse_value.toLowerCase().indexOf("http")>=0) {
				target = "target='_blank'";
			}
			tdValue = "<a href='"+tr_href_parse_value+"' class='TableUrlA' "+target+" >"+displayValue+"</a>";
		}else {
			if(z.isNotNull(displayValue)) {
				tdValue = displayValue;	
			}else {
				tdValue = "";
			}
		}
		return tdValue;
	}


	/**
	 * 显示TD
	 * @param column
	 * @param bean
	 * @return
	 */
	private String printTdR(String displayValue,z_report_column column, HashMap<String, String> bean) {
		String tdValue = "";
		String tr_href = column.getTr_href();//获取列表行链接
		if(z.isNotNull(tr_href)) {
			//解析链接变量
			String tr_href_parse_value = StringUtil.parseExpression(tr_href, bean);
			//如果链接中包括http，自动以新窗口找开
			String target = "";
			if(tr_href_parse_value.toLowerCase().indexOf("http")>=0) {
				target = "target='_blank'";
			}
			tdValue = "<a href='"+tr_href_parse_value+"' class='TableUrlA' "+target+" >"+displayValue+"</a>";
		}else {
			if(z.isNotNull(displayValue)) {
				tdValue = displayValue;	
			}else {
				tdValue = "";
			}
		}
		return tdValue;
	}

	/**
	 * 生成升序降序图标
	 * @param column_id
	 * @param sort_column_id
	 * @param orderby_pattern
	 * @return
	 */
	private String SetSortTH(String column_id, String sort_column_id, String orderby_pattern) {
		String sort_img = "";
		if(column_id.equals(sort_column_id)) {
			if("desc".equals(orderby_pattern)) {
				//返回降序图标
				sort_img = "<i class='fa fa-arrow-down'></i>";
			}else {
				//返回升序图标
				sort_img = "<i class='fa fa-arrow-up'></i>";
			}
		}
		return sort_img;
	}

	/**
	 * 创建表格代码
	 * @param out_html
	 * @throws JspException 
	 */
	protected void CreateTableHtmlR(StringBuffer out_html,List<z_report_column> columnList,List<HashMap<String,String>> list,HashMap<String,String> parameter) throws Exception {
		out_html.append("<table id=\"MainTable\" class=\"easyui-datagrid\" data-options=\"fit:true,ctrlSelect:true,resizeEdge:20,nowrap:false\">").append("\r\n");
		out_html.append("<thead data-options=\"frozen:true\">").append("\r\n");
		out_html.append("<tr>").append("\r\n");
		out_html.append("<th data-options=\"field:'zid',checkbox:true\"></th>").append("\r\n");
		out_html.append("</tr>").append("\r\n");
		out_html.append("</thead>").append("\r\n");
		out_html.append("<thead>").append("\r\n");
		out_html.append("<tr>").append("\r\n");
		//循环所有表字段。生成表头
		for (z_report_column column : columnList) {
			if(!"zid".equals(column.getColumn_id()) && !"pid".equals(column.getColumn_id())){

				if(!"1".equals(column.getIs_hidden_list())){//判读是否列表隐藏
					String sortThSymbol = SetSortTH(column.getColumn_id(),parameter.get("orderby"),parameter.get("orderby_pattern"));
					if(!"".equals(column.getColunm_length_list()) && column.getColunm_length_list()!=null && !"null".equals(column.getColunm_length_list())) {
						out_html.append("<th  data-options=\"field:'"+column.getColumn_id()+"',width:"+column.getColunm_length_list()+"\"><button type=\"button\" onClick=\"SortList('"+column.getColumn_id()+"')\" class=\"btn btn-sm btn-light btn-block\">"+column.getColumn_name()+sortThSymbol+"</button></th>").append("\r\n");
					}else {
						out_html.append("<th  data-options=\"field:'"+column.getColumn_id()+"'\"><button type=\"button\" class=\"btn btn-sm btn-light btn-block\" onClick=\"SortList('"+column.getColumn_id()+"')\">"+column.getColumn_name()+sortThSymbol+"</button></th>").append("\r\n");
					}
				}

			}
		}
		out_html.append("</tr>").append("\r\n");
		out_html.append("</thead>").append("\r\n");
		out_html.append("<tbody>").append("\r\n");
		try {
			//循环查询结果集List  
			for (HashMap<String,String> bean : list) {

				out_html.append("<tr>").append("\r\n");
				out_html.append("<td>"+bean.get("zid")+"</td>").append("\r\n");
				//循环表字段
				for (z_report_column column : columnList) {
					if(!"zid".equals(column.getColumn_id()) && !"pid".equals(column.getColumn_id())){

						if(!"1".equals(column.getIs_hidden_list())){//判读是否列表隐藏

							if("0".equals(column.getColumn_type())) {//文本
								out_html.append("<td>"+printTdR(StringUtil.toString(bean.get(column.getColumn_id())),column,bean)+"</td>").append("\r\n");
							}else if("1".equals(column.getColumn_type())) {//多行文本
								out_html.append("<td>"+printTdR(StringUtil.toString(bean.get(column.getColumn_id())),column,bean)+"</td>").append("\r\n");
							}else if("2".equals(column.getColumn_type())) {//数字
								out_html.append("<td>"+printTdR(StringUtil.toString(bean.get(column.getColumn_id())),column,bean)+"</td>").append("\r\n");
							}else if("3".equals(column.getColumn_type())) {//文件
								if(z.isNotNull(bean.get(column.getColumn_id()))) {
									//if(bean.get(column.getColumn_id())!=null || "".equals(bean.get(column.getColumn_id()))) {
									out_html.append("<td><a class=\"btn btn-outline-secondary btn-sm\" href=\""+StringUtil.toString(bean.get(column.getColumn_id()))+"\" download=\"\">文件下载<a></td>").append("\r\n");
								}else {
									out_html.append("<td>"+StringUtil.toString(bean.get(column.getColumn_id()))+"</td>").append("\r\n");
								}
							}else if("4".equals(column.getColumn_type())) {//图片
								if(z.isNotNull(bean.get(column.getColumn_id()))) {
									//if(bean.get(column.getColumn_id())!=null || "".equals(bean.get(column.getColumn_id()))) {
									out_html.append("<td><img src=\""+StringUtil.toString(bean.get(column.getColumn_id()))+"\" style=\"height:32px;\" onclick=\"lookupImg2('"+StringUtil.toString(bean.get(column.getColumn_id()))+"')\"></td>").append("\r\n");
								}else {
									out_html.append("<td>"+StringUtil.toString(bean.get(column.getColumn_id()))+"</td>").append("\r\n");
								}
							}else if("5".equals(column.getColumn_type())) {//多选
								String CheckedValue = CheckedValueR(column,StringUtil.toString(bean.get(column.getColumn_id())));
								out_html.append("<td>"+printTdR(CheckedValue,column,bean)+"</td>").append("\r\n");
							}else if("6".equals(column.getColumn_type())) {//单选
								String cv = z.codeValue.get(column.getP_code_id()+"_"+bean.get(column.getColumn_id()));
								if("".equals(cv) || cv==null) {
									cv = StringUtil.toString(bean.get(column.getColumn_id()));
								}
								out_html.append("<td>"+printTdR(cv,column,bean)+"</td>").append("\r\n");
							}else if("7".equals(column.getColumn_type())) {//下拉框
								String cv = z.codeValue.get(column.getP_code_id()+"_"+bean.get(column.getColumn_id()));
								if("".equals(cv) || cv==null) {
									cv = StringUtil.toString(bean.get(column.getColumn_id()));
								}
								out_html.append("<td>"+printTdR(cv,column,bean)+"</td>").append("\r\n");
							}else if("8".equals(column.getColumn_type())) {//Z5
								String columnValue = StringUtil.toString(bean.get(column.getColumn_id()));
								out_html.append("<td>"+printTdR(getZ5DisplayValueR(column,columnValue),column,bean)+"</td>").append("\r\n");
							}else if("9".equals(column.getColumn_type())) {//日期
								out_html.append("<td>"+printTdR(DateUtil.FormatDate(bean.get(column.getColumn_id()), "yyyy-MM-dd"),column,bean)+"</td>").append("\r\n");
							}else if("10".equals(column.getColumn_type())) {//日期时间
								out_html.append("<td>"+printTdR(DateUtil.FormatDate(bean.get(column.getColumn_id()), "yyyy-MM-dd HH:mm:ss"),column,bean)+"</td>").append("\r\n");
							}else if("11".equals(column.getColumn_type())) {//HTML输入框
								out_html.append("<td>"+StringUtil.toString(bean.get(column.getColumn_id()))+"</td>").append("\r\n");
							}else if("12".equals(column.getColumn_type())) {//源码输入框
								out_html.append("<td>"+StringUtil.toString(bean.get(column.getColumn_id()))+"</td>").append("\r\n");
							}
						}

					}
				}
				out_html.append("</tr>").append("\r\n");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		out_html.append("</tbody>").append("\r\n");
		out_html.append("</table>").append("\r\n");
	}

	/**
	 * 创建分页代码
	 * @param out_html
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws Exception 
	 */
	protected void CreatePagingHtml(String pageType,HashMap<String,String> parameter,StringBuffer out_html) throws Exception{
		//页号
		int rownum = 1;
		if(z.isNotNull(parameter.get("pagenum"))) {
			rownum = new Integer(parameter.get("pagenum"));
		}

		int rowcount = new Integer(z.sp.get("rowcount"));
		if(z.isNotNull(parameter.get("rowcount"))) {
			rowcount = new Integer(parameter.get("rowcount"));
		}

		//页数
		int page_count = 1;
		if(z.isNotNull(parameter.get("pagecount"))) {
			page_count = new Integer(parameter.get("pagecount"));
		}

		//记录数
		int alldatacount = 0;
		if(z.isNotNull(parameter.get("datacount"))) {
			alldatacount = new Integer(parameter.get("datacount"));
		}

		out_html.append("<ul class=\"pagination \">").append("\r\n");
		//上一页按钮,如果页号为1，把上一页只读
		if(rownum<=1){
			if(!"1".equals(parameter.get("is_mobile"))) {
				out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">首页</a></li>").append("\r\n");
			}

			out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">上一页</a></li>").append("\r\n");
		}else{
			if(!"1".equals(parameter.get("is_mobile"))) {
				out_html.append("<li class=\"page-item \"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query(1)\">首页</a></li>").append("\r\n");
			}

			out_html.append("<li class=\"page-item \"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+(rownum-1)+")\">上一页</a></li>").append("\r\n");
		}

		if(!"1".equals(parameter.get("is_mobile"))) {
			//如果总页数大于5
			if(page_count>6){
				//如果页号大于6
				if(rownum>6){
					out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+1+")\">1</a></li> ").append("\r\n");
					out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">...</a></li> ").append("\r\n");
				}
				if(rownum>6){
					for (int i = rownum-3; i <= rownum+3; i++) {
						if(i==rownum){
							out_html.append("<li class=\"page-item active\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+i+")\"  >"+i+"</a></li> ").append("\r\n");
						}else{
							if(i<=page_count){
								out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+i+")\">"+i+"</a></li> ").append("\r\n");
							}
						}
					}
				}else{
					for (int i = 1; i <= 7; i++) {
						if(i==rownum){
							out_html.append("<li class=\"page-item active\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+i+")\"  >"+i+"</a></li> ").append("\r\n");
						}else{
							out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+i+")\">"+i+"</a></li> ").append("\r\n");
						}
					}
				}
				if(page_count>7 && page_count-rownum>3){
					out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">...</a></li> ").append("\r\n");
					out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+page_count+")\">"+page_count+"</a></li> ").append("\r\n");
				}
			}else{
				//如果总页数小于等于5
				for (int i = 1; i <= page_count; i++) {
					if(i==rownum){
						out_html.append("<li class=\"page-item active\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+i+")\"  >"+i+"</a></li> ").append("\r\n");
					}else{
						out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+i+")\"  >"+i+"</a></li> ").append("\r\n");
					}

				}
			}
		}


		//下一页按钮
		if(rownum>=page_count){
			out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">下一页</a></li>").append("\r\n");
			if(!"1".equals(parameter.get("is_mobile"))) {
				out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">末页</a></li>").append("\r\n");
			}
		}else{
			out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+(rownum+1)+")\">下一页</a></li> ").append("\r\n");
			if(!"1".equals(parameter.get("is_mobile"))) {
				out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"form_query("+page_count+")\">末页</a></li> ").append("\r\n");
			}
		}
		out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">"+rownum+"-"+page_count+"页/"+alldatacount+"条</a></li>").append("\r\n");

		//添加功能按钮
		if("list".equals(pageType)) {
			if(!"1".equals(parameter.get("is_mobile"))) {
				out_html.append("<li class=\"page-item disabled\">&nbsp;&nbsp;&nbsp;</li>").append("\r\n");
				out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">跳到</a></li>").append("\r\n");
				out_html.append("<li class=\"page-item\"><input type=\"text\" class=\"page-link\" style=\"width:40px;\" id=\"JumpRownumId\"></li>").append("\r\n");
				out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">页</a></li>").append("\r\n");
				out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"JumpRow()\">&nbsp;<i class=\"fa fa-paper-plane-o\"></i>&nbsp;跳转</a></li>").append("\r\n");
				out_html.append("<li class=\"page-item disabled\">&nbsp;&nbsp;&nbsp;</li>").append("\r\n");
				out_html.append("<li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">每页显示:</a></li>").append("\r\n");
				if(rowcount==50 || rowcount==9999999) {
					String default_rowcount = z.sp.get("rowcount");
					//获取页面默认显示行数
					z_form_table table = z.tables.get(parameter.get("tableId"));
					if(z.isNotNull(table) && z.isNotNull(table.getDefault_rowcount())) {
						default_rowcount = table.getDefault_rowcount();
					}
					out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"setRowCount("+default_rowcount+")\">"+default_rowcount+"</a></li>").append("\r\n");
				}else {
					out_html.append("<li class=\"page-item active\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"setRowCount("+rowcount+")\">"+rowcount+"</a></li>").append("\r\n");
				}
				if(rowcount==50) {
					out_html.append("<li class=\"page-item active\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"setRowCount(50)\">50</a></li>").append("\r\n");
				}else {
					out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"setRowCount(50)\">50</a></li>").append("\r\n");
				}
				if(rowcount==9999999) {
					out_html.append("<li class=\"page-item active\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"setRowCount(9999999)\">全部</a></li>").append("\r\n");
				}else {
					out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"setRowCount(9999999)\">全部</a></li>").append("\r\n");
				}
				out_html.append("<li class=\"page-item disabled\">&nbsp;&nbsp;&nbsp;</li>").append("\r\n");
				out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"ExportExcel()\">&nbsp;<i class=\"fa fa-download\"></i>&nbsp;导出</a></li>").append("\r\n");
			}
			out_html.append("<li class=\"page-item\"><a class=\"page-link\" href=\"javascript:void(0);\" onclick=\"RefreshList()\">&nbsp;<i class=\"fa fa-refresh\"></i>&nbsp;刷新</a></li>").append("\r\n");

		}
		out_html.append("</ul>").append("\r\n");
	}




	/**
	 * 输出HTML
	 * @param html
	 * @throws Exception 
	 */
	protected void outHTML(String html){
		//输出前台
		try {
			JspWriter out = pageContext.getOut();
			out.println(html);
		} catch (IOException e) {
			z.Error("自定义标签输出异常："+html,e);
		}
	}

	/**
	 * 获取前台标签体信息
	 * @return
	 */
	protected String getInfo(){
		String info = "";
		//获取前台标签体信息
		BodyContent bodycontent = getBodyContent();
		if(bodycontent!=null){
			if(!"".equals(bodycontent.getString()) && bodycontent.getString()!=null){
				info = bodycontent.getString();
			}
		}
		return info;
	}


	/**
	 * 获取Bean中的内容
	 * @param bean 
	 * @param column_name
	 * @return
	 * @throws Exception 
	 */
	protected String getColumnValue(HashMap<String,String> bean, String column_name,String editType) throws Exception {
		String result = "";
		try {
			if(bean!=null) {
				result = String.valueOf(bean.get(column_name));
				if(z.isNull(result)) {
					//如果，当前是新增页面，给字段返回默认值，获取字段中的默认值
					if("add".equals(editType)) {
						//获取当前字段
						z_form_table_column column = z.columns.get(bean.get("tableId")+"_"+column_name);
						if("0".equals(column.getColumn_default_type())) {//固定值
							result = column.getColumn_default();
						}else if("1".equals(column.getColumn_default_type())) {//系统时间
							result = DateUtil.getDateTime();
						}else if("2".equals(column.getColumn_default_type())) {//系统日期
							result = DateUtil.getDate();
						}else if("3".equals(column.getColumn_default_type())) {//当前用户
							result = bean.get("session_userid");
						}else if("4".equals(column.getColumn_default_type())) {//当前组织
							result = bean.get("session_orgid");
						}else if("5".equals(column.getColumn_default_type())) {//当前组织
							result = z.newNumber();
						}else if("6".equals(column.getColumn_default_type())) {//当前组织
							result = z.newZid(bean.get("tableId"));
						}else {
							result = "";
						}
					}else {
						result = "";
					}
				}
			}
		} catch (Exception e) {
			z.Error("获取Bean中的内容出错："+e.getMessage());
		}
		return result;
	}


	private String isSelected(String key, String comparisonType) {
		if(key.equals(comparisonType)) {
			return " selected=\"selected\" ";
		}else {
			return "";
		}
	}

	private String isRadioChecked(String key, String comparisonType) {
		if(key.equals(comparisonType)) {
			return " checked=\"checked\" ";
		}else {
			return "";
		}
	}

	/**
	 * 设置是否非空
	 * @param column
	 * @return
	 */
	protected String ColumnIsNull(String  Is_Null) {
		if("1".equals(Is_Null)) {
			return " RedText ";
		}else {
			return "";
		}
	}

	/**
	 * 设置是否只读
	 * @param column
	 * @return
	 */
	protected String ColumnIsReadonly(String  Is_readonly) {
		if("1".equals(Is_readonly)) {
			return " readonly=\"readonly\" ";
		}else {
			return "";
		}
	}

	/**
	 * 设置是否只读
	 * @param column
	 * @return
	 */
	protected String ColumnIsDisabled(String  Is_readonly) {
		if("1".equals(Is_readonly)) {
			return " disabled=\"disabled\" ";
		}else {
			return "";
		}
	}

	/**
	 * 	获取单选多选排列方向
	 * @param arrangement_direction
	 * @return
	 */
	protected String GetArrangementDirectionCss(String arrangement_direction) {
		if("1".equals(arrangement_direction)) {
			return "float_left";
		}else {
			return " ";
		}
	}

	/**
	 * 设置EasyUI中，按钮行所在区域高度
	 * @return
	 */
	protected String getNorthHeight(String is_mobile) {
		String NorthHeight = "38";
		if("1".equals(is_mobile)) {
			NorthHeight = "76";
		}
		return NorthHeight;
	}

	/**
	 * 创建ListSearchBoxForm + 设置快速查询框信息
	 * @param out_html
	 * @param columnList
	 * @param pageType
	 * @param table
	 * @param parameter
	 * @throws Exception
	 */
	protected void CreateListQueryForm(StringBuffer out_html, List<z_form_table_column> columnList, String pageType, z_form_table table, HashMap<String, String> parameter) throws Exception {
		//获取所有列名
		List<String> columninfoList = new ArrayList<String>();
		Map<String,z_form_table_column> columninfoMap = new HashMap<String,z_form_table_column>();
		for (int i = 0; i < columnList.size(); i++) {
			z_form_table_column column = columnList.get(i);
			if(column.getIs_hidden().equals("0")) {//判读是否隐藏
				columninfoList.add(column.getColumn_id() +"☆"+column.getColumn_name());
				columninfoMap.put(column.getColumn_id(), column);
			}
		}
		//列ID加列名转字符器
		String ComparisonColumnIds = StringUtil.ListToString(columninfoList, ",", "", "");
		//获取所有列字段信息JSON
		String ComparisonColumnIdsJson = JsonUtil.getJson(columninfoMap);

		//设置Form Action 调用方法
		String list_action = "Z5list";
		if("list".equals(pageType)) {
			list_action = table.getList_action();
		}
		out_html.append("<form id=\"select_form\" action=\""+list_action+"\" >").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"query_terms_id\" name=\"query_terms\" value='"+parameter.get("query_terms")+"' />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"ComparisonColumnIdsJsonId\" value='"+ComparisonColumnIdsJson+"' />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"ComparisonColumnIdsId\" value='"+ComparisonColumnIds+"' />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"pagenum_id\" name=\"pagenum\" value=\""+parameter.get("pagenum")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"rowcount_id\" name=\"rowcount\" value=\""+parameter.get("rowcount")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"tableId_id\" name=\"tableId\" value=\""+parameter.get("tableId")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"TableColunmId\" name=\"TableColunmId\" value=\""+parameter.get("TableColunmId")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"ReturnKeyColumnId\" name=\"ReturnKeyColumnId\" value=\""+parameter.get("ReturnKeyColumnId")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"ReturnValueColumnId\" name=\"ReturnValueColumnId\" value=\""+parameter.get("ReturnValueColumnId")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"Z5QueryParameters\" name=\"Z5QueryParameters\" value=\""+parameter.get("Z5QueryParameters")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"PageType\" name=\"PageType\" value=\""+pageType+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"zid_id\" name=\"zid\" value=\""+parameter.get("zid")+"\" />").append("\r\n");
		//添加排序字段与排序模式
		out_html.append("<input type=\"hidden\" id=\"orderby_id\" name=\"orderby\" value=\""+parameter.get("orderby")+"\" />").append("\r\n");//排序字段 
		out_html.append("<input type=\"hidden\" id=\"orderby_pattern_id\" name=\"orderby_pattern\" value=\""+parameter.get("orderby_pattern")+"\" />").append("\r\n");//排序模式 asc 升序  desc 降序
		out_html.append("<input type=\"hidden\" id=\"list_dblclick_id\" name=\"list_dblclick\" value=\""+table.getList_dblclick()+"\" />").append("\r\n");//双击行执行动作

		z_form zf = z.form_tableid.get(table.getTable_id());
		if(z.isNotNull(zf) && "1".equals(zf.getIsStartQuickQuery())) {
			//设置快速查询框信息
			CreateSearchBox(out_html,table,pageType,parameter);
		}


		out_html.append("</form>").append("\r\n");

	}


	protected void CreateListQueryFormR(StringBuffer out_html, List<z_report_column> columnList,String pageType,z_report r, HashMap<String, String> parameter) throws Exception {
		//获取所有列名
		List<String> columninfoList = new ArrayList<String>();
		Map<String,z_report_column> columninfoMap = new HashMap<String,z_report_column>();
		for (int i = 0; i < columnList.size(); i++) {
			z_report_column column = columnList.get(i);
			columninfoList.add(column.getColumn_id() +"☆"+column.getColumn_name());
			columninfoMap.put(column.getColumn_id(), column);
		}
		//列ID加列名转字符器
		String ComparisonColumnIds = StringUtil.ListToString(columninfoList, ",", "", "");
		//获取所有列字段信息JSON
		String ComparisonColumnIdsJson = JsonUtil.getJson(columninfoMap);
		//form
		out_html.append("<form id=\"select_form\" action=\""+pageType+"\" >").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"query_terms_id\" name=\"query_terms\" value='"+parameter.get("query_terms")+"' />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"ComparisonColumnIdsJsonId\" value='"+ComparisonColumnIdsJson+"' />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"ComparisonColumnIdsId\" value='"+ComparisonColumnIds+"' />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"pagenum_id\" name=\"pagenum\" value=\""+parameter.get("pagenum")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"rowcount_id\" name=\"rowcount\" value=\""+parameter.get("rowcount")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"tableId_id\" name=\"tableId\" value=\""+r.getReportid()+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"TableColunmId\" name=\"TableColunmId\" value=\""+parameter.get("TableColunmId")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"ReturnKeyColumnId\" name=\"ReturnKeyColumnId\" value=\""+parameter.get("ReturnKeyColumnId")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"ReturnValueColumnId\" name=\"ReturnValueColumnId\" value=\""+parameter.get("ReturnValueColumnId")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"Z5QueryParameters\" name=\"Z5QueryParameters\" value=\""+parameter.get("Z5QueryParameters")+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"PageType\" name=\"PageType\" value=\""+pageType+"\" />").append("\r\n");
		out_html.append("<input type=\"hidden\" id=\"zid_id\" name=\"zid\" value=\""+parameter.get("zid")+"\" />").append("\r\n");
		//添加排序字段与排序模式
		out_html.append("<input type=\"hidden\" id=\"orderby_id\" name=\"orderby\" value=\""+parameter.get("orderby")+"\" />").append("\r\n");//排序字段 
		out_html.append("<input type=\"hidden\" id=\"orderby_pattern_id\" name=\"orderby_pattern\" value=\""+parameter.get("orderby_pattern")+"\" />").append("\r\n");//排序模式 asc 升序  desc 降序


		if("1".equals(r.getIsStartQuickQuery())) {
			//报表设置快速查询框信息
			CreateSearchBoxR(out_html,r,pageType,parameter);
		}

		out_html.append("</form>").append("\r\n");

	}

	/**
	 * 设置快速查询框信息
	 * @param out_html
	 * @param table
	 * @param PageType
	 * @param parameter
	 * @throws Exception
	 */
	protected void CreateSearchBox(StringBuffer out_html,z_form_table table,String PageType,HashMap<String,String> parameter) throws Exception{
		out_html.append("<div id=\"search_div\" class=\"container-fluid mt-3\">").append("\r\n");
		out_html.append("<div class=\"row\">").append("\r\n");
		//遍历所有字段
		for (z_form_table_column column : table.getZ_form_table_column_list()) {
			//判读字段是否是快速查询字段
			if("1".equals(column.getIs_search())) {
				//创建字段
				out_html.append("<div class=\"col-sm-"+column.getColumn_size()+" mb-2 d-flex align-items-start\">").append("\r\n");
				out_html.append("<div class=\"input-group\">").append("\r\n");
				//标题
				out_html.append("<div class=\"input-group-prepend\"><div class=\"input-group-text\">"+column.getColumn_name()+"：</div></div>").append("\r\n");
				if("0".equals(column.getColumn_type())) {//文本
					out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("1".equals(column.getColumn_type())) {//多行文本
					out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("2".equals(column.getColumn_type())) {//数字
					out_html.append("<input placeholder=\"从\" id=\""+column.getColumn_id()+"_id_from\" name=\""+column.getColumn_id()+"_from\" type=\"number\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id()+"_from",PageType)+"\"  />").append("\r\n");
					out_html.append("<input placeholder=\"到\" id=\""+column.getColumn_id()+"_id_to\" name=\""+column.getColumn_id()+"_to\" type=\"number\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id()+"_to",PageType)+"\"  />").append("\r\n");
				}else if("3".equals(column.getColumn_type())) {//文件
					out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("4".equals(column.getColumn_type())) {//图片
					out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("5".equals(column.getColumn_type())) {//多选
					//获取Code
					if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
						z_code code = z.code.get(column.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							out_html.append("<div class=\"zcheckbox_div\">").append("\r\n");
							String value = getColumnValue(parameter,column.getColumn_id(),PageType);
							out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+value+"\"/>").append("\r\n");
							for (z_code_detail code_d : delailList) {
								//如果字段值与Key相同，选中状态
								boolean ic = isChecked(value,code_d.getZ_key());
								String ischecked = "";
								if(ic) {
									ischecked = "checked=\"checked\"";
								}
								//如果只读换成隐藏域
								String type = "checkbox";
								String hidden_ico = "";
								if("1".equals(column.getIs_readonly())) {
									type = "hidden";
									if(z.isNotNull(ischecked)) {
										hidden_ico = "<i class=\"fa fa-check-square-o\"></i> ";
									}else {
										hidden_ico = "<i class=\"fa fa-square-o\"></i> ";
									}
								}
								out_html.append("<div class=\"form-check "+GetArrangementDirectionCss(column.getArrangement_direction())+"\">").append("\r\n");
								out_html.append("	<input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_name')\" type=\""+type+"\" "+ischecked+" name=\""+column.getColumn_id()+"_name\" id=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\" value=\""+code_d.getZ_key()+"\" >").append("\r\n");
								out_html.append("	<label class=\"form-check-label\" for=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\">"+hidden_ico+code_d.getZ_value()+"</label>").append("\r\n");
								out_html.append("</div>").append("\r\n");
								//out_html.append("<div class=\""+GetArrangementDirectionCss(column.getArrangement_direction())+"\"><input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_name')\" type=\""+type+"\" "+ischecked+" id=\""+column.getColumn_id()+"_checked_id\" value=\""+code_d.getZ_key()+"\" >"+hidden_ico+code_d.getZ_value()+" </div>").append("\r\n");
							}
							out_html.append("</div>").append("\r\n");
						}
					}
				}else if("6".equals(column.getColumn_type())) {//单选
					//获取Code
					if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
						z_code code = z.code.get(column.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							out_html.append("<div class=\"zcheckbox_div\" >").append("\r\n");
							for (z_code_detail code_d : delailList) {
								String value = getColumnValue(parameter,column.getColumn_id(),PageType);
								//如果字段值与Key相同，选中状态
								String ischecked = "";
								if(value.equals(code_d.getZ_key())) {
									ischecked = "checked=\"checked\"";
								}
								String type = "radio";
								//如果只读换成隐藏域
								String hidden_ico = "";
								if("1".equals(column.getIs_readonly())) {
									type = "hidden";
									if(z.isNotNull(ischecked)) {
										hidden_ico = "<i class=\"fa fa-dot-circle-o\"></i> ";
									}else {
										hidden_ico = "<i class=\"fa fa-circle-o\"></i> ";
									}
								}
								out_html.append("<div class=\"form-check "+GetArrangementDirectionCss(column.getArrangement_direction())+"\">").append("\r\n");
								out_html.append("	<input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+code_d.getZ_key()+"_checked_id')\" type=\""+type+"\" "+ischecked+" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\" value=\""+code_d.getZ_key()+"\" >").append("\r\n");
								out_html.append("	<label class=\"form-check-label\" for=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\">"+hidden_ico+code_d.getZ_value()+"</label>").append("\r\n");
								out_html.append("</div>").append("\r\n");
							}
							out_html.append("</div>").append("\r\n");
						}

					}
				}else if("7".equals(column.getColumn_type())) {//下拉框
					out_html.append("<select class=\"form-control\" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" >").append("\r\n");
					out_html.append("<option value=\"\">请选择</option>").append("\r\n");
					//获取Code
					if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
						z_code code = z.code.get(column.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							for (z_code_detail code_d : delailList) {
								String value = getColumnValue(parameter,column.getColumn_id(),PageType);
								if(value.equals(code_d.getZ_key())) {
									out_html.append("<option value=\""+code_d.getZ_key()+"\" selected=\"selected\">"+code_d.getZ_value()+"</option>").append("\r\n");
								}else {
									out_html.append("<option value=\""+code_d.getZ_key()+"\" >"+code_d.getZ_value()+"</option>").append("\r\n");
								}
							}
						}
					}
					out_html.append("</select>").append("\r\n");
				}else if("8".equals(column.getColumn_type())) {//Z5
					String ColumnValue = getColumnValue(parameter,column.getColumn_id(),PageType);
					String SelectDataOnClick = "onclick=\"Z5list('"+table.getTable_id()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display',0)\"";
					//如果字段是只读，删除输入框点击事件
					if("1".equals(column.getIs_readonly())) {
						SelectDataOnClick = "";
					}
					out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+ColumnValue+"\"/>").append("\r\n");
					out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_display\" "+SelectDataOnClick+" name=\""+column.getColumn_id()+"_display\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"text\" class=\"form-control marginleft1 border-right-0\" "+" readonly />").append("\r\n");
					out_html.append("<span class=\"input-group-prepend marginright1 border-top border-right border-bottom\">").append("\r\n");
					if(!"look".equals(PageType)) {
						out_html.append("<button class=\"btn btn-light\" onclick=\"Z5Clear('"+table.getTable_id()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display')\" type=\"button\" "+ColumnIsDisabled(column.getIs_readonly())+"><i class=\"fa fa-trash-o\"></i></button>").append("\r\n");
						out_html.append("<button id=\""+column.getColumn_id()+"_selectbutton_id\" class=\"btn btn-light\" "+SelectDataOnClick+" type=\"button\" "+ColumnIsDisabled(column.getIs_readonly())+"><i class=\"fa fa-search\"></i></button>").append("\r\n");
					}
					out_html.append("</span>").append("\r\n");
				}else if("9".equals(column.getColumn_type())) {//日期
					if("3".equals(column.getCompare())) {
						out_html.append("<input  placeholder=\"从\" id=\""+column.getColumn_id()+"_id_from\" name=\""+column.getColumn_id()+"_from\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id()+"_from",PageType), "yyyy-MM-dd") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" />").append("\r\n");
						out_html.append("<input  placeholder=\"到\" id=\""+column.getColumn_id()+"_id_to\" name=\""+column.getColumn_id()+"_to\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id()+"_to",PageType), "yyyy-MM-dd") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" />").append("\r\n");
					}else {
						out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id(),PageType), "yyyy-MM-dd") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" />").append("\r\n");
					}
				}else if("10".equals(column.getColumn_type())) {//日期时间
					if("3".equals(column.getCompare())) {
						out_html.append("<input  placeholder=\"从\" id=\""+column.getColumn_id()+"_id_from\" name=\""+column.getColumn_id()+"_from\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id()+"_from",PageType), "yyyy-MM-dd HH:mm:ss") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" />").append("\r\n");
						out_html.append("<input  placeholder=\"到\" id=\""+column.getColumn_id()+"_id_to\" name=\""+column.getColumn_id()+"_to\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id()+"_to",PageType), "yyyy-MM-dd HH:mm:ss") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" />").append("\r\n");
					}else {
						out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id(),PageType), "yyyy-MM-dd HH:mm:ss") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" />").append("\r\n");
					}
				}else if("11".equals(column.getColumn_type())) {//HTML输入框
					out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("12".equals(column.getColumn_type())) {//源码输入框
					out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}
				out_html.append("</div>").append("\r\n");
				out_html.append("</div>").append("\r\n");

				//添加换行
				if("1".equals(column.getIsbr())) {
					out_html.append("<div class=\"col-12\"></div>").append("\r\n");
				}
			}

		}
		out_html.append("</div>").append("\r\n");
		out_html.append("<div class=\"row\">").append("\r\n");
		out_html.append("	<div class=\"col-sm-12 mt-4 text-center\">").append("\r\n");
		out_html.append("		<button type=\"button\" class=\"btn btn-secondary\" onclick=\""+table.getTable_id()+"_select_button();\"><i class=\"fa fa fa-tasks\"></i> 高级查询</button>").append("\r\n");
		out_html.append("		<button type=\"button\" class=\"btn btn-secondary\" onclick=\"list_form_reset()\"><i class=\"fa fa-trash-o\"></i> 清空</button>").append("\r\n");
		out_html.append("		<button type=\"button\" class=\"btn btn-secondary\" onclick=\"list_simple_query()\"><i class=\"fa fa-search\"></i> 查询</button>").append("\r\n");
		out_html.append("	</div>").append("\r\n");
		out_html.append("</div>").append("\r\n");
		out_html.append("</div>").append("\r\n");
	}

	private void CreateSearchBoxR(StringBuffer out_html, z_report r, String PageType,HashMap<String, String> parameter) throws Exception {
		out_html.append("<div id=\"search_div\" class=\"container-fluid mt-3\">").append("\r\n");
		out_html.append("<div class=\"row\">").append("\r\n");
		//遍历所有字段
		for (z_report_column column : r.getZ_report_column_list()) {
			//判读字段是否是快速查询字段
			if("1".equals(column.getIs_search())) {
				//创建字段
				out_html.append("<div class=\"col-sm-"+column.getColumn_size()+" mb-2 d-flex align-items-start\">").append("\r\n");
				out_html.append("<div class=\"input-group\">").append("\r\n");
				//标题
				out_html.append("<div class=\"input-group-prepend\"><div class=\"input-group-text\">"+column.getColumn_name()+"：</div></div>").append("\r\n");
				if("0".equals(column.getColumn_type())) {//文本
					out_html.append("<input placeholder=\""+column.getColumn_name()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("1".equals(column.getColumn_type())) {//多行文本
					out_html.append("<input placeholder=\""+column.getColumn_name()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("2".equals(column.getColumn_type())) {//数字
					out_html.append("<input placeholder=\"从\" id=\""+column.getColumn_id()+"_id_from\" name=\""+column.getColumn_id()+"_from\" type=\"number\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id()+"_from",PageType)+"\"  />").append("\r\n");
					out_html.append("<input placeholder=\"到\" id=\""+column.getColumn_id()+"_id_to\" name=\""+column.getColumn_id()+"_to\" type=\"number\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id()+"_to",PageType)+"\"  />").append("\r\n");
				}else if("3".equals(column.getColumn_type())) {//文件
					out_html.append("<input placeholder=\""+column.getColumn_name()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("4".equals(column.getColumn_type())) {//图片
					out_html.append("<input placeholder=\""+column.getColumn_name()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("5".equals(column.getColumn_type())) {//多选
					//获取Code
					if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
						z_code code = z.code.get(column.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							out_html.append("<div class=\"zcheckbox_div\">").append("\r\n");
							String value = getColumnValue(parameter,column.getColumn_id(),PageType);
							out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+value+"\"/>").append("\r\n");
							for (z_code_detail code_d : delailList) {
								//如果字段值与Key相同，选中状态
								boolean ic = isChecked(value,code_d.getZ_key());
								String ischecked = "";
								if(ic) {
									ischecked = "checked=\"checked\"";
								}
								//如果只读换成隐藏域
								String type = "checkbox";
								//								String hidden_ico = "";
								//								if("1".equals(column.getIs_readonly())) {
								//									type = "hidden";
								//									if(z.isNotNull(ischecked)) {
								//										hidden_ico = "<i class=\"fa fa-check-square-o\"></i> ";
								//									}else {
								//										hidden_ico = "<i class=\"fa fa-square-o\"></i> ";
								//									}
								//								}
								out_html.append("<div class=\"form-check float_left\">").append("\r\n");
								out_html.append("	<input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_name')\" type=\""+type+"\" "+ischecked+" name=\""+column.getColumn_id()+"_name\" id=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\" value=\""+code_d.getZ_key()+"\" >").append("\r\n");
								out_html.append("	<label class=\"form-check-label\" for=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\">"+code_d.getZ_value()+"</label>").append("\r\n");
								out_html.append("</div>").append("\r\n");
								//out_html.append("<div class=\""+GetArrangementDirectionCss(column.getArrangement_direction())+"\"><input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_name')\" type=\""+type+"\" "+ischecked+" id=\""+column.getColumn_id()+"_checked_id\" value=\""+code_d.getZ_key()+"\" >"+hidden_ico+code_d.getZ_value()+" </div>").append("\r\n");
							}
							out_html.append("</div>").append("\r\n");
						}
					}
				}else if("6".equals(column.getColumn_type())) {//单选
					//获取Code
					if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
						z_code code = z.code.get(column.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							out_html.append("<div class=\"zcheckbox_div\" >").append("\r\n");
							for (z_code_detail code_d : delailList) {
								String value = getColumnValue(parameter,column.getColumn_id(),PageType);
								//如果字段值与Key相同，选中状态
								String ischecked = "";
								if(value.equals(code_d.getZ_key())) {
									ischecked = "checked=\"checked\"";
								}
								String type = "radio";
								//如果只读换成隐藏域
								//								String hidden_ico = "";
								//								if("1".equals(column.getIs_readonly())) {
								//									type = "hidden";
								//									if(z.isNotNull(ischecked)) {
								//										hidden_ico = "<i class=\"fa fa-dot-circle-o\"></i> ";
								//									}else {
								//										hidden_ico = "<i class=\"fa fa-circle-o\"></i> ";
								//									}
								//								}
								out_html.append("<div class=\"form-check float_left\">").append("\r\n");
								out_html.append("	<input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+code_d.getZ_key()+"_checked_id')\" type=\""+type+"\" "+ischecked+" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\" value=\""+code_d.getZ_key()+"\" >").append("\r\n");
								out_html.append("	<label class=\"form-check-label\" for=\""+column.getColumn_id()+code_d.getZ_key()+"_checked_id\">"+code_d.getZ_value()+"</label>").append("\r\n");
								out_html.append("</div>").append("\r\n");
							}
							out_html.append("</div>").append("\r\n");
						}

					}
				}else if("7".equals(column.getColumn_type())) {//下拉框
					out_html.append("<select class=\"form-control\" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" >").append("\r\n");
					out_html.append("<option value=\"\">请选择</option>").append("\r\n");
					//获取Code
					if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
						z_code code = z.code.get(column.getP_code_id());
						if(code!=null && code.getZ_code_detail_list().size()>0) {
							List<z_code_detail> delailList = code.getZ_code_detail_list();
							for (z_code_detail code_d : delailList) {
								String value = getColumnValue(parameter,column.getColumn_id(),PageType);
								if(value.equals(code_d.getZ_key())) {
									out_html.append("<option value=\""+code_d.getZ_key()+"\" selected=\"selected\">"+code_d.getZ_value()+"</option>").append("\r\n");
								}else {
									out_html.append("<option value=\""+code_d.getZ_key()+"\" >"+code_d.getZ_value()+"</option>").append("\r\n");
								}
							}
						}
					}
					out_html.append("</select>").append("\r\n");
				}else if("8".equals(column.getColumn_type())) {//Z5
					String ColumnValue = getColumnValue(parameter,column.getColumn_id(),PageType);
					String SelectDataOnClick = "onclick=\"Z5list('"+column.getZ5_table()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display',0)\"";
					//如果字段是只读，删除输入框点击事件
					//					if("1".equals(column.getIs_readonly())) {
					//						SelectDataOnClick = "";
					//					}
					out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+ColumnValue+"\"/>").append("\r\n");
					out_html.append("<input placeholder=\""+column.getColumn_name()+"\" id=\""+column.getColumn_id()+"_display\" "+SelectDataOnClick+" name=\""+column.getColumn_id()+"_display\" value=\""+getZ5DisplayValueR(column,ColumnValue)+"\" type=\"text\" class=\"form-control marginleft1 border-right-0\" "+" readonly />").append("\r\n");
					out_html.append("<span class=\"input-group-prepend marginright1 border-top border-right border-bottom\">").append("\r\n");
					if(!"look".equals(PageType)) {
						out_html.append("<button class=\"btn btn-light\" onclick=\"Z5Clear('"+column.getZ5_table()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display')\" type=\"button\"><i class=\"fa fa-trash-o\"></i></button>").append("\r\n");
						out_html.append("<button id=\""+column.getColumn_id()+"_selectbutton_id\" class=\"btn btn-light\" "+SelectDataOnClick+" type=\"button\"><i class=\"fa fa-search\"></i></button>").append("\r\n");
					}
					out_html.append("</span>").append("\r\n");
				}else if("9".equals(column.getColumn_type())) {//日期
					if("3".equals(column.getCompare())) {
						out_html.append("<input  placeholder=\"从\" id=\""+column.getColumn_id()+"_id_from\" name=\""+column.getColumn_id()+"_from\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id()+"_from",PageType), "yyyy-MM-dd") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" />").append("\r\n");
						out_html.append("<input  placeholder=\"到\" id=\""+column.getColumn_id()+"_id_to\" name=\""+column.getColumn_id()+"_to\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id()+"_to",PageType), "yyyy-MM-dd") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" />").append("\r\n");
					}else {
						out_html.append("<input  placeholder=\""+column.getColumn_name()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id(),PageType), "yyyy-MM-dd") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" />").append("\r\n");
					}
				}else if("10".equals(column.getColumn_type())) {//日期时间
					if("3".equals(column.getCompare())) {
						out_html.append("<input  placeholder=\"从\" id=\""+column.getColumn_id()+"_id_from\" name=\""+column.getColumn_id()+"_from\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id()+"_from",PageType), "yyyy-MM-dd HH:mm:ss") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" />").append("\r\n");
						out_html.append("<input  placeholder=\"到\" id=\""+column.getColumn_id()+"_id_to\" name=\""+column.getColumn_id()+"_to\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id()+"_to",PageType), "yyyy-MM-dd HH:mm:ss") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" />").append("\r\n");
					}else {
						out_html.append("<input  placeholder=\""+column.getColumn_name()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+DateUtil.FormatStringDate(getColumnValue(parameter,column.getColumn_id(),PageType), "yyyy-MM-dd HH:mm:ss") +"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" />").append("\r\n");
					}
				}else if("11".equals(column.getColumn_type())) {//HTML输入框
					out_html.append("<input placeholder=\""+column.getColumn_name()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}else if("12".equals(column.getColumn_type())) {//源码输入框
					out_html.append("<input placeholder=\""+column.getColumn_name()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),PageType)+"\" />").append("\r\n");
				}
				out_html.append("</div>").append("\r\n");
				out_html.append("</div>").append("\r\n");

			}

		}
		out_html.append("</div>").append("\r\n");
		out_html.append("<div class=\"row\">").append("\r\n");
		out_html.append("	<div class=\"col-sm-12 mt-4 text-center\">").append("\r\n");
		out_html.append("		<button type=\"button\" class=\"btn btn-secondary\" onclick=\"select_button();\"><i class=\"fa fa fa-tasks\"></i> 高级查询</button>").append("\r\n");
		out_html.append("		<button type=\"button\" class=\"btn btn-secondary\" onclick=\"list_form_reset()\"><i class=\"fa fa-trash-o\"></i> 清空</button>").append("\r\n");
		out_html.append("		<button type=\"button\" class=\"btn btn-secondary\" onclick=\"list_simple_query()\"><i class=\"fa fa-search\"></i> 查询</button>").append("\r\n");
		out_html.append("	</div>").append("\r\n");
		out_html.append("</div>").append("\r\n");
		out_html.append("</div>").append("\r\n");
	}
}
