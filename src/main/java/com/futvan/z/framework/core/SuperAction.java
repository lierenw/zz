package com.futvan.z.framework.core;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.LiteDeviceResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import com.futvan.z.system.zorg.z_org;
import com.futvan.z.system.zuser.z_user;
import com.futvan.z.framework.core.z;

public class SuperAction extends SuperZ{

	@Autowired
	protected HttpServletRequest request;
	
	/**
	 * 获取WebApp路径
	 * @return Path
	 * 获取WebApp目录下的图片
	 * 例：String filePath = getWebAppPath()+"img\\zlogin.png";
	 * 
	 */
	protected String getWebAppPath() {
		return request.getSession().getServletContext().getRealPath(""); 
	}
	
	/**
	 * 判读是否是手机设备访问
	 * @return
	 */
	protected boolean isMobile() {
		boolean result = false;
		if(z.isNotNull(request)) {
			LiteDeviceResolver deviceResolver = new LiteDeviceResolver();
			Device device = deviceResolver.resolveDevice(request);
			if(device.isMobile()) {
				result = true;
			}
		}
		return result;
	}
	
	/**
	 * 	从Session中获取用户ID
	 * 
	 * @param request
	 * @return
	 */
	protected String GetSessionUserId() {
		String userId = "";
		if(request!=null) {
			Object userObj = request.getSession().getAttribute("zuser");
			if(userObj!=null && userObj instanceof z_user) {
				z_user user = (z_user) userObj;
				userId = user.getZid();
			}
		}
		return userId;
	}
	
	/**
	 * 	从Session中获取用户ID
	 * 
	 * @param request
	 * @return
	 */
	protected z_user GetSessionUser() {
		z_user user = null;
		if(request!=null) {
			Object userObj = request.getSession().getAttribute("zuser");
			if(userObj!=null && userObj instanceof z_user) {
				user = (z_user) userObj;
			}
		}
		return user;
	}

	
}
