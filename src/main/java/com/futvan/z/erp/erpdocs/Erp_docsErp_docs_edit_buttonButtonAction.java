package com.futvan.z.erp.erpdocs;
import java.util.HashMap;
import com.futvan.z.framework.core.z;
import com.sun.org.apache.bcel.internal.generic.Select;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
public class Erp_docsErp_docs_edit_buttonButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/erp_docs_edit_button")
	public ModelAndView erp_docs_edit_button(@RequestParam HashMap<String,String> bean) throws Exception {
		ModelAndView mv = null;
		String UserId = getUserId();
		String zid = bean.get("zid");
		//获取文档上传者
		String create_user = commonService.selectString("select create_user from erp_docs where zid = '"+zid+"'");
		if(UserId.equals(create_user)) {
			mv = commonService.edit("common/form/edit",bean,request);
		}else {
			z.Exception("该功能只有上传者可以操作");
		}
		return mv;
	}
}
