package com.futvan.z.erp.erp_account_outdetail;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class erp_account_outdetail extends SuperBean{
	//出账记录
	private String outid;

	//关联入账记录
	private String inid;

	//操作模式
	private String operation_mode;

	//账户类型
	private String a_type;

	//出账操作数量
	private String amount;

	//单位
	private String unit;

	/**
	* get出账记录
	* @return outid
	*/
	public String getOutid() {
		return outid;
  	}

	/**
	* set出账记录
	* @return outid
	*/
	public void setOutid(String outid) {
		this.outid = outid;
 	}

	/**
	* get关联入账记录
	* @return inid
	*/
	public String getInid() {
		return inid;
  	}

	/**
	* set关联入账记录
	* @return inid
	*/
	public void setInid(String inid) {
		this.inid = inid;
 	}

	/**
	* get操作模式
	* @return operation_mode
	*/
	public String getOperation_mode() {
		return operation_mode;
  	}

	/**
	* set操作模式
	* @return operation_mode
	*/
	public void setOperation_mode(String operation_mode) {
		this.operation_mode = operation_mode;
 	}

	/**
	* get账户类型
	* @return a_type
	*/
	public String getA_type() {
		return a_type;
  	}

	/**
	* set账户类型
	* @return a_type
	*/
	public void setA_type(String a_type) {
		this.a_type = a_type;
 	}

	/**
	* get出账操作数量
	* @return amount
	*/
	public String getAmount() {
		return amount;
  	}

	/**
	* set出账操作数量
	* @return amount
	*/
	public void setAmount(String amount) {
		this.amount = amount;
 	}

	/**
	* get单位
	* @return unit
	*/
	public String getUnit() {
		return unit;
  	}

	/**
	* set单位
	* @return unit
	*/
	public void setUnit(String unit) {
		this.unit = unit;
 	}

}
