package com.futvan.z.erp.cms_column;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class cms_column extends SuperBean{
	//上级栏目
	private String parentid;

	//栏目名称
	private String column_name;

	//栏目类型
	private String column_type;

	//是否启用
	private String is_enable;

	//标题1
	private String title1;

	//首图
	private String img;

	//图书说明
	private String info;

	//标题2
	private String title2;

	//标题3
	private String title3;

	//视频
	private String video;

	//是否显示栏目框
	private String is_parent_display;

	//是否新窗口打开
	private String isblank;

	//栏目框规格
	private String box_size;

	//URL
	private String url;

	//语言
	private String lang;

	//栏目附件
	private List<cms_column_detail> cms_column_detail_list;

	/**
	* get上级栏目
	* @return parentid
	*/
	public String getParentid() {
		return parentid;
  	}

	/**
	* set上级栏目
	* @return parentid
	*/
	public void setParentid(String parentid) {
		this.parentid = parentid;
 	}

	/**
	* get栏目名称
	* @return column_name
	*/
	public String getColumn_name() {
		return column_name;
  	}

	/**
	* set栏目名称
	* @return column_name
	*/
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
 	}

	/**
	* get栏目类型
	* @return column_type
	*/
	public String getColumn_type() {
		return column_type;
  	}

	/**
	* set栏目类型
	* @return column_type
	*/
	public void setColumn_type(String column_type) {
		this.column_type = column_type;
 	}

	/**
	* get是否启用
	* @return is_enable
	*/
	public String getIs_enable() {
		return is_enable;
  	}

	/**
	* set是否启用
	* @return is_enable
	*/
	public void setIs_enable(String is_enable) {
		this.is_enable = is_enable;
 	}

	/**
	* get标题1
	* @return title1
	*/
	public String getTitle1() {
		return title1;
  	}

	/**
	* set标题1
	* @return title1
	*/
	public void setTitle1(String title1) {
		this.title1 = title1;
 	}

	/**
	* get首图
	* @return img
	*/
	public String getImg() {
		return img;
  	}

	/**
	* set首图
	* @return img
	*/
	public void setImg(String img) {
		this.img = img;
 	}

	/**
	* get图书说明
	* @return info
	*/
	public String getInfo() {
		return info;
  	}

	/**
	* set图书说明
	* @return info
	*/
	public void setInfo(String info) {
		this.info = info;
 	}

	/**
	* get标题2
	* @return title2
	*/
	public String getTitle2() {
		return title2;
  	}

	/**
	* set标题2
	* @return title2
	*/
	public void setTitle2(String title2) {
		this.title2 = title2;
 	}

	/**
	* get标题3
	* @return title3
	*/
	public String getTitle3() {
		return title3;
  	}

	/**
	* set标题3
	* @return title3
	*/
	public void setTitle3(String title3) {
		this.title3 = title3;
 	}

	/**
	* get视频
	* @return video
	*/
	public String getVideo() {
		return video;
  	}

	/**
	* set视频
	* @return video
	*/
	public void setVideo(String video) {
		this.video = video;
 	}

	/**
	* get是否显示栏目框
	* @return is_parent_display
	*/
	public String getIs_parent_display() {
		return is_parent_display;
  	}

	/**
	* set是否显示栏目框
	* @return is_parent_display
	*/
	public void setIs_parent_display(String is_parent_display) {
		this.is_parent_display = is_parent_display;
 	}

	/**
	* get是否新窗口打开
	* @return isblank
	*/
	public String getIsblank() {
		return isblank;
  	}

	/**
	* set是否新窗口打开
	* @return isblank
	*/
	public void setIsblank(String isblank) {
		this.isblank = isblank;
 	}

	/**
	* get栏目框规格
	* @return box_size
	*/
	public String getBox_size() {
		return box_size;
  	}

	/**
	* set栏目框规格
	* @return box_size
	*/
	public void setBox_size(String box_size) {
		this.box_size = box_size;
 	}

	/**
	* getURL
	* @return url
	*/
	public String getUrl() {
		return url;
  	}

	/**
	* setURL
	* @return url
	*/
	public void setUrl(String url) {
		this.url = url;
 	}

	/**
	* get语言
	* @return lang
	*/
	public String getLang() {
		return lang;
  	}

	/**
	* set语言
	* @return lang
	*/
	public void setLang(String lang) {
		this.lang = lang;
 	}

	/**
	* get栏目附件
	* @return 栏目附件
	*/
	public List<cms_column_detail> getCms_column_detail_list() {
		return cms_column_detail_list;
  	}

	/**
	* set栏目附件
	* @return 栏目附件
	*/
	public void setCms_column_detail_list(List<cms_column_detail> cms_column_detail_list) {
		this.cms_column_detail_list = cms_column_detail_list;
 	}

}
