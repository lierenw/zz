package com.futvan.z.erp.cms_info;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class cms_info extends SuperBean{
	//所属栏目
	private String columnid;

	//类型
	private String cms_type;

	//是否发表
	private String is_publish;

	//是否置顶
	private String istop;

	//标题
	private String title1;

	//首图
	private String img1;

	//视频
	private String video;

	//作者
	private String publish_user;

	//发表时间
	private String publish_time;

	//图文信息
	private String info;

	//副标题1
	private String title2;

	//副标题2
	private String title3;

	//阅读量
	private String reading_amount;

	//点赞量
	private String likes_amount;

	//语言
	private String lang;

	//所属地区
	private String areaid;

	//原链接
	private String from_url;

	//是否新窗口打开
	private String isblank;

	//附件
	private List<cms_info_detail> cms_info_detail_list;

	/**
	* get所属栏目
	* @return columnid
	*/
	public String getColumnid() {
		return columnid;
  	}

	/**
	* set所属栏目
	* @return columnid
	*/
	public void setColumnid(String columnid) {
		this.columnid = columnid;
 	}

	/**
	* get类型
	* @return cms_type
	*/
	public String getCms_type() {
		return cms_type;
  	}

	/**
	* set类型
	* @return cms_type
	*/
	public void setCms_type(String cms_type) {
		this.cms_type = cms_type;
 	}

	/**
	* get是否发表
	* @return is_publish
	*/
	public String getIs_publish() {
		return is_publish;
  	}

	/**
	* set是否发表
	* @return is_publish
	*/
	public void setIs_publish(String is_publish) {
		this.is_publish = is_publish;
 	}

	/**
	* get是否置顶
	* @return istop
	*/
	public String getIstop() {
		return istop;
  	}

	/**
	* set是否置顶
	* @return istop
	*/
	public void setIstop(String istop) {
		this.istop = istop;
 	}

	/**
	* get标题
	* @return title1
	*/
	public String getTitle1() {
		return title1;
  	}

	/**
	* set标题
	* @return title1
	*/
	public void setTitle1(String title1) {
		this.title1 = title1;
 	}

	/**
	* get首图
	* @return img1
	*/
	public String getImg1() {
		return img1;
  	}

	/**
	* set首图
	* @return img1
	*/
	public void setImg1(String img1) {
		this.img1 = img1;
 	}

	/**
	* get视频
	* @return video
	*/
	public String getVideo() {
		return video;
  	}

	/**
	* set视频
	* @return video
	*/
	public void setVideo(String video) {
		this.video = video;
 	}

	/**
	* get作者
	* @return publish_user
	*/
	public String getPublish_user() {
		return publish_user;
  	}

	/**
	* set作者
	* @return publish_user
	*/
	public void setPublish_user(String publish_user) {
		this.publish_user = publish_user;
 	}

	/**
	* get发表时间
	* @return publish_time
	*/
	public String getPublish_time() {
		return publish_time;
  	}

	/**
	* set发表时间
	* @return publish_time
	*/
	public void setPublish_time(String publish_time) {
		this.publish_time = publish_time;
 	}

	/**
	* get图文信息
	* @return info
	*/
	public String getInfo() {
		return info;
  	}

	/**
	* set图文信息
	* @return info
	*/
	public void setInfo(String info) {
		this.info = info;
 	}

	/**
	* get副标题1
	* @return title2
	*/
	public String getTitle2() {
		return title2;
  	}

	/**
	* set副标题1
	* @return title2
	*/
	public void setTitle2(String title2) {
		this.title2 = title2;
 	}

	/**
	* get副标题2
	* @return title3
	*/
	public String getTitle3() {
		return title3;
  	}

	/**
	* set副标题2
	* @return title3
	*/
	public void setTitle3(String title3) {
		this.title3 = title3;
 	}

	/**
	* get阅读量
	* @return reading_amount
	*/
	public String getReading_amount() {
		return reading_amount;
  	}

	/**
	* set阅读量
	* @return reading_amount
	*/
	public void setReading_amount(String reading_amount) {
		this.reading_amount = reading_amount;
 	}

	/**
	* get点赞量
	* @return likes_amount
	*/
	public String getLikes_amount() {
		return likes_amount;
  	}

	/**
	* set点赞量
	* @return likes_amount
	*/
	public void setLikes_amount(String likes_amount) {
		this.likes_amount = likes_amount;
 	}

	/**
	* get语言
	* @return lang
	*/
	public String getLang() {
		return lang;
  	}

	/**
	* set语言
	* @return lang
	*/
	public void setLang(String lang) {
		this.lang = lang;
 	}

	/**
	* get所属地区
	* @return areaid
	*/
	public String getAreaid() {
		return areaid;
  	}

	/**
	* set所属地区
	* @return areaid
	*/
	public void setAreaid(String areaid) {
		this.areaid = areaid;
 	}

	/**
	* get原链接
	* @return from_url
	*/
	public String getFrom_url() {
		return from_url;
  	}

	/**
	* set原链接
	* @return from_url
	*/
	public void setFrom_url(String from_url) {
		this.from_url = from_url;
 	}

	/**
	* get是否新窗口打开
	* @return isblank
	*/
	public String getIsblank() {
		return isblank;
  	}

	/**
	* set是否新窗口打开
	* @return isblank
	*/
	public void setIsblank(String isblank) {
		this.isblank = isblank;
 	}

	/**
	* get附件
	* @return 附件
	*/
	public List<cms_info_detail> getCms_info_detail_list() {
		return cms_info_detail_list;
  	}

	/**
	* set附件
	* @return 附件
	*/
	public void setCms_info_detail_list(List<cms_info_detail> cms_info_detail_list) {
		this.cms_info_detail_list = cms_info_detail_list;
 	}

}
