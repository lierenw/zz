package com.futvan.z.erp.customer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.erp.customer.CustomerService;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;
@Controller
public class CustomerAction extends SuperAction{
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value="/customerLogin")
	public @ResponseBody Result Service(@RequestParam HashMap<String, String> bean) {
		Result result = new Result();
 		Result authority = z.isServiceAuthority(bean,request);
 		if(Code.SUCCESS.equals(authority.getCode())){
 			String username = bean.get("username");
 			String password = bean.get("password");
 			if(z.isNotNull(username)){
 				if(z.isNotNull(password)){
 					crm_customer cq = new crm_customer();
 					cq.setUsername(username);
 					cq.setIs_enabled("1");
 	 				crm_customer c = sqlSession.selectOne("crm_customer_select", cq);
 	 				String jiami_password = StringUtil.CreatePassword(password);
 	 				if(jiami_password.equals(c.getPassword_login())) {
 	 					result.setCode(Code.SUCCESS);
 	 					result.setData(c);
 	 				}else {
 	 					result.setCode(Code.ERROR);
 	 	 				result.setMsg("密码错误");
 	 				}
 	 			}else {
 	 				result.setCode(Code.ERROR);
 	 				result.setMsg("password is null");
 	 			}
 			}else {
 				result.setCode(Code.ERROR);
 				result.setMsg("username is null");
 			}
 		}else {
 			result = authority;
 		}
		return result;
	}
}
