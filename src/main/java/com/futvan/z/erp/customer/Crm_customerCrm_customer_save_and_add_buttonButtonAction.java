package com.futvan.z.erp.customer;
import java.util.HashMap;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
public class Crm_customerCrm_customer_save_and_add_buttonButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/crm_customer_save_and_add_button")
	public @ResponseBody Result crm_customer_save_and_add_button(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		//获取客户推荐人ID
		String parentid = bean.get("parentid");

		if(z.isNotNull(parentid)) {
			//根据客户推荐人ID获取推荐人的网体节点序号
			int new_node = sqlSession.selectOne("selectoneint", "SELECT node + 1 new_node FROM crm_customer WHERE zid = '"+parentid+"'");

			//设置新客户的网体节点序号
			bean.put("node", String.valueOf(new_node));

			String PageType = bean.get("PageType");
			if("add".equals(PageType)) {
				result = commonService.insert(bean, request);
			}else if("edit".equals(PageType)) {
				result = commonService.update(bean, request);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("PageType is null");
			}
		}else{
			result.setCode(Code.ERROR);
			result.setMsg("推荐人不能为空");
		}

		
		return result;
	}
}
