package com.futvan.z.erp.erp_account_type;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class erp_account_type extends SuperBean{
	//单位
	private String unit;

	//保留小数位数
	private String decimals;

	//是否启用
	private String is_start;

	/**
	* get单位
	* @return unit
	*/
	public String getUnit() {
		return unit;
  	}

	/**
	* set单位
	* @return unit
	*/
	public void setUnit(String unit) {
		this.unit = unit;
 	}

	/**
	* get保留小数位数
	* @return decimals
	*/
	public String getDecimals() {
		return decimals;
  	}

	/**
	* set保留小数位数
	* @return decimals
	*/
	public void setDecimals(String decimals) {
		this.decimals = decimals;
 	}

	/**
	* get是否启用
	* @return is_start
	*/
	public String getIs_start() {
		return is_start;
  	}

	/**
	* set是否启用
	* @return is_start
	*/
	public void setIs_start(String is_start) {
		this.is_start = is_start;
 	}

}
