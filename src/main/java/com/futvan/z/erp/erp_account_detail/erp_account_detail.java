package com.futvan.z.erp.erp_account_detail;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class erp_account_detail extends SuperBean{
	//客户
	private String userid;

	//方向
	private String in_out;

	//账号类型
	private String a_type;

	//操作模式
	private String operation_mode;

	//数量
	private String amount;

	//单位
	private String unit;

	//总账可用数量
	private String all_balance;

	//总账冻结数量
	private String all_frozen;

	//明细账可用数量
	private String balance;

	//明细账冻结数量
	private String frozen;

	//是否解冻
	private String isunfrozen;

	//业务ID
	private String bizid;

	/**
	* get客户
	* @return userid
	*/
	public String getUserid() {
		return userid;
  	}

	/**
	* set客户
	* @return userid
	*/
	public void setUserid(String userid) {
		this.userid = userid;
 	}

	/**
	* get方向
	* @return in_out
	*/
	public String getIn_out() {
		return in_out;
  	}

	/**
	* set方向
	* @return in_out
	*/
	public void setIn_out(String in_out) {
		this.in_out = in_out;
 	}

	/**
	* get账号类型
	* @return a_type
	*/
	public String getA_type() {
		return a_type;
  	}

	/**
	* set账号类型
	* @return a_type
	*/
	public void setA_type(String a_type) {
		this.a_type = a_type;
 	}

	/**
	* get操作模式
	* @return operation_mode
	*/
	public String getOperation_mode() {
		return operation_mode;
  	}

	/**
	* set操作模式
	* @return operation_mode
	*/
	public void setOperation_mode(String operation_mode) {
		this.operation_mode = operation_mode;
 	}

	/**
	* get数量
	* @return amount
	*/
	public String getAmount() {
		return amount;
  	}

	/**
	* set数量
	* @return amount
	*/
	public void setAmount(String amount) {
		this.amount = amount;
 	}

	/**
	* get单位
	* @return unit
	*/
	public String getUnit() {
		return unit;
  	}

	/**
	* set单位
	* @return unit
	*/
	public void setUnit(String unit) {
		this.unit = unit;
 	}

	/**
	* get总账可用数量
	* @return all_balance
	*/
	public String getAll_balance() {
		return all_balance;
  	}

	/**
	* set总账可用数量
	* @return all_balance
	*/
	public void setAll_balance(String all_balance) {
		this.all_balance = all_balance;
 	}

	/**
	* get总账冻结数量
	* @return all_frozen
	*/
	public String getAll_frozen() {
		return all_frozen;
  	}

	/**
	* set总账冻结数量
	* @return all_frozen
	*/
	public void setAll_frozen(String all_frozen) {
		this.all_frozen = all_frozen;
 	}

	/**
	* get明细账可用数量
	* @return balance
	*/
	public String getBalance() {
		return balance;
  	}

	/**
	* set明细账可用数量
	* @return balance
	*/
	public void setBalance(String balance) {
		this.balance = balance;
 	}

	/**
	* get明细账冻结数量
	* @return frozen
	*/
	public String getFrozen() {
		return frozen;
  	}

	/**
	* set明细账冻结数量
	* @return frozen
	*/
	public void setFrozen(String frozen) {
		this.frozen = frozen;
 	}

	/**
	* get是否解冻
	* @return isunfrozen
	*/
	public String getIsunfrozen() {
		return isunfrozen;
  	}

	/**
	* set是否解冻
	* @return isunfrozen
	*/
	public void setIsunfrozen(String isunfrozen) {
		this.isunfrozen = isunfrozen;
 	}

	/**
	* get业务ID
	* @return bizid
	*/
	public String getBizid() {
		return bizid;
  	}

	/**
	* set业务ID
	* @return bizid
	*/
	public void setBizid(String bizid) {
		this.bizid = bizid;
 	}

}
