package com.futvan.z.erp.erp_account_detail;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.erp.erp_account_detail.Erp_account_detailService;
@Controller
public class Erp_account_detailAction extends SuperAction{
	@Autowired
	private Erp_account_detailService erp_account_detailService;
	
	
	@RequestMapping(value="/erp_account_detail_list")
	public ModelAndView erp_account_detail_list(@RequestParam HashMap<String,String> bean) throws Exception {
		//从Session中获取用户与组织信息保存到Bean中
		GetSessionInfoToBean(bean,request);
		return erp_account_detailService.select(bean,"common/form/list");

	}
}
