package com.futvan.z.system.zhttpservices;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_http_services extends SuperBean{
	//所属项目
	private String projectid;

	//接口标识
	private String serviceid;

	//接口名称
	private String title;

	//是否启用
	private String isenable;

	//接口类型
	private String http_services_type;

	//接口代码
	private String javainfo;

	//接口参数
	private List<z_http_services_parameter> z_http_services_parameter_list;

	/**
	* get所属项目
	* @return projectid
	*/
	public String getProjectid() {
		return projectid;
  	}

	/**
	* set所属项目
	* @return projectid
	*/
	public void setProjectid(String projectid) {
		this.projectid = projectid;
 	}

	/**
	* get接口标识
	* @return serviceid
	*/
	public String getServiceid() {
		return serviceid;
  	}

	/**
	* set接口标识
	* @return serviceid
	*/
	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
 	}

	/**
	* get接口名称
	* @return title
	*/
	public String getTitle() {
		return title;
  	}

	/**
	* set接口名称
	* @return title
	*/
	public void setTitle(String title) {
		this.title = title;
 	}

	/**
	* get是否启用
	* @return isenable
	*/
	public String getIsenable() {
		return isenable;
  	}

	/**
	* set是否启用
	* @return isenable
	*/
	public void setIsenable(String isenable) {
		this.isenable = isenable;
 	}

	/**
	* get接口类型
	* @return http_services_type
	*/
	public String getHttp_services_type() {
		return http_services_type;
  	}

	/**
	* set接口类型
	* @return http_services_type
	*/
	public void setHttp_services_type(String http_services_type) {
		this.http_services_type = http_services_type;
 	}

	/**
	* get接口代码
	* @return javainfo
	*/
	public String getJavainfo() {
		return javainfo;
  	}

	/**
	* set接口代码
	* @return javainfo
	*/
	public void setJavainfo(String javainfo) {
		this.javainfo = javainfo;
 	}

	/**
	* get接口参数
	* @return 接口参数
	*/
	public List<z_http_services_parameter> getZ_http_services_parameter_list() {
		return z_http_services_parameter_list;
  	}

	/**
	* set接口参数
	* @return 接口参数
	*/
	public void setZ_http_services_parameter_list(List<z_http_services_parameter> z_http_services_parameter_list) {
		this.z_http_services_parameter_list = z_http_services_parameter_list;
 	}

}
