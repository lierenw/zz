package com.futvan.z.system.zhttpservices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.catalina.startup.Bootstrap;
import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.JavaClass;
import com.futvan.z.framework.common.bean.JavaMethod;
import com.futvan.z.framework.common.bean.JavaMethodParameter;
import com.futvan.z.framework.common.bean.JavaVariable;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.BeanUtil;
import com.futvan.z.framework.util.FileUtil;
import com.futvan.z.framework.util.JavaCompilerUtil;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.framework.util.SystemUtil;
import com.futvan.z.system.zhttpservices.ZhttpservicesService;
@Controller
public class ZhttpservicesAction extends SuperAction{
	@Autowired
	private ZhttpservicesService zhttpservicesService;
	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/z_http_services_insert")
	public @ResponseBody Result z_http_services_insert(@RequestParam HashMap<String,String> bean) throws Exception {
		//判读是否为自定义程序接口，并生成相应接口文件
		CreateHttpServiceFile(bean);
		return commonService.insert(bean, request);
	}



	@RequestMapping(value="/z_http_services_update")
	public @ResponseBody Result z_http_services_update(@RequestParam HashMap<String,String> bean) throws Exception {
		//判读是否为自定义程序接口，并生成相应接口文件
		CreateHttpServiceFile(bean);
		return commonService.update(bean, request);
	}
	
	@RequestMapping(value="/z_http_services_delete")
	public @ResponseBody Result z_http_services_delete(@RequestParam HashMap<String,String> bean) throws Exception {
		//删除对应接口文件
		DeleteHttpServiceFile(bean);
		return commonService.delete(bean, request);
	}

	/**
	 * 删除对应接口文件
	 * @param bean
	 */
	private void DeleteHttpServiceFile(HashMap<String, String> bean) {
		String zids = bean.get("zids");
		String [] zidarray = zids.split(",");
		for (String zid : zidarray) {
			z_http_services s = sqlSession.selectOne("z_http_services_select_zid", zid);
			if(z.isNotNull(s)) {
				//只在自定义接口和动态编译接口需要删除文件，系统接口不用删除
				if("1".equals(s.getHttp_services_type()) || "0".equals(s.getHttp_services_type()) ) {
					String service_path = SystemUtil.getRunClassPath()+"com/futvan/z/httpservices/"+s.getServiceid()+"HttpService";
					File java = new File(service_path+".java");
					if(java.exists()) {
						java.delete();
					}
					File sclass = new File(service_path+".class");
					if(sclass.exists()) {
						sclass.delete();
					}
				}
			}
		}
		
		
	}



	/**
	 * 创建自定义接口文件
	 * @param bean
	 * @throws Exception
	 */
	private void CreateHttpServiceFile(HashMap<String, String> bean) throws Exception {
		String projectid = bean.get("projectid");//所属项目
		String serviceid = bean.get("serviceid");//接口标识
		//是否为自定义程序接口
		if("1".equals(bean.get("http_services_type"))) {
			//先判读是否是开发环境，如是是开发环境，在生成java类
			if(SystemUtil.isEclipseRunTomcat()) {
				//Eclipse开发环境
				String project_path = z.sp.get("project_path");//源码路径
				File f = new File(project_path+"\\src\\main\\java\\com\\futvan\\z\\httpservices\\"+projectid+"\\"+StringUtil.firstLetterToUpper(serviceid)+"HttpService.java");
				//如果找到文件删除
				if (f.exists()) {
					f.delete();
				}
				//创建文件
				f.createNewFile();

				//创建输入流
				OutputStreamWriter write = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");
				BufferedWriter writer = new BufferedWriter(write);
				writer.write("package com.futvan.z.httpservices."+projectid+";\n");
				writer.write("import com.futvan.z.framework.core.SuperAction;\n");
				writer.write("import com.futvan.z.framework.common.bean.Result;\n");
				writer.write("import com.futvan.z.framework.common.bean.Code;\n");
				writer.write("import com.futvan.z.framework.core.z;\n");
				writer.write("import org.springframework.web.bind.annotation.RequestParam;\n");
				writer.write("import org.springframework.stereotype.Controller;\n");
				writer.write("import org.springframework.web.bind.annotation.RequestMapping;\n");
				writer.write("import org.springframework.web.bind.annotation.ResponseBody;\n");
				writer.write("import java.util.HashMap;\n");
				writer.write("/**\n");
				writer.write(" * "+bean.get("title")+"\n");
				writer.write(" */\n");
				writer.write("@Controller\n");
				writer.write("public class "+StringUtil.firstLetterToUpper(serviceid)+"HttpService extends SuperAction{\n");
				writer.write(" \n");
				writer.write("	@RequestMapping(value=\"/"+serviceid+"\")\n");
				writer.write("	public @ResponseBody Result Service(@RequestParam HashMap<String, String> bean) {\n");
				writer.write("		Result result = new Result();\n");
				writer.write(" 		Result authority = z.isServiceAuthority(bean,request);\n");
				writer.write(" 		if(Code.SUCCESS.equals(authority.getCode())){\n");
				writer.write(" 			\n");
				writer.write(" 		}else {\n");
				writer.write(" 			result = authority;\n");
				writer.write(" 		}\n");
				writer.write("		return result;\n");
				writer.write("	}\n");
				writer.write("}\n");
				writer.close();
				write.close();
			}else {
				z.Exception("非开发环境，无法创建自定义程序接口");
			}

		}else if("0".equals(bean.get("http_services_type"))) {
			//生成Java类，并编译
			z_http_services s = BeanUtil.MapToBean(bean, z_http_services.class);
			JavaCompilerUtil.CreateServicesClass(s);
		}
	}



}