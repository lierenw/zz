package com.futvan.z.system.zmenu;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.SystemUtil;
import com.futvan.z.system.zmenu.ZmenuService;
@Controller
public class ZmenuAction extends SuperAction{
	@Autowired
	private ZmenuService zmenuService;
	@Autowired
	private CommonService commonService;
	
	/**
	 * 	打开导入按钮页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/OpenImportButtonsList")
	public ModelAndView OpenImportButtonsList(String formid,String menuid) throws Exception {
		ModelAndView  model = new ModelAndView("system/zmenu/ImportButtonsList");
		model.addObject("buttonsList",zmenuService.getbuttonsList(formid,menuid));
		model.addObject("menuid",menuid);
		return model;
	}
	
	/**
	 * 微信小程序中获取菜单列表1
	 * @param userzid
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getMenuList1ForWX")
	public @ResponseBody Result getMenuList1ForWX(String userzid)  throws Exception{
		return zmenuService.getMenuList1ForWX(userzid);
	}
	
	/**
	 * 	打开导入按钮页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/OpenImportReportButtonsList")
	public ModelAndView OpenImportReportButtonsList(String reportid,String menuid) throws Exception {
		ModelAndView  model = new ModelAndView("system/zmenu/ImportReportButtonsList");
		model.addObject("buttonsList",zmenuService.getReportButtonsList(reportid,menuid));
		model.addObject("menuid",menuid);
		return model;
	}
	
	@RequestMapping(value="/ImportButtons")
	public @ResponseBody Result ImportButtons(String buttonIds,String menuid) throws Exception {
		Result result = new Result();
		if(!"".equals(buttonIds) && buttonIds!=null){
			if(!"".equals(menuid) && menuid!=null) {
				zmenuService.ImportButtons(menuid,buttonIds);
				result.setCode(Code.SUCCESS);
			}else {
				result.setCode(Code.ERROR);
				result.setData("表单ID为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setData("按钮ID为空");
		}

		return result;
	}
	
	@RequestMapping(value="/ImportReportButtons")
	public @ResponseBody Result ImportReportButtons(String buttonIds,String menuid) throws Exception {
		Result result = new Result();
		if(!"".equals(buttonIds) && buttonIds!=null){
			if(!"".equals(menuid) && menuid!=null) {
				zmenuService.ImportReportButtons(menuid,buttonIds);
				result.setCode(Code.SUCCESS);
			}else {
				result.setCode(Code.ERROR);
				result.setData("表单ID为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setData("按钮ID为空");
		}

		return result;
	}

	@RequestMapping(value="/MenuInsert")
	public @ResponseBody Result MenuInsert(@RequestParam HashMap<String,String> bean) throws Exception {
		//生成主表ZID
		String zid = z.newZid("z_menu");
		bean.put("zid", zid);

		//判读是否联系表单，如果关联了
		String formid = bean.get("formid");
		if(!"".equals(formid) && formid!=null) {
			//导入关联表单所有按钮
			zmenuService.AddButtons(zid,formid);
		}
		return commonService.insert(bean,request);
	}

}
