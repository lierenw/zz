package com.futvan.z.system.zreportquery;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.system.zreportquery.ZreportqueryService;
@Controller
public class ZreportqueryAction extends SuperAction{
	@Autowired
	private ZreportqueryService zreportqueryService;
	
	/**
	 * 查询用户常用查询条件
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getQueryInfoListR")
	public @ResponseBody Result getQueryInfoListR(@RequestParam HashMap<String,String> bean) throws Exception{
		Result result = new Result();
		//当前用户ID
		String userid = GetSessionUserId(request);
		//表ID
		String reportid = bean.get("reportid");
		if(z.isNotNull(reportid)) {
			if(z.isNotNull(userid)) {
				z_report_query qq = new z_report_query();
				qq.setReportid(reportid);
				qq.setUserid(userid);
				List<z_report_query> list = sqlSession.selectList("z_report_query_select", qq);
				result.setCode(Code.SUCCESS);
				result.setData(list);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("无法获取当前登录用户信息");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("tableid is not null");
		}
		
		
		return result;
		
	}
	
	/**
	 * 删除常用查询条件
	 * @param bean
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/deleteQueryInfoR")
	public @ResponseBody Result deleteQueryInfoR(@RequestParam HashMap<String,String> bean) throws Exception{
		Result result = new Result();
		String zid = bean.get("zid");
		if(z.isNotNull(zid)) {
			int num = sqlSession.delete("z_report_query_delete_zid",zid);
			if(num==1) {
				result.setCode(Code.SUCCESS);
			}else {
				z.Exception("delete z_report_query_delete_zid 数据大于1");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("zid is not null");
		}
		return result;
	}
	/**
	 * 保存用户常用查询条件
	 * @param bean
	 * @return
	 */
	@RequestMapping(value="/SaveQueryInfoR")
	public @ResponseBody Result SaveQueryInfoR(@RequestParam HashMap<String,String> bean){
		Result result = new Result();
		//表ID
		String reportid = bean.get("reportid");
		//查询条件
		String queryinfo = bean.get("queryinfo");
		//名称
		String name = bean.get("name");
		//用户ID
		String userzid = getUserId();
		if(z.isNotNull(reportid)) {
			if(z.isNotNull(userzid)) {
				if(z.isNotNull(queryinfo)) {
					if(z.isNotNull(name)) {
						//保存数据到数据库
						z_report_query fq = new z_report_query();
						fq.setZid(z.newZid("z_report_query"));
						fq.setName(name);
						fq.setReportid(reportid);
						fq.setUserid(userzid);
						fq.setQueryinfo(queryinfo);
						int num = sqlSession.insert("z_report_query_insert", fq);
						if(num==1) {
							result.setCode(Code.SUCCESS);
							result.setMsg("更新系统参数成功");
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("insert return count | "+num);
						}
					}else {
						result.setCode(Code.ERROR);
						result.setMsg("name is null");
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("queryinfo is null");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("userzid is null");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("reportid is null");
		}
		return result;
	}
}
