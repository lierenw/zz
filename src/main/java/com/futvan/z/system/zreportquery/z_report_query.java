package com.futvan.z.system.zreportquery;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_report_query extends SuperBean{
	//用户
	private String userid;

	//报表
	private String reportid;

	//查询条件
	private String queryinfo;

	/**
	* get用户
	* @return userid
	*/
	public String getUserid() {
		return userid;
  	}

	/**
	* set用户
	* @return userid
	*/
	public void setUserid(String userid) {
		this.userid = userid;
 	}

	/**
	* get报表
	* @return reportid
	*/
	public String getReportid() {
		return reportid;
  	}

	/**
	* set报表
	* @return reportid
	*/
	public void setReportid(String reportid) {
		this.reportid = reportid;
 	}

	/**
	* get查询条件
	* @return queryinfo
	*/
	public String getQueryinfo() {
		return queryinfo;
  	}

	/**
	* set查询条件
	* @return queryinfo
	*/
	public void setQueryinfo(String queryinfo) {
		this.queryinfo = queryinfo;
 	}

}
