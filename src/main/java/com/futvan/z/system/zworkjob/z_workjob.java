package com.futvan.z.system.zworkjob;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_workjob extends SuperBean{
	//申请人
	private String user_id;

	//当前处理节点
	private String now_node;

	//关联业务表
	private String table_id;

	//业务单据ID
	private String biz_id;

	//工作任务节点
	private List<z_workjob_node> z_workjob_node_list;

	/**
	* get申请人
	* @return user_id
	*/
	public String getUser_id() {
		return user_id;
  	}

	/**
	* set申请人
	* @return user_id
	*/
	public void setUser_id(String user_id) {
		this.user_id = user_id;
 	}

	/**
	* get当前处理节点
	* @return now_node
	*/
	public String getNow_node() {
		return now_node;
  	}

	/**
	* set当前处理节点
	* @return now_node
	*/
	public void setNow_node(String now_node) {
		this.now_node = now_node;
 	}

	/**
	* get关联业务表
	* @return table_id
	*/
	public String getTable_id() {
		return table_id;
  	}

	/**
	* set关联业务表
	* @return table_id
	*/
	public void setTable_id(String table_id) {
		this.table_id = table_id;
 	}

	/**
	* get业务单据ID
	* @return biz_id
	*/
	public String getBiz_id() {
		return biz_id;
  	}

	/**
	* set业务单据ID
	* @return biz_id
	*/
	public void setBiz_id(String biz_id) {
		this.biz_id = biz_id;
 	}

	/**
	* get工作任务节点
	* @return 工作任务节点
	*/
	public List<z_workjob_node> getZ_workjob_node_list() {
		return z_workjob_node_list;
  	}

	/**
	* set工作任务节点
	* @return 工作任务节点
	*/
	public void setZ_workjob_node_list(List<z_workjob_node> z_workjob_node_list) {
		this.z_workjob_node_list = z_workjob_node_list;
 	}

}
