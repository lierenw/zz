package com.futvan.z.system.zvcode;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_vcode extends SuperBean{
	//手机
	private String tel;

	//验证码
	private String vcode;

	//失效时间
	private String endtime;

	/**
	* get手机
	* @return tel
	*/
	public String getTel() {
		return tel;
  	}

	/**
	* set手机
	* @return tel
	*/
	public void setTel(String tel) {
		this.tel = tel;
 	}

	/**
	* get验证码
	* @return vcode
	*/
	public String getVcode() {
		return vcode;
  	}

	/**
	* set验证码
	* @return vcode
	*/
	public void setVcode(String vcode) {
		this.vcode = vcode;
 	}

	/**
	* get失效时间
	* @return endtime
	*/
	public String getEndtime() {
		return endtime;
  	}

	/**
	* set失效时间
	* @return endtime
	*/
	public void setEndtime(String endtime) {
		this.endtime = endtime;
 	}

}
