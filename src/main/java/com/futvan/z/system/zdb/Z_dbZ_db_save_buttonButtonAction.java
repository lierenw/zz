package com.futvan.z.system.zdb;
import java.util.HashMap;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
public class Z_dbZ_db_save_buttonButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;
	@Autowired
	private ZdbService zdbService;

	@RequestMapping(value="/z_db_save_button")
	public @ResponseBody Result z_db_save_button(@RequestParam HashMap<String,String> bean) throws Exception {
		return zdbService.z_db_save_button(bean, request);
	}
}
