package com.futvan.z.system.zreport;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_report_button extends SuperBean{
	//按钮标识
	private String button_id;

	//按钮名称
	private String button_name;

	//按钮图标
	private String button_icon;

	//是否隐藏
	private String is_hidden;

	//点击事件
	private String js_onclick;

	/**
	* get按钮标识
	* @return button_id
	*/
	public String getButton_id() {
		return button_id;
  	}

	/**
	* set按钮标识
	* @return button_id
	*/
	public void setButton_id(String button_id) {
		this.button_id = button_id;
 	}

	/**
	* get按钮名称
	* @return button_name
	*/
	public String getButton_name() {
		return button_name;
  	}

	/**
	* set按钮名称
	* @return button_name
	*/
	public void setButton_name(String button_name) {
		this.button_name = button_name;
 	}

	/**
	* get按钮图标
	* @return button_icon
	*/
	public String getButton_icon() {
		return button_icon;
  	}

	/**
	* set按钮图标
	* @return button_icon
	*/
	public void setButton_icon(String button_icon) {
		this.button_icon = button_icon;
 	}

	/**
	* get是否隐藏
	* @return is_hidden
	*/
	public String getIs_hidden() {
		return is_hidden;
  	}

	/**
	* set是否隐藏
	* @return is_hidden
	*/
	public void setIs_hidden(String is_hidden) {
		this.is_hidden = is_hidden;
 	}

	/**
	* get点击事件
	* @return js_onclick
	*/
	public String getJs_onclick() {
		return js_onclick;
  	}

	/**
	* set点击事件
	* @return js_onclick
	*/
	public void setJs_onclick(String js_onclick) {
		this.js_onclick = js_onclick;
 	}

}
