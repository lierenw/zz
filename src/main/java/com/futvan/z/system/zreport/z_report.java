package com.futvan.z.system.zreport;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_report extends SuperBean{
	//报表标识
	private String reportid;

	//报表标题
	private String title;

	//查询数据库
	private String dbid;

	//是否启动快速查询
	private String isStartQuickQuery;

	//SQL
	private String sqlinfo;

	//报表字段
	private List<z_report_column> z_report_column_list;

	//报表按钮
	private List<z_report_button> z_report_button_list;

	/**
	* get报表标识
	* @return reportid
	*/
	public String getReportid() {
		return reportid;
  	}

	/**
	* set报表标识
	* @return reportid
	*/
	public void setReportid(String reportid) {
		this.reportid = reportid;
 	}

	/**
	* get报表标题
	* @return title
	*/
	public String getTitle() {
		return title;
  	}

	/**
	* set报表标题
	* @return title
	*/
	public void setTitle(String title) {
		this.title = title;
 	}

	/**
	* get查询数据库
	* @return dbid
	*/
	public String getDbid() {
		return dbid;
  	}

	/**
	* set查询数据库
	* @return dbid
	*/
	public void setDbid(String dbid) {
		this.dbid = dbid;
 	}

	/**
	* get是否启动快速查询
	* @return isStartQuickQuery
	*/
	public String getIsStartQuickQuery() {
		return isStartQuickQuery;
  	}

	/**
	* set是否启动快速查询
	* @return isStartQuickQuery
	*/
	public void setIsStartQuickQuery(String isStartQuickQuery) {
		this.isStartQuickQuery = isStartQuickQuery;
 	}

	/**
	* getSQL
	* @return sqlinfo
	*/
	public String getSqlinfo() {
		return sqlinfo;
  	}

	/**
	* setSQL
	* @return sqlinfo
	*/
	public void setSqlinfo(String sqlinfo) {
		this.sqlinfo = sqlinfo;
 	}

	/**
	* get报表字段
	* @return 报表字段
	*/
	public List<z_report_column> getZ_report_column_list() {
		return z_report_column_list;
  	}

	/**
	* set报表字段
	* @return 报表字段
	*/
	public void setZ_report_column_list(List<z_report_column> z_report_column_list) {
		this.z_report_column_list = z_report_column_list;
 	}

	/**
	* get报表按钮
	* @return 报表按钮
	*/
	public List<z_report_button> getZ_report_button_list() {
		return z_report_button_list;
  	}

	/**
	* set报表按钮
	* @return 报表按钮
	*/
	public void setZ_report_button_list(List<z_report_button> z_report_button_list) {
		this.z_report_button_list = z_report_button_list;
 	}

}
