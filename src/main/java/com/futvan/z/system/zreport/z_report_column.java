package com.futvan.z.system.zreport;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_report_column extends SuperBean{
	//字段类型
	private String column_type;

	//字段ID
	private String column_id;

	//字段名称
	private String column_name;

	//字段规格
	private String column_size;

	//显示长度
	private String colunm_length_list;

	//比较符类型
	private String compare;

	//列表中是否隐藏
	private String Is_hidden_list;

	//行链接地址
	private String tr_href;

	//关联代码编号
	private String p_code_id;

	//Z5关联表
	private String z5_table;

	//Z5关联字段
	private String z5_key;

	//Z5显示字段
	private String z5_value;

	//Z5显示辅助字段
	private String z5_value2;

	//Z5弹出页面显示字段
	private String z5_page_display;

	//Z5关联SQL
	private String z5_sql;

	//是否启用快速查询
	private String is_search;

	/**
	* get字段类型
	* @return column_type
	*/
	public String getColumn_type() {
		return column_type;
  	}

	/**
	* set字段类型
	* @return column_type
	*/
	public void setColumn_type(String column_type) {
		this.column_type = column_type;
 	}

	/**
	* get字段ID
	* @return column_id
	*/
	public String getColumn_id() {
		return column_id;
  	}

	/**
	* set字段ID
	* @return column_id
	*/
	public void setColumn_id(String column_id) {
		this.column_id = column_id;
 	}

	/**
	* get字段名称
	* @return column_name
	*/
	public String getColumn_name() {
		return column_name;
  	}

	/**
	* set字段名称
	* @return column_name
	*/
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
 	}

	/**
	* get字段规格
	* @return column_size
	*/
	public String getColumn_size() {
		return column_size;
  	}

	/**
	* set字段规格
	* @return column_size
	*/
	public void setColumn_size(String column_size) {
		this.column_size = column_size;
 	}

	/**
	* get显示长度
	* @return colunm_length_list
	*/
	public String getColunm_length_list() {
		return colunm_length_list;
  	}

	/**
	* set显示长度
	* @return colunm_length_list
	*/
	public void setColunm_length_list(String colunm_length_list) {
		this.colunm_length_list = colunm_length_list;
 	}

	/**
	* get比较符类型
	* @return compare
	*/
	public String getCompare() {
		return compare;
  	}

	/**
	* set比较符类型
	* @return compare
	*/
	public void setCompare(String compare) {
		this.compare = compare;
 	}

	/**
	* get列表中是否隐藏
	* @return Is_hidden_list
	*/
	public String getIs_hidden_list() {
		return Is_hidden_list;
  	}

	/**
	* set列表中是否隐藏
	* @return Is_hidden_list
	*/
	public void setIs_hidden_list(String Is_hidden_list) {
		this.Is_hidden_list = Is_hidden_list;
 	}

	/**
	* get行链接地址
	* @return tr_href
	*/
	public String getTr_href() {
		return tr_href;
  	}

	/**
	* set行链接地址
	* @return tr_href
	*/
	public void setTr_href(String tr_href) {
		this.tr_href = tr_href;
 	}

	/**
	* get关联代码编号
	* @return p_code_id
	*/
	public String getP_code_id() {
		return p_code_id;
  	}

	/**
	* set关联代码编号
	* @return p_code_id
	*/
	public void setP_code_id(String p_code_id) {
		this.p_code_id = p_code_id;
 	}

	/**
	* getZ5关联表
	* @return z5_table
	*/
	public String getZ5_table() {
		return z5_table;
  	}

	/**
	* setZ5关联表
	* @return z5_table
	*/
	public void setZ5_table(String z5_table) {
		this.z5_table = z5_table;
 	}

	/**
	* getZ5关联字段
	* @return z5_key
	*/
	public String getZ5_key() {
		return z5_key;
  	}

	/**
	* setZ5关联字段
	* @return z5_key
	*/
	public void setZ5_key(String z5_key) {
		this.z5_key = z5_key;
 	}

	/**
	* getZ5显示字段
	* @return z5_value
	*/
	public String getZ5_value() {
		return z5_value;
  	}

	/**
	* setZ5显示字段
	* @return z5_value
	*/
	public void setZ5_value(String z5_value) {
		this.z5_value = z5_value;
 	}

	/**
	* getZ5显示辅助字段
	* @return z5_value2
	*/
	public String getZ5_value2() {
		return z5_value2;
  	}

	/**
	* setZ5显示辅助字段
	* @return z5_value2
	*/
	public void setZ5_value2(String z5_value2) {
		this.z5_value2 = z5_value2;
 	}

	/**
	* getZ5弹出页面显示字段
	* @return z5_page_display
	*/
	public String getZ5_page_display() {
		return z5_page_display;
  	}

	/**
	* setZ5弹出页面显示字段
	* @return z5_page_display
	*/
	public void setZ5_page_display(String z5_page_display) {
		this.z5_page_display = z5_page_display;
 	}

	/**
	* getZ5关联SQL
	* @return z5_sql
	*/
	public String getZ5_sql() {
		return z5_sql;
  	}

	/**
	* setZ5关联SQL
	* @return z5_sql
	*/
	public void setZ5_sql(String z5_sql) {
		this.z5_sql = z5_sql;
 	}

	/**
	* get是否启用快速查询
	* @return is_search
	*/
	public String getIs_search() {
		return is_search;
  	}

	/**
	* set是否启用快速查询
	* @return is_search
	*/
	public void setIs_search(String is_search) {
		this.is_search = is_search;
 	}

}
