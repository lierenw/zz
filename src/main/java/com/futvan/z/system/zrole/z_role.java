package com.futvan.z.system.zrole;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_role extends SuperBean{
	//角色
	private String rolename;

	//角色用户
	private List<z_role_user> z_role_user_list;

	//角色菜单
	private List<z_role_menu> z_role_menu_list;

	/**
	* get角色
	* @return rolename
	*/
	public String getRolename() {
		return rolename;
  	}

	/**
	* set角色
	* @return rolename
	*/
	public void setRolename(String rolename) {
		this.rolename = rolename;
 	}

	/**
	* get角色用户
	* @return 角色用户
	*/
	public List<z_role_user> getZ_role_user_list() {
		return z_role_user_list;
  	}

	/**
	* set角色用户
	* @return 角色用户
	*/
	public void setZ_role_user_list(List<z_role_user> z_role_user_list) {
		this.z_role_user_list = z_role_user_list;
 	}

	/**
	* get角色菜单
	* @return 角色菜单
	*/
	public List<z_role_menu> getZ_role_menu_list() {
		return z_role_menu_list;
  	}

	/**
	* set角色菜单
	* @return 角色菜单
	*/
	public void setZ_role_menu_list(List<z_role_menu> z_role_menu_list) {
		this.z_role_menu_list = z_role_menu_list;
 	}

}
