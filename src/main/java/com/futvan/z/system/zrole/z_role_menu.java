package com.futvan.z.system.zrole;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_role_menu extends SuperBean{
	//功能菜单
	private String menuid;

	/**
	* get功能菜单
	* @return menuid
	*/
	public String getMenuid() {
		return menuid;
  	}

	/**
	* set功能菜单
	* @return menuid
	*/
	public void setMenuid(String menuid) {
		this.menuid = menuid;
 	}

}
