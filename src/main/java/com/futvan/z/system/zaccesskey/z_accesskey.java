package com.futvan.z.system.zaccesskey;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_accesskey extends SuperBean{
	//AccessKeyId
	private String accesskeyid;

	//AccessKeySecret
	private String accesskeysecret;

	//授权IP
	private List<z_accesskey_ip> z_accesskey_ip_list;

	/**
	* getAccessKeyId
	* @return accesskeyid
	*/
	public String getAccesskeyid() {
		return accesskeyid;
  	}

	/**
	* setAccessKeyId
	* @return accesskeyid
	*/
	public void setAccesskeyid(String accesskeyid) {
		this.accesskeyid = accesskeyid;
 	}

	/**
	* getAccessKeySecret
	* @return accesskeysecret
	*/
	public String getAccesskeysecret() {
		return accesskeysecret;
  	}

	/**
	* setAccessKeySecret
	* @return accesskeysecret
	*/
	public void setAccesskeysecret(String accesskeysecret) {
		this.accesskeysecret = accesskeysecret;
 	}

	/**
	* get授权IP
	* @return 授权IP
	*/
	public List<z_accesskey_ip> getZ_accesskey_ip_list() {
		return z_accesskey_ip_list;
  	}

	/**
	* set授权IP
	* @return 授权IP
	*/
	public void setZ_accesskey_ip_list(List<z_accesskey_ip> z_accesskey_ip_list) {
		this.z_accesskey_ip_list = z_accesskey_ip_list;
 	}

}
