package com.futvan.z.system.zmessages;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.system.zmessages.ZmessagesService;
import com.futvan.z.system.zuser.z_user;
@Controller
public class ZmessagesAction extends SuperAction{
	@Autowired
	private ZmessagesService zmessagesService;
	
	/**
	 * 获取用户消息列表
	 * @param touserid 接收消息人
	 * @return
	 */
	@RequestMapping(value="/GetUserMessageList",method=RequestMethod.GET)
	public @ResponseBody Result GetUserMessageList(String touserid){
		return super.GetUserMessageList(touserid);
	}
	
	/**
	 * 获取指定用户发来的消息数量
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getFromuserMessageCount",method=RequestMethod.GET)
	public @ResponseBody Result getFromuserMessageCount(String fromuserid) throws Exception {
	    Result result = new Result();
	    if(z.isNotNull(fromuserid)) {
	    	//获取发信数量
	    	String touserid = GetSessionUserId(request);
	    	int messageCount = zmessagesService.getFromuserMessageCount(fromuserid,touserid);
	    	//获取发信人姓名
	    	z_user fromuser = z.users.get(fromuserid);
	    	if(z.isNotNull(fromuser)) {
	    		result.setCode(Code.SUCCESS);//返回状态
	    	    result.setMsg("成功");//返回提示
	    	    result.setData(fromuser.getUser_name()+" ["+messageCount+"]");//返回内容
	    	}else {
	    		result.setCode(Code.ERROR);
		    	result.setMsg("根据发信人ID无法找到对应用户");
	    	}
	    }else {
	    	result.setCode(Code.ERROR);
	    	result.setMsg("发信人ID为空，无法查询该用户发送消息数量");
	    }
	    return result;
	}

	
	/**
	 * 修改消息是否查看状态为已查看
	 * @param touserid
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/UpdateIsOpen")
	public @ResponseBody Result UpdateIsOpen(String fromuserid) throws Exception {
	    Result result = new Result();
	    if(z.isNotNull(fromuserid)) {
	    	//修改消息是否查看状态为已查看
	    	String touserid = GetSessionUserId(request);
	    	zmessagesService.UpdateIsOpen(fromuserid,touserid);
	    	result.setCode(Code.SUCCESS);//返回状态
	    	result.setMsg("成功");//返回提示
	    }else {
	    	result.setCode(Code.ERROR);
	    	result.setMsg("发信人ID为空，无法修改消息是否查看状态");
	    }
	    return result;
	}

	
	
}
