package com.futvan.z.system.zetlin;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_etl_in_log extends SuperBean{
	//是否执行成功
	private String issuccess;

	//执行时间
	private String runtime;

	//完成时间
	private String endtime;

	//耗时
	private String timelenght;

	//抽取数据条数
	private String etlcount;

	//日志信息
	private String etllog;

	/**
	* get是否执行成功
	* @return issuccess
	*/
	public String getIssuccess() {
		return issuccess;
  	}

	/**
	* set是否执行成功
	* @return issuccess
	*/
	public void setIssuccess(String issuccess) {
		this.issuccess = issuccess;
 	}

	/**
	* get执行时间
	* @return runtime
	*/
	public String getRuntime() {
		return runtime;
  	}

	/**
	* set执行时间
	* @return runtime
	*/
	public void setRuntime(String runtime) {
		this.runtime = runtime;
 	}

	/**
	* get完成时间
	* @return endtime
	*/
	public String getEndtime() {
		return endtime;
  	}

	/**
	* set完成时间
	* @return endtime
	*/
	public void setEndtime(String endtime) {
		this.endtime = endtime;
 	}

	/**
	* get耗时
	* @return timelenght
	*/
	public String getTimelenght() {
		return timelenght;
  	}

	/**
	* set耗时
	* @return timelenght
	*/
	public void setTimelenght(String timelenght) {
		this.timelenght = timelenght;
 	}

	/**
	* get抽取数据条数
	* @return etlcount
	*/
	public String getEtlcount() {
		return etlcount;
  	}

	/**
	* set抽取数据条数
	* @return etlcount
	*/
	public void setEtlcount(String etlcount) {
		this.etlcount = etlcount;
 	}

	/**
	* get日志信息
	* @return etllog
	*/
	public String getEtllog() {
		return etllog;
  	}

	/**
	* set日志信息
	* @return etllog
	*/
	public void setEtllog(String etllog) {
		this.etllog = etllog;
 	}

}
