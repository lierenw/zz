package com.futvan.z.system.zetlin;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_etl_in extends SuperBean{
	//抽取数据库
	private String form_db;

	//存储数据库
	private String to_db;

	//取数据SQL
	private String sqlinfo;

	//存储表
	private String to_db_table;

	//是否执行成功
	private String last_issuccess;

	//最后抽取记录数
	private String last_etl_count;

	//最后执行时间
	private String last_time;

	//最后抽取日志
	private String last_log;

	//字段映射关系
	private List<z_etl_in_detail> z_etl_in_detail_list;

	//数据抽取记录
	private List<z_etl_in_log> z_etl_in_log_list;

	/**
	* get抽取数据库
	* @return form_db
	*/
	public String getForm_db() {
		return form_db;
  	}

	/**
	* set抽取数据库
	* @return form_db
	*/
	public void setForm_db(String form_db) {
		this.form_db = form_db;
 	}

	/**
	* get存储数据库
	* @return to_db
	*/
	public String getTo_db() {
		return to_db;
  	}

	/**
	* set存储数据库
	* @return to_db
	*/
	public void setTo_db(String to_db) {
		this.to_db = to_db;
 	}

	/**
	* get取数据SQL
	* @return sqlinfo
	*/
	public String getSqlinfo() {
		return sqlinfo;
  	}

	/**
	* set取数据SQL
	* @return sqlinfo
	*/
	public void setSqlinfo(String sqlinfo) {
		this.sqlinfo = sqlinfo;
 	}

	/**
	* get存储表
	* @return to_db_table
	*/
	public String getTo_db_table() {
		return to_db_table;
  	}

	/**
	* set存储表
	* @return to_db_table
	*/
	public void setTo_db_table(String to_db_table) {
		this.to_db_table = to_db_table;
 	}

	/**
	* get是否执行成功
	* @return last_issuccess
	*/
	public String getLast_issuccess() {
		return last_issuccess;
  	}

	/**
	* set是否执行成功
	* @return last_issuccess
	*/
	public void setLast_issuccess(String last_issuccess) {
		this.last_issuccess = last_issuccess;
 	}

	/**
	* get最后抽取记录数
	* @return last_etl_count
	*/
	public String getLast_etl_count() {
		return last_etl_count;
  	}

	/**
	* set最后抽取记录数
	* @return last_etl_count
	*/
	public void setLast_etl_count(String last_etl_count) {
		this.last_etl_count = last_etl_count;
 	}

	/**
	* get最后执行时间
	* @return last_time
	*/
	public String getLast_time() {
		return last_time;
  	}

	/**
	* set最后执行时间
	* @return last_time
	*/
	public void setLast_time(String last_time) {
		this.last_time = last_time;
 	}

	/**
	* get最后抽取日志
	* @return last_log
	*/
	public String getLast_log() {
		return last_log;
  	}

	/**
	* set最后抽取日志
	* @return last_log
	*/
	public void setLast_log(String last_log) {
		this.last_log = last_log;
 	}

	/**
	* get字段映射关系
	* @return 字段映射关系
	*/
	public List<z_etl_in_detail> getZ_etl_in_detail_list() {
		return z_etl_in_detail_list;
  	}

	/**
	* set字段映射关系
	* @return 字段映射关系
	*/
	public void setZ_etl_in_detail_list(List<z_etl_in_detail> z_etl_in_detail_list) {
		this.z_etl_in_detail_list = z_etl_in_detail_list;
 	}

	/**
	* get数据抽取记录
	* @return 数据抽取记录
	*/
	public List<z_etl_in_log> getZ_etl_in_log_list() {
		return z_etl_in_log_list;
  	}

	/**
	* set数据抽取记录
	* @return 数据抽取记录
	*/
	public void setZ_etl_in_log_list(List<z_etl_in_log> z_etl_in_log_list) {
		this.z_etl_in_log_list = z_etl_in_log_list;
 	}

}
