package com.futvan.z.system.zfile;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_file extends SuperBean{
	//文件名称
	private String filename;

	//文件类型
	private String file_type;

	//文件大小
	private String size;

	//服务器地址
	private String serverurl;

	//文件路径
	private String filepath;

	//文件
	private String allpath;

	/**
	* get文件名称
	* @return filename
	*/
	public String getFilename() {
		return filename;
  	}

	/**
	* set文件名称
	* @return filename
	*/
	public void setFilename(String filename) {
		this.filename = filename;
 	}

	/**
	* get文件类型
	* @return file_type
	*/
	public String getFile_type() {
		return file_type;
  	}

	/**
	* set文件类型
	* @return file_type
	*/
	public void setFile_type(String file_type) {
		this.file_type = file_type;
 	}

	/**
	* get文件大小
	* @return size
	*/
	public String getSize() {
		return size;
  	}

	/**
	* set文件大小
	* @return size
	*/
	public void setSize(String size) {
		this.size = size;
 	}

	/**
	* get服务器地址
	* @return serverurl
	*/
	public String getServerurl() {
		return serverurl;
  	}

	/**
	* set服务器地址
	* @return serverurl
	*/
	public void setServerurl(String serverurl) {
		this.serverurl = serverurl;
 	}

	/**
	* get文件路径
	* @return filepath
	*/
	public String getFilepath() {
		return filepath;
  	}

	/**
	* set文件路径
	* @return filepath
	*/
	public void setFilepath(String filepath) {
		this.filepath = filepath;
 	}

	/**
	* get文件
	* @return allpath
	*/
	public String getAllpath() {
		return allpath;
  	}

	/**
	* set文件
	* @return allpath
	*/
	public void setAllpath(String allpath) {
		this.allpath = allpath;
 	}

}
