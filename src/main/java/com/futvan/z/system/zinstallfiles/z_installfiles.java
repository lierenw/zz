package com.futvan.z.system.zinstallfiles;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_installfiles extends SuperBean{
	//安装文件类型
	private String install_files;

	//版本号
	private String version_number;

	//是否发布
	private String isrelease;

	//文件安装包
	private String filepackage;

	//SQL脚本
	private String sql_file;

	//发布说明
	private String info;

	/**
	* get安装文件类型
	* @return install_files
	*/
	public String getInstall_files() {
		return install_files;
  	}

	/**
	* set安装文件类型
	* @return install_files
	*/
	public void setInstall_files(String install_files) {
		this.install_files = install_files;
 	}

	/**
	* get版本号
	* @return version_number
	*/
	public String getVersion_number() {
		return version_number;
  	}

	/**
	* set版本号
	* @return version_number
	*/
	public void setVersion_number(String version_number) {
		this.version_number = version_number;
 	}

	/**
	* get是否发布
	* @return isrelease
	*/
	public String getIsrelease() {
		return isrelease;
  	}

	/**
	* set是否发布
	* @return isrelease
	*/
	public void setIsrelease(String isrelease) {
		this.isrelease = isrelease;
 	}

	/**
	* get文件安装包
	* @return filepackage
	*/
	public String getFilepackage() {
		return filepackage;
  	}

	/**
	* set文件安装包
	* @return filepackage
	*/
	public void setFilepackage(String filepackage) {
		this.filepackage = filepackage;
 	}

	/**
	* getSQL脚本
	* @return sql_file
	*/
	public String getSql_file() {
		return sql_file;
  	}

	/**
	* setSQL脚本
	* @return sql_file
	*/
	public void setSql_file(String sql_file) {
		this.sql_file = sql_file;
 	}

	/**
	* get发布说明
	* @return info
	*/
	public String getInfo() {
		return info;
  	}

	/**
	* set发布说明
	* @return info
	*/
	public void setInfo(String info) {
		this.info = info;
 	}

}
