package com.futvan.z.system.zuser;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_user extends SuperBean{
	//所属组织
	private String user_orgid;

	//所属角色
	private String user_role;

	//登录账号
	private String user_id;

	//账号名称
	private String user_name;

	//真实姓名
	private String realname;

	//性别
	private String gender;

	//手机号
	private String tel;

	//邮箱
	private String email;

	//身份证号
	private String idcard;

	//出生日期
	private String birth_date;

	//籍贯
	private String native_place;

	//居住地址
	private String address;

	//头像
	private String photo;

	//是否启用
	private String is_start;

	//推荐人
	private String parentid;

	//用户级别
	private String zuser_level;

	//用户岗位级别
	private String user_role_level;

	//微信OpenId
	private String openid;

	//登陆密码
	private String login_password;

	//支付密码
	private String super_password;

	//系统风格
	private String z_themes;

	/**
	* get所属组织
	* @return user_orgid
	*/
	public String getUser_orgid() {
		return user_orgid;
  	}

	/**
	* set所属组织
	* @return user_orgid
	*/
	public void setUser_orgid(String user_orgid) {
		this.user_orgid = user_orgid;
 	}

	/**
	* get所属角色
	* @return user_role
	*/
	public String getUser_role() {
		return user_role;
  	}

	/**
	* set所属角色
	* @return user_role
	*/
	public void setUser_role(String user_role) {
		this.user_role = user_role;
 	}

	/**
	* get登录账号
	* @return user_id
	*/
	public String getUser_id() {
		return user_id;
  	}

	/**
	* set登录账号
	* @return user_id
	*/
	public void setUser_id(String user_id) {
		this.user_id = user_id;
 	}

	/**
	* get账号名称
	* @return user_name
	*/
	public String getUser_name() {
		return user_name;
  	}

	/**
	* set账号名称
	* @return user_name
	*/
	public void setUser_name(String user_name) {
		this.user_name = user_name;
 	}

	/**
	* get真实姓名
	* @return realname
	*/
	public String getRealname() {
		return realname;
  	}

	/**
	* set真实姓名
	* @return realname
	*/
	public void setRealname(String realname) {
		this.realname = realname;
 	}

	/**
	* get性别
	* @return gender
	*/
	public String getGender() {
		return gender;
  	}

	/**
	* set性别
	* @return gender
	*/
	public void setGender(String gender) {
		this.gender = gender;
 	}

	/**
	* get手机号
	* @return tel
	*/
	public String getTel() {
		return tel;
  	}

	/**
	* set手机号
	* @return tel
	*/
	public void setTel(String tel) {
		this.tel = tel;
 	}

	/**
	* get邮箱
	* @return email
	*/
	public String getEmail() {
		return email;
  	}

	/**
	* set邮箱
	* @return email
	*/
	public void setEmail(String email) {
		this.email = email;
 	}

	/**
	* get身份证号
	* @return idcard
	*/
	public String getIdcard() {
		return idcard;
  	}

	/**
	* set身份证号
	* @return idcard
	*/
	public void setIdcard(String idcard) {
		this.idcard = idcard;
 	}

	/**
	* get出生日期
	* @return birth_date
	*/
	public String getBirth_date() {
		return birth_date;
  	}

	/**
	* set出生日期
	* @return birth_date
	*/
	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
 	}

	/**
	* get籍贯
	* @return native_place
	*/
	public String getNative_place() {
		return native_place;
  	}

	/**
	* set籍贯
	* @return native_place
	*/
	public void setNative_place(String native_place) {
		this.native_place = native_place;
 	}

	/**
	* get居住地址
	* @return address
	*/
	public String getAddress() {
		return address;
  	}

	/**
	* set居住地址
	* @return address
	*/
	public void setAddress(String address) {
		this.address = address;
 	}

	/**
	* get头像
	* @return photo
	*/
	public String getPhoto() {
		return photo;
  	}

	/**
	* set头像
	* @return photo
	*/
	public void setPhoto(String photo) {
		this.photo = photo;
 	}

	/**
	* get是否启用
	* @return is_start
	*/
	public String getIs_start() {
		return is_start;
  	}

	/**
	* set是否启用
	* @return is_start
	*/
	public void setIs_start(String is_start) {
		this.is_start = is_start;
 	}

	/**
	* get推荐人
	* @return parentid
	*/
	public String getParentid() {
		return parentid;
  	}

	/**
	* set推荐人
	* @return parentid
	*/
	public void setParentid(String parentid) {
		this.parentid = parentid;
 	}

	/**
	* get用户级别
	* @return zuser_level
	*/
	public String getZuser_level() {
		return zuser_level;
  	}

	/**
	* set用户级别
	* @return zuser_level
	*/
	public void setZuser_level(String zuser_level) {
		this.zuser_level = zuser_level;
 	}

	/**
	* get用户岗位级别
	* @return user_role_level
	*/
	public String getUser_role_level() {
		return user_role_level;
  	}

	/**
	* set用户岗位级别
	* @return user_role_level
	*/
	public void setUser_role_level(String user_role_level) {
		this.user_role_level = user_role_level;
 	}

	/**
	* get微信OpenId
	* @return openid
	*/
	public String getOpenid() {
		return openid;
  	}

	/**
	* set微信OpenId
	* @return openid
	*/
	public void setOpenid(String openid) {
		this.openid = openid;
 	}

	/**
	* get登陆密码
	* @return login_password
	*/
	public String getLogin_password() {
		return login_password;
  	}

	/**
	* set登陆密码
	* @return login_password
	*/
	public void setLogin_password(String login_password) {
		this.login_password = login_password;
 	}

	/**
	* get支付密码
	* @return super_password
	*/
	public String getSuper_password() {
		return super_password;
  	}

	/**
	* set支付密码
	* @return super_password
	*/
	public void setSuper_password(String super_password) {
		this.super_password = super_password;
 	}

	/**
	* get系统风格
	* @return z_themes
	*/
	public String getZ_themes() {
		return z_themes;
  	}

	/**
	* set系统风格
	* @return z_themes
	*/
	public void setZ_themes(String z_themes) {
		this.z_themes = z_themes;
 	}

}
