<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>导入数据</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
function UploadDataFile(){
	var zid = newid();
	if(isNotNull(zid)){
		uploadFile('importExcel',zid)
	}else{
		parent.alert_error('生成临时ID出错，无法打开上传文件页面');
	}
}
function CheckData(){
	var url = $("#importExcel_id").val();
	if(isNotNull(url)){
		url = encodeURIComponent(url);
		$.ajax({
			type : "get",
			url : "import_data_check",
		    async: false,
			data:{url:url},
			success : function(data) {
				$("#info_id").val(data.msg);
			},
			error: function (data) {
				parent.alertErrorMessager("ajax错误："+JSON.stringify(data));
			}
		});
	}else{
		parent.alert_info('请先上传数据导入文件');
	}
}
function ImportDataToDB(){
	var url = $("#importExcel_id").val();
	if(isNotNull(url)){
		$.messager.confirm('信息提示','您确定要执行导入系统操作吗？',function(r){
			if (r){
				url = encodeURIComponent(url);
				$.ajax({
					type : "get",
					url : "import_data_todb",
				    async: false,
					data:{url:url},
					success : function(data) {
						$("#info_id").val(data.msg);
					},
					error: function (data) {
						parent.alertErrorMessager("ajax错误："+JSON.stringify(data));
					}
				});
			}
		});
	}else{
		parent.alert_info('请先上传数据导入文件');
	}
}
</script>
</head>
<body>
<p>
<div class="container-fluid">
  <div class="row">
    <div class="col-6">
      <button type="button" class="btn btn-secondary" onclick="UploadDataFile();">1-上传数据文件</button>
			<button type="button" class="btn btn-secondary" onclick="CheckData();">2-检查数据有效性</button>
			<button type="button" class="btn btn-secondary" onclick="ImportDataToDB();">3-执行导入系统</button>
    </div>
    <div class="col-5 text-right">
      <div style="color: red;font-size: 20px;">数据导入模板文件，请到【开发/表单管理】中下载 </div>
    </div>
    <div class="col-1">
    	<button class="btn btn-secondary" onclick="parent.addTab('表单管理','list?tableId=z_form');">下载模板</button>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
     <hr>
		<z:column column_id="importExcel" column_type="0" column_help="模板文件"  column_size="12"></z:column>
		<hr>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
		<z:column column_id="info" column_type="1" column_help=""  column_size="12" textarea_height="200"></z:column>
    </div>
  </div>
</div>
</body>
</html>