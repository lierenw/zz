<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common/common.jsp"%>
<link  href="<%=request.getContextPath()%>/js/json-viewer/jquery.json-viewer.css" rel="stylesheet" type="text/css"/>
<script src="<%=request.getContextPath()%>/js/json-viewer/jquery.json-viewer.js"></script>
</head>
<body>

<div id="SystemStateTab" class="easyui-tabs" fit="true">
	<div title="system_state"><pre id="system_state_pre"></pre></div>
	<div title="jobRunList"><pre id="jobRunList_pre"></pre></div>
	<!-- <div title="dbsMap"><pre id="dbsMap_pre"></pre></div>
	<div title="tables"><pre id="tables_pre"></pre></div>
	<div title="reports"><pre id="reports_pre"></pre></div>
	<div title="wf"><pre id="wf_pre"></pre></div>
	<div title="code"><pre id="code_pre"></pre></div>
	<div title="sp"><pre id="sp_pre"></pre></div>
	<div title="accesskey"><pre id="accesskey_pre"></pre></div>
	<div title="httpservices"><pre id="httpservices_pre"></pre></div>
	<div title="orgs"><pre id="orgs_pre"></pre></div>
	<div title="users"><pre id="users_pre"></pre></div>
	<div title="session_users"><pre id="session_users_pre"></pre></div>
	<div title="cms"><pre id="cms_pre"></pre></div>
	<div title="cmsColumn"><pre id="cmsColumn_pre"></pre></div>
	<div title="cmsList"><pre id="cmsList_pre"></pre></div> -->
</div>

<script type="text/javascript">
	var yes = {collapsed: true};
	var no = {collapsed: false};
	var system_state_data = ${info.system_state};
	var jobRunList_data = ${info.jobRunList};
	/* var dbsMap_data = ${info.dbsMap};
	var tables_data = ${info.tables};
	var reports_data = ${info.reports};
	var wf_data = ${info.wf};
	var code_data = ${info.code};
	var sp_data = ${info.sp};
	var accesskey_data = ${info.accesskey};
	var httpservices_data = ${info.httpservices};
	var orgs_data = ${info.orgs};
	var users_data = ${info.users};
	var session_users_data = ${info.session_users};
	var cms_data = ${info.cms};
	var cmsColumn_data = ${info.cmsColumn};
	var cmsList_data = ${info.cmsList}; */
	
	$('#system_state_pre').jsonViewer(system_state_data,no);
	$('#jobRunList_pre').jsonViewer(jobRunList_data,no);
	
	/* $('#dbsMap_pre').jsonViewer(dbsMap_data,no);
	$('#tables_pre').jsonViewer(tables_data,yes);
	$('#reports_pre').jsonViewer(reports_data,yes);
	$('#wf_pre').jsonViewer(wf_data,yes);
	$('#code_pre').jsonViewer(code_data,yes);
	$('#sp_pre').jsonViewer(sp_data,no);
	$('#accesskey_pre').jsonViewer(accesskey_data,no);
	$('#httpservices_pre').jsonViewer(httpservices_data,yes);
	$('#orgs_pre').jsonViewer(orgs_data,yes);
	$('#users_pre').jsonViewer(users_data,yes);
	$('#session_users_pre').jsonViewer(session_users_data,yes);
	$('#cms_pre').jsonViewer(cms_data,yes);
	$('#cmsColumn_pre').jsonViewer(cmsColumn_data,yes);
	$('#cmsList_pre').jsonViewer(cmsList_data,yes); */
</script>
<%@include file="/views/common/body.jsp"%>
</body>
</html>