<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
//初始化方法
$(document).ready(function() {
	initSearchBoxHeight();
});
</script>
<style type="text/css">

</style>
</head>
<body class="easyui-layout">
<div id="list_north_div" data-options="region:'north'" class="border-bottom" style="overflow:hidden;">
<div id="ButtonHR" class="row"><div class="col-sm border-bottom">
<button id="z_user_select_button" type="button" class="btn btn-light" onclick="z_user_select_button();"><i class="fa fa-search"></i> 查询</button>
<button id="z_user_add_button" type="button" class="btn btn-light" onclick="z_user_add_button();"><i class="fa fa-file-text"></i> 新增</button>
<button id="z_user_edit_button" type="button" class="btn btn-light" onclick="z_user_edit_button();"><i class="fa fa-pencil-square-o"></i> 修改</button>
<button id="z_user_remove_button" type="button" class="btn btn-light" onclick="z_user_remove_button();"><i class="fa fa-trash"></i> 删除</button>
<button id="z_user_moveUp_button" type="button" class="btn btn-light" onclick="z_user_moveUp_button();"><i class="fa fa-arrow-up"></i> 上移</button>
<button id="z_user_moveDown_button" type="button" class="btn btn-light" onclick="z_user_moveDown_button();"><i class="fa fa-arrow-down"></i> 下移</button>
<button id="init_login_password" type="button" class="btn btn-light" onclick="init_login_password();"><i class="fa fa-braille"></i> 初始化登录密码</button>
<button id="init_super_password" type="button" class="btn btn-light" onclick="init_super_password();"><i class="fa fa-braille"></i> 初始化支付密码</button>
<button id="user_start" type="button" class="btn btn-light" onclick="user_start();"><i class="fa fa-unlock"></i> 启用</button>
<button id="user_prohibit" type="button" class="btn btn-light" onclick="user_prohibit();"><i class="fa fa-unlock-alt"></i> 禁用</button>
<button id="z_user_look_button" type="button" class="btn btn-light" onclick="z_user_look_button();"><i class="fa fa-eye"></i> 查看</button>
<button id="z_user_print_list_button" type="button" class="btn btn-light" onclick="z_user_print_list_button();"><i class="fa fa-print"></i> 打印</button>
</div></div>

<div id="oa_wid_windows" class="easyui-window" title="请选择申请的流程" style="width:500px;height:300px" data-options="iconCls:'fa fa-server',closed:true,modal:true,shadow:false,minimizable:false,collapsible:false">
</div>



<div id="search_div" class="container-fluid mt-3">
	<div class="row">
	
		<div class="col-sm-6 mb-2 d-flex align-items-start">
			<div class="input-group">
				<div class="input-group-prepend"><div class="input-group-text ">所属组织：</div></div>
				<input id="user_orgid_id" name="user_orgid" type="hidden" value="f8f4dffc8c84464rao81251784gd331b3cz302b78261716152205">
				<input placeholder="所属组织" id="user_orgid_display" onclick="Z5list('z_user_user_orgid','user_orgid_id','user_orgid_display',0)" name="user_orgid_display" value="集团_人力资源部" type="text" class="form-control marginleft1 border-right-0" readonly="">
				<span class="input-group-prepend marginright1 border-top border-right border-bottom">
					<button class="btn btn-light" onclick="Z5Clear('z_user_user_orgid','user_orgid_id','user_orgid_display')" type="button"><i class="fa fa-trash-o"></i></button>
					<button id="user_orgid_selectbutton_id" class="btn btn-light" onclick="Z5list('z_user_user_orgid','user_orgid_id','user_orgid_display',0)" type="button"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</div>
		
		<div class="col-sm-3 mb-2 d-flex align-items-start">
			<div class="input-group">
				<div class="input-group-prepend"><div class="input-group-text  ">所属角色：</div></div>
				<input id="user_role_id" name="user_role" type="hidden" value="0e916e37c029472eba4le6796dro512517aba98z1130252425192006">
				<input placeholder="所属角色" id="user_role_display" onclick="Z5list('z_user_user_role','user_role_id','user_role_display',0)" name="user_role_display" value="人事专员" type="text" class="form-control marginleft1 border-right-0" readonly="">
				<span class="input-group-prepend marginright1 border-top border-right border-bottom">
					<button class="btn btn-light" onclick="Z5Clear('z_user_user_role','user_role_id','user_role_display')" type="button"><i class="fa fa-trash-o"></i></button>
					<button id="user_role_selectbutton_id" class="btn btn-light" onclick="Z5list('z_user_user_role','user_role_id','user_role_display',0)" type="button"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</div>
		
		<div class="col-sm-3 mb-2 d-flex align-items-start">
			<div class="input-group">
				<div class="input-group-prepend"><div class="input-group-text  ">登录账号：</div></div>
				<input placeholder="登录账号" id="user_id_id" name="user_id" type="text" class="form-control" value="rszy1">
			</div>
		</div>
	
	</div>
	
	
	<div class="row">
		<div class="col-sm-12 mt-4 text-center">
			<button type="button" class="btn btn-secondary" onclick="z_user_select_button();"><i class="fa fa fa-tasks"></i> 高级查询</button>
			<button type="button" class="btn btn-secondary" onclick="form_clear()"><i class="fa fa-trash-o"></i> 清空</button>
 			<button type="button" class="btn btn-secondary" onclick="form_query(1)"><i class="fa fa-search"></i> 查询</button>
		</div>
	</div>
</div>


</div>
<form id="select_form" action="list" >
<input type="hidden" id="query_terms_id" name="query_terms" value='null' />
<input type="hidden" id="ComparisonColumnIdsJsonId" value='{"is_start":{"arrangement_direction":"0","column_default":"1","column_default_type":"0","column_help":"是否启用","column_id":"is_start","column_length_db":"","column_name":"是否启用","column_size":"3","column_type":"7","colunm_length_list":"150","compare":"0","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"1","is_hidden_list":"0","is_null":"0","is_readonly":"1","isbr":"0","ishr":"0","mode_type":"javascript","p_code_id":"yesorno","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"291","textarea_height":"0","tr_href":"","update_time":"2020-04-27 13:57:23.0","update_user":"null","z5_key":"","z5_table":"","z5_value":"","z5_value2":"","zid":"3df3768fc2ff4c888bebdfd9d1852f3c","zversion":"0"},"address":{"arrangement_direction":"0","column_default":"","column_default_type":"","column_help":"居住地址","column_id":"address","column_length_db":"256","column_name":"居住地址","column_size":"6","column_type":"0","colunm_length_list":"150","compare":"1","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_readonly":"0","isbr":"0","ishr":"0","mode_type":"javascript","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"289","textarea_height":"0","tr_href":"","update_time":"2020-04-26 12:40:30.0","update_user":"null","z5_key":"","z5_table":"","z5_value":"","z5_value2":"","zid":"51f0be3a908e4126b315162e0d0b4458","zversion":"0"},"gender":{"arrangement_direction":"1","column_default":"","column_default_type":"","column_help":"性别","column_id":"gender","column_length_db":"","column_name":"性别","column_size":"3","column_type":"6","colunm_length_list":"150","compare":"0","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_readonly":"0","isbr":"0","ishr":"0","mode_type":"javascript","p_code_id":"gender","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"283","textarea_height":"0","tr_href":"","update_time":"2020-12-10 11:13:32.0","update_user":"null","z5_key":"","z5_table":"","z5_value":"","z5_value2":"","zid":"5cc4218f5bcf47dc87b228e5c2fa5d8a","zversion":"0"},"user_name":{"arrangement_direction":"0","column_default":"","column_default_type":"","column_help":"账号名称","column_id":"user_name","column_length_db":"","column_name":"账号名称","column_size":"3","column_type":"0","colunm_length_list":"200","compare":"1","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"0","is_null":"1","is_readonly":"0","isbr":"1","ishr":"1","mode_type":"javascript","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"281","textarea_height":"0","tr_href":"","update_time":"2020-04-26 12:57:55.0","update_user":"null","z5_key":"","z5_table":"","z5_value":"","z5_value2":"","zid":"0873d8e7aec344f186532cdc8a6309ee","zversion":"0"},"openid":{"arrangement_direction":"0","column_default":"","column_default_type":"","column_help":"微信OpenId","column_id":"openid","column_length_db":"","column_name":"微信OpenId","column_size":"3","column_type":"0","colunm_length_list":"150","compare":"0","create_time":"2019-07-11 14:06:53.0","create_user":"null","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_null":"0","is_readonly":"1","isbr":"0","ishr":"0","mode_type":"","orgid":"null","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"295","textarea_height":"200","tr_href":"","update_time":"2020-03-06 10:18:47.0","update_user":"sa","z5_key":"","z5_table":"","z5_value":"","zid":"276c733e8aobucodm8m4tz4r1251790n8lbel50612517da5125171265ff40741322301016132815281124242012132612162719","zversion":"0"},"birth_date":{"arrangement_direction":"0","column_default_type":"","column_help":"出生日期","column_id":"birth_date","column_name":"出生日期","column_size":"3","column_type":"9","colunm_length_list":"150","compare":"0","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_readonly":"0","isbr":"0","ishr":"0","mode_type":"javascript","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"287","textarea_height":"0","update_time":"2020-03-06 10:22:38.0","update_user":"sa","zid":"2a283b668f8d480a8c183bf19ef9b22c","zversion":"0"},"user_role_level":{"arrangement_direction":"0","column_default":"7","column_default_type":"0","column_help":"用户岗位级别","column_id":"user_role_level","column_length_db":"","column_name":"用户岗位级别","column_size":"3","column_type":"7","colunm_length_list":"150","compare":"0","create_time":"2019-09-12 15:13:04.0","create_user":"null","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_null":"0","is_readonly":"0","isbr":"0","ishr":"0","orgid":"null","p_code_id":"role_level","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"294","textarea_height":"200","tr_href":"","update_time":"2020-04-26 12:29:03.0","update_user":"null","z5_key":"","z5_table":"","z5_value":"","z5_value2":"","zid":"be829014faeonb4edaal48fomm125172125171buzec3l0r0t1251742e18b7771924171825192129142428132229111829231219","zversion":"0"},"photo":{"arrangement_direction":"0","column_default":"","column_default_type":"","column_help":"头像","column_id":"photo","column_length_db":"","column_name":"头像","column_size":"6","column_type":"4","colunm_length_list":"150","compare":"0","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_readonly":"0","isbr":"1","ishr":"1","mode_type":"javascript","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"290","textarea_height":"0","tr_href":"","update_time":"2020-04-26 12:41:43.0","update_user":"null","z5_key":"","z5_table":"","z5_value":"","z5_value2":"","zid":"7699464fa48b40d7a8c4bab93e1e2e9e","zversion":"0"},"parentid":{"arrangement_direction":"0","column_help":"推荐人","column_id":"parentid","column_name":"推荐人","column_size":"3","column_type":"8","colunm_length_list":"150","compare":"0","create_time":"2019-04-16 09:20:17.0","create_user":"sa","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_null":"0","is_readonly":"0","isbr":"0","ishr":"0","orgid":"null","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"292","textarea_height":"200","update_time":"2020-03-06 10:18:47.0","update_user":"sa","z5_key":"zid","z5_table":"z_user","z5_value":"user_id","zid":"ad9c486440cua47ln00e9colae1251712517a2emorb7aff72tm12517e55fz423026232122291930202517162218191411301619","zversion":"0"},"realname":{"arrangement_direction":"0","column_default_type":"","column_help":"真实姓名","column_id":"realname","column_name":"真实姓名","column_size":"3","column_type":"0","colunm_length_list":"150","compare":"1","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_readonly":"0","isbr":"0","ishr":"0","mode_type":"javascript","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"282","textarea_height":"0","update_time":"2020-03-06 10:22:38.0","update_user":"sa","zid":"4d884fbc3530466bbf16c8f0d5eca82a","zversion":"0"},"user_role":{"arrangement_direction":"1","column_default":"","column_default_type":"","column_help":"所属角色","column_id":"user_role","column_length_db":"","column_name":"所属角色","column_size":"3","column_type":"8","colunm_length_list":"150","compare":"0","create_time":"2020-04-26 12:34:18.0","create_user":"null","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"0","is_null":"1","is_readonly":"0","isbr":"1","ishr":"0","orgid":"null","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"279","textarea_height":"200","tr_href":"","update_time":"2020-04-26 12:55:32.0","update_user":"null","z5_key":"zid","z5_table":"z_role","z5_value":"rolename","z5_value2":"","zid":"f09522cdcb12517obldac4ufmctf8nlr9acm5ozaee3a1251712517f347961481924151018212816211112301016292318202519","zversion":"0"},"native_place":{"arrangement_direction":"0","column_default":"","column_default_type":"","column_help":"籍贯","column_id":"native_place","column_length_db":"128","column_name":"籍贯","column_size":"6","column_type":"8","colunm_length_list":"150","compare":"0","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_readonly":"0","isbr":"0","ishr":"0","mode_type":"javascript","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"288","textarea_height":"0","tr_href":"","update_time":"2020-04-26 12:40:21.0","update_user":"null","z5_key":"area_id","z5_table":"z_area","z5_value":"area_name","z5_value2":"","zid":"b5980257eb304b328f9b124191bedca8","zversion":"0"},"user_orgid":{"arrangement_direction":"1","column_default":"","column_default_type":"","column_help":"所属组织","column_id":"user_orgid","column_length_db":"","column_name":"所属组织","column_size":"6","column_type":"8","colunm_length_list":"400","compare":"0","create_time":"2020-04-26 12:32:25.0","create_user":"null","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"0","is_null":"1","is_readonly":"0","isbr":"0","ishr":"0","orgid":"null","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"278","textarea_height":"200","tr_href":"","update_time":"2020-04-26 12:52:23.0","update_user":"null","z5_key":"zid","z5_table":"z_org","z5_value":"full_org_name","z5_value2":"","zid":"a8a338337d5zu4c412517mmcade9e12517l2n12517f3aotlbr3obca42d306c11114202423152125172727212213292412172819","zversion":"0"},"user_id":{"arrangement_direction":"0","column_default":"","column_default_type":"","column_help":"登录账号","column_id":"user_id","column_length_db":"","column_name":"登录账号","column_size":"3","column_type":"0","colunm_length_list":"200","compare":"0","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"0","is_null":"1","is_readonly":"0","isbr":"0","ishr":"0","mode_type":"javascript","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"280","textarea_height":"0","tr_href":"","update_time":"2020-04-26 12:56:33.0","update_user":"null","z5_key":"","z5_table":"","z5_value":"","z5_value2":"","zid":"e0bae54c3b434fe6a1f89722e95d8346","zversion":"0"},"z_themes":{"arrangement_direction":"1","column_default":"gray","column_default_type":"0","column_help":"系统风格","column_id":"z_themes","column_name":"系统风格","column_size":"3","column_type":"7","colunm_length_list":"150","compare":"0","create_time":"2020-09-10 11:07:04.0","create_user":"null","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"0","is_null":"0","is_readonly":"0","isbr":"0","ishr":"0","orgid":"null","p_code_id":"z_themes","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"298","textarea_height":"200","update_time":"2020-09-10 11:07:04.0","update_user":"null","zid":"041e733044lfu4ocf4125179lc12517fnmacbe12517mbb4t40e4aofr7eze2f62818102729201625302015221412121012242419","zversion":"0"},"idcard":{"arrangement_direction":"0","column_default_type":"","column_help":"身份证号","column_id":"idcard","column_name":"身份证号","column_size":"3","column_type":"0","colunm_length_list":"150","compare":"0","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"1","is_readonly":"0","isbr":"0","ishr":"0","mode_type":"javascript","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"286","textarea_height":"0","update_time":"2020-03-06 10:22:38.0","update_user":"sa","zid":"b15c0cee42874877a3df8c19125da2a6","zversion":"0"},"tel":{"arrangement_direction":"0","column_default":"","column_default_type":"","column_help":"手机号","column_id":"tel","column_length_db":"","column_name":"手机号","column_size":"3","column_type":"0","colunm_length_list":"200","compare":"0","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"0","is_readonly":"0","isbr":"0","ishr":"0","mode_type":"javascript","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"284","textarea_height":"0","tr_href":"","update_time":"2020-04-26 12:30:18.0","update_user":"null","z5_key":"","z5_table":"","z5_value":"","z5_value2":"","zid":"c23ddf4415b7441d92b46750f08844ce","zversion":"0"},"email":{"arrangement_direction":"0","column_default":"","column_default_type":"","column_help":"邮箱","column_id":"email","column_length_db":"","column_name":"邮箱","column_size":"3","column_type":"0","colunm_length_list":"200","compare":"0","create_time":"2019-01-31 09:52:48.0","is_hidden":"0","is_hidden_edit":"0","is_hidden_list":"0","is_readonly":"0","isbr":"0","ishr":"0","mode_type":"javascript","p_code_id":"","pid":"262d2aa6f016426bbfce5552ecbff688","seq":"285","textarea_height":"0","tr_href":"","update_time":"2020-04-26 12:57:49.0","update_user":"null","z5_key":"","z5_table":"","z5_value":"","z5_value2":"","zid":"8f9fc5444b8441bb94117850f034d615","zversion":"0"}}' />
<input type="hidden" id="ComparisonColumnIdsId" value='user_orgid☆所属组织,user_role☆所属角色,user_id☆登录账号,user_name☆账号名称,realname☆真实姓名,gender☆性别,tel☆手机号,email☆邮箱,idcard☆身份证号,birth_date☆出生日期,native_place☆籍贯,address☆居住地址,photo☆头像,is_start☆是否启用,parentid☆推荐人,user_role_level☆用户岗位级别,openid☆微信OpenId,z_themes☆系统风格' />
<input type="hidden" id="pagenum_id" name="pagenum" value="1" />
<input type="hidden" id="rowcount_id" name="rowcount" value="30" />
<input type="hidden" id="tableId_id" name="tableId" value="z_user" />
<input type="hidden" id="TableColunmId" name="TableColunmId" value="null" />
<input type="hidden" id="ReturnKeyColumnId" name="ReturnKeyColumnId" value="null" />
<input type="hidden" id="ReturnValueColumnId" name="ReturnValueColumnId" value="null" />
<input type="hidden" id="Z5QueryParameters" name="Z5QueryParameters" value="null" />
<input type="hidden" id="PageType" name="PageType" value="list" />
<input type="hidden" id="zid_id" name="zid" value="null" />
<input type="hidden" id="orderby_id" name="orderby" value="null" />
<input type="hidden" id="orderby_pattern_id" name="orderby_pattern" value="null" />
<input type="hidden" id="list_dblclick_id" name="list_dblclick" value="0" />
</form>
 <div id="select_windows" class="easyui-window" title="查询条件" style="width:900px;height:600px" data-options="iconCls:'fa fa-filter',modal:true,closed:true,shadow:false,minimizable:false,maximizable:false,collapsible:false,closable:true"> 
 <div class="easyui-layout" data-options="fit:true,border:false"> 
 <div data-options="region:'west',split:false" style="width:260px;"> 
 <table id="QueryInfoList" class="easyui-datagrid" data-options="singleSelect:true,onClickRow:SelectQueryInfoOnDblClickRow,fit:true,nowrap:false,border:false"> 
 <thead> 
 	<tr>
 		<th data-options="field:'zid',hidden:true">zid</th>
 		<th data-options="field:'name',width:258">常用查询条件</th>
 		<th data-options="field:'queryinfo',hidden:true">queryinfo</th>
 	</tr> 
 </thead> 
 <tbody> 
 </tbody> 
 </table> 
 </div> 
 <div data-options="region:'center',border:false" > 
 	<div data-options="region:'north',border:false"  >
 		<div class="easyui-panel" style="padding:2px;">
 			<a id="AddQueryRomButton" href="javascript:void(0);" class="easyui-linkbutton" onclick="AddQueryRom()" data-options="plain:true,iconCls:'fa fa-plus-circle'">增加行</a>
 			<a id="DeleteQueryTermsButton" href="javascript:void(0);" class="easyui-linkbutton" onclick="DeleteQueryInfo()" data-options="plain:true,iconCls:'fa fa-trash-o'">删除常用查询条件</a>
 			<a id="SaveQueryTermsButton" href="javascript:void(0);" class="easyui-linkbutton" onclick="SaveQueryTerms()" data-options="plain:true,iconCls:'fa fa-floppy-o'">保存查询条件</a>
 		</div>
 	</div>
 <table id="QueryTable"  class="table table-sm table-bordered"> 
 <thead> 
 	<tr>
 		<th width="30%">查询列</th>
 		<th width="15%">比较符</th>
 		<th width="35%">查询内容</th>
 		<th width="15%">条件连接</th>
 		<th width="5%"></th>
 	</tr> 
 </thead> 
 <tbody> 
 </tbody> 
 </table> 
 </div> 
 <div data-options="region:'south'" style="height:60px; text-align:center;padding-top: 10px"> 
 <button type="button" class="btn btn-light" onclick="form_clear()"><i class="fa fa-trash-o"></i> 清空</button>
 <button type="button" class="btn btn-light" onclick="form_query(1)"><i class="fa fa-search"></i> 查询</button>
 </div> 
 </div> 
 </div> 
<div data-options="region:'center',border:false">
<table id="MainTable" class="easyui-datagrid" data-options="fit:true,ctrlSelect:true,resizeEdge:20,nowrap:false,onDblClickCell:DblClickList">
<thead data-options="frozen:true">
<tr>
<th data-options="field:'zid',checkbox:true">主键</th>
</tr>
</thead>
<thead>
<tr>
<th  data-options="field:'user_orgid',width:400"><button type="button" onClick="SortList('user_orgid')" class="btn btn-sm btn-light btn-block">所属组织</button></th>
<th  data-options="field:'user_role',width:150"><button type="button" onClick="SortList('user_role')" class="btn btn-sm btn-light btn-block">所属角色</button></th>
<th  data-options="field:'user_id',width:200"><button type="button" onClick="SortList('user_id')" class="btn btn-sm btn-light btn-block">登录账号</button></th>
<th  data-options="field:'user_name',width:200"><button type="button" onClick="SortList('user_name')" class="btn btn-sm btn-light btn-block">账号名称</button></th>
<th  data-options="field:'tel',width:200"><button type="button" onClick="SortList('tel')" class="btn btn-sm btn-light btn-block">手机号</button></th>
<th  data-options="field:'email',width:200"><button type="button" onClick="SortList('email')" class="btn btn-sm btn-light btn-block">邮箱</button></th>
<th  data-options="field:'is_start',width:150"><button type="button" onClick="SortList('is_start')" class="btn btn-sm btn-light btn-block">是否启用</button></th>
<th  data-options="field:'z_themes',width:150"><button type="button" onClick="SortList('z_themes')" class="btn btn-sm btn-light btn-block">系统风格</button></th>
</tr>
</thead>
<tbody>
<tr>
<td>c0c373adbbs112517c4u4c8bcca62zdd8bre31fe6822111410303006</td>
<td>集团</td>
<td>系统管理员</td>
<td>admin</td>
<td>管理理</td>
<td></td>
<td></td>
<td><span style="color:#FF0000;">是</span></td>
<td></td>
</tr>
<tr>
<td>sa</td>
<td>集团</td>
<td>员工</td>
<td>sa</td>
<td>超级管理员</td>
<td></td>
<td></td>
<td><span style="color:#FF0000;">是</span></td>
<td>gray</td>
</tr>
<tr>
<td>9e2ced12b38sz341251735rdeb2350u8b3b5d2f9e211142311191806</td>
<td>集团_人力资源部</td>
<td>人事专员</td>
<td>rszy1</td>
<td>人事专员1</td>
<td></td>
<td></td>
<td><span style="color:#FF0000;">是</span></td>
<td></td>
</tr>
<tr>
<td>c7879046662144za12517sc8ere780377b7u91832714162817212106</td>
<td>集团_财务部</td>
<td>财务经理</td>
<td>cwjl</td>
<td>财务经理</td>
<td></td>
<td></td>
<td><span style="color:#FF0000;">是</span></td>
<td></td>
</tr>
<tr>
<td>78b334db968e4ef9bf3d59s12517u4dreee3a51z6c30222322282706</td>
<td>集团_财务部_资金组</td>
<td>员工</td>
<td>chuna</td>
<td>出纳专员</td>
<td></td>
<td></td>
<td><span style="color:#FF0000;">是</span></td>
<td></td>
</tr>
</tbody>
</table>
</div>
<div data-options="region:'south',border:false" style="height:40px;overflow:hidden">
<ul class="pagination ">
<li class="page-item disabled"><a class="page-link" href="#">首页</a></li>
<li class="page-item disabled"><a class="page-link" href="#">上一页</a></li>
<li class="page-item active"><a class="page-link" href="javascript:void(0);" onclick="form_query(1)"  >1</a></li> 
<li class="page-item disabled"><a class="page-link" href="#">下一页</a></li>
<li class="page-item disabled"><a class="page-link" href="#">末页</a></li>
<li class="page-item disabled"><a class="page-link" href="#">1-1页/5条</a></li>
<li class="page-item disabled">&nbsp;&nbsp;&nbsp;</li>
<li class="page-item disabled"><a class="page-link" href="#">跳到</a></li>
<li class="page-item"><input type="text" class="page-link" style="width:40px;" id="JumpRownumId"></li>
<li class="page-item disabled"><a class="page-link" href="#">页</a></li>
<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="JumpRow()">&nbsp;<i class="fa fa-paper-plane-o"></i>&nbsp;跳转</a></li>
<li class="page-item disabled">&nbsp;&nbsp;&nbsp;</li>
<li class="page-item disabled"><a class="page-link" href="#">每页显示:</a></li>
<li class="page-item active"><a class="page-link" href="javascript:void(0);" onclick="setRowCount(30)">30</a></li>
<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="setRowCount(50)">50</a></li>
<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="setRowCount(9999999)">全部</a></li>
<li class="page-item disabled">&nbsp;&nbsp;&nbsp;</li>
<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="ExportExcel()">&nbsp;<i class="fa fa-download"></i>&nbsp;导出</a></li>
<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="RefreshList()">&nbsp;<i class="fa fa-refresh"></i>&nbsp;刷新</a></li>
</ul>
</div>
 <script type="text/javascript"> 
 $(document).ready(function(){ 
 checkRowForCheckedZid('null','list','MainTable',''); 
 }); 
 function z_user_select_button(){ 
openSelectWindows();
 } 
 function z_user_add_button(){ 
add();
 } 
 function z_user_edit_button(){ 
edit();
 } 
 function z_user_remove_button(){ 

	var zids = getTableColumn('MainTable','zid');
	if(zids==''){
		parent.alertMessager('请选择要删除的记录');
	}else{
		$.messager.confirm('信息提示','您确定要删除这些记录吗？',function(r){
			if (r){
				parent.openLoading();
				$.ajax({
					type : "get",
					url : 'UserDelete',
					data:{zids:zids,tableId:'z_user'},
					success : function(data) {
						if(data.code=='SUCCESS'){
							parent.alertMessager('成功删除'+data.data+'条记录');
							RefreshList();
						}else{
							parent.alertErrorMessager(''+data.msg);
						}
					},
					error: function (data) {
						parent.alertErrorMessager('ajax错误：'+JSON.stringify(data));
					},
					complete: function () {
						//执行完成
						parent.closedLoading();
					}
				});

			}
		});
	}
 } 
 function z_user_moveUp_button(){ 
moveUp();
 } 
 function z_user_moveDown_button(){ 
moveDown();
 } 
 function init_login_password(){ 
var tableId = $('#tableId_id').val();
$.messager.confirm('信息提示','您确定要重新初始化此用户的登录密码？',function(r){
	if (r){
		var rows = $('#MainTable').datagrid('getSelections');
		for (var i = 0; i < rows.length; i++) {
			var zid = rows[i]['zid'];
			var password = '2dad747738130ebbb1e7127b0f0a5e7a';
			$.ajax({
				type : "get",
				url : 'update',
				data:{zid:zid,tableId:tableId,login_password:password},
				success : function(data) {
					if(data.code=='SUCCESS'){
						parent.alertMessager2('操作成功【初始化密码：123456】');
						form_query(1);
					}else{
						parent.alertErrorMessager(data.msg);
						return;
					}
				},
				error: function (data) {
					parent.alertErrorMessager('ajax错误：'+JSON.stringify(data));
				}
			});
		}
	}
});
 } 
 function init_super_password(){ 
var tableId = $('#tableId_id').val();
$.messager.confirm('信息提示','您确定要重新初始化此用户的支付密码？',function(r){
	if (r){
		var rows = $('#MainTable').datagrid('getSelections');
		for (var i = 0; i < rows.length; i++) {
			var zid = rows[i]['zid'];
			var password = '2dad747738130ebbb1e7127b0f0a5e7a';
			$.ajax({
				type : "get",
				url : 'update',
				data:{zid:zid,tableId:tableId,super_password:password},
				success : function(data) {
					if(data.code=='SUCCESS'){
						parent.alertMessager2('操作成功【初始化密码：123456】');
						form_query(1);
					}else{
						parent.alertErrorMessager(data.msg);
						return;
					}
				},
				error: function (data) {
					parent.alertErrorMessager('ajax错误：'+JSON.stringify(data));
				}
			});
		}
	}
});
 } 
 function user_start(){ 
var tableId = $('#tableId_id').val();
$.messager.confirm('信息提示','您确定要启用此用户吗？',function(r){
	if (r){
		var rows = $('#MainTable').datagrid('getSelections');
		for (var i = 0; i < rows.length; i++) {
			var zid = rows[i]['zid'];
			$.ajax({
				type : "get",
				url : 'update',
				data:{zid:zid,tableId:tableId,is_start:1},
				success : function(data) {
					if(data.code=='SUCCESS'){
						parent.alertMessager('操作成功');
						form_query(1);
					}else{
						parent.alertErrorMessager(data.msg);
						return;
					}
				},
				error: function (data) {
					parent.alertErrorMessager('ajax错误：'+JSON.stringify(data));
				}
			});
		}
	}
});
 } 
 function user_prohibit(){ 
var tableId = $('#tableId_id').val();
$.messager.confirm('信息提示','您确定要禁用此用户吗？',function(r){
	if (r){
		var rows = $('#MainTable').datagrid('getSelections');
		for (var i = 0; i < rows.length; i++) {
			var zid = rows[i]['zid'];
			$.ajax({
				type : "get",
				url : 'update',
				data:{zid:zid,tableId:tableId,is_start:0},
				success : function(data) {
					if(data.code=='SUCCESS'){
						parent.alertMessager('操作成功');
						form_query(1);
					}else{
						parent.alertErrorMessager(data.msg);
						return;
					}
				},
				error: function (data) {
					parent.alertErrorMessager('ajax错误：'+JSON.stringify(data));
				}
			});
		}
	}
});
 } 
 function z_user_look_button(){ 
look();
 } 
 function z_user_print_list_button(){ 
printList();
 } 
 </script> 




<!-- 添加Echars -->
<script src="/z/js/echarts.min.js"></script>

<!-- 加载等待提示框 -->
<div id="loading_windows" class="easyui-window" title=" "   data-options="resizable:false,draggable:false,closable:false,closed:true,modal:true,shadow:false,maximizable:false,minimizable:false,collapsible:false,border:false">
	<img alt="" src="/z/img/system/loading.gif" width="70px" height="10px"/>
</div>

</body>
</html>