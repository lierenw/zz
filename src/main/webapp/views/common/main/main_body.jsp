<%@ page pageEncoding="utf-8"%>
<div id="myTab-tools">
	<!-- 超级管理员功能 -->
 	<c:if test='${zuser.zid==sp.super_user}'>
		<button type="button" class="easyui-menubutton" data-options="menu:'#kaifa'"><i class="fa fa-gears"></i> 开发管理</button>
	</c:if>
	<button type="button" class="easyui-linkbutton" data-options="plain:true" onclick="my_workjob_list();"><i class="fa fa-tags"></i> 待办任务</button>
	<button type="button" class="easyui-linkbutton" data-options="plain:true" onclick="user_settings();"><i class="fa fa-sliders"></i> 设置</button>
	<button type="button" class="easyui-linkbutton" data-options="plain:true" onclick="UserExit();"><i class="fa fa-power-off"></i> 退出</button>
</div>

<div id="kaifa" style="width:100px;">
	<div data-options="iconCls:'fa fa-window-restore'" onclick="addTab('表单管理','list?tableId=z_form','fa fa-window-restore')">表单管理</div>
	<div data-options="iconCls:'fa fa-server'" onclick="addTab('HTTP接口','list?tableId=z_http_services','fa fa-server')">HTTP接口</div>
	<div data-options="iconCls:'fa fa-bar-chart'" onclick="addTab('报表管理','list?tableId=z_report','fa fa-bar-chart')">报表管理</div>
	<div data-options="iconCls:'fa fa-usb'" onclick="addTab('流程定义','list?tableId=z_workflow','fa fa-usb')">流程定义</div>
	<div data-options="iconCls:'fa fa-braille'" onclick="addTab('字典管理','list?tableId=z_code','fa fa-braille')">字典管理</div>
	<div class="menu-sep"></div>
	<div data-options="iconCls:'fa fa-map-pin'" onclick="addTab('任务管理','list?tableId=z_job','fa fa-map-pin')">任务管理</div>
	<div class="menu-sep"></div>
	<div data-options="iconCls:'fa fa-power-off'" onclick="RestartTomcat()">重启Tomcat</div>
	<div class="menu-sep"></div>
	<div data-options="iconCls:'fa fa-gears'" onclick="SetSystemParameter()">系统参数设置</div>
	<div data-options="iconCls:'fa fa-refresh'" onclick="LoadParameter()">更新缓存数据</div>
</div>
	
<div id="UserMessagesList" data-options="region:'east',hideCollapsedContent:false,collapsible:true,collapsed:true,split:true" title="系统用户：${user_messages_list.size()}人" style="width:200px;">
	<c:forEach items="${user_messages_list}" var="users_message">
		<c:if test='${users_message.zid!= zuser.zid}'>
			<c:if test='${users_message.isonline==0}'>
				<button id="message_user_${users_message.zid}" type="button" class="btn btn-success btn-block" onclick="OpenMessageWindows('${users_message.zid}','${users_message.user_name}')">${users_message.user_name} <c:if test='${users_message.messagecount>0}'>[${users_message.messagecount}]</c:if></button>
			</c:if>
			<c:if test='${users_message.isonline==1}'>
				<button id="message_user_${users_message.zid}" type="button" class="btn btn-light btn-block" onclick="OpenMessageWindows('${users_message.zid}','${users_message.user_name}')">${users_message.user_name} <c:if test='${users_message.messagecount>0}'>[${users_message.messagecount}]</c:if></button>
			</c:if>
		</c:if> 
	</c:forEach>
</div>
	
<!-- 图片展示框 -->
<div id="img_windows" class="easyui-window" title="图片" style="width:700px;height:500px;text-align:center" data-options="iconCls:'fa fa-picture-o',closed:true,modal:true,shadow:false,minimizable:false,collapsible:false">
</div>
	
<!-- 删除信息提示框 -->
<div id="error_windows" class="easyui-window" title="错误信息" style="width:700px;height:500px" data-options="iconCls:'fa fa-exclamation-triangle',closed:true,modal:true,shadow:false,minimizable:false,collapsible:false">
</div>
	
<!-- 信息提示2 -->
<div id="alert_windows" class="easyui-window" title="提示信息" style="width:700px;height:500px" data-options="iconCls:'fa fa-bullhorn',closed:true,modal:true,shadow:false,minimizable:false,collapsible:false">
</div>
	
<!-- 聊天窗口 -->
<div id="message_windows" class="easyui-window" title="系统管理员" style="width:800px;height:500px" data-options="closed:true,shadow:false,minimizable:false,cache:false">
	<div id="MessageTab" class="easyui-tabs"  border="false" fit="true" tabPosition="left"></div>
</div>
	
<!-- 加载等待提示框 -->
<div id="loading_windows" class="easyui-window" title="加载中"   data-options="resizable:false,draggable:false,closable:false,closed:true,modal:true,shadow:false,maximizable:false,minimizable:false,collapsible:false,border:false">
	<img alt="" src="<%=request.getContextPath()%>/img/system/loading.gif" width="70px" height="10px"/>
</div>
	
<!-- myTab右键菜单 -->
<div id="my_tab_menu" class="easyui-menu" style="width:120px;">
	<div data-options="name:'tab_refresh',iconCls:'fa fa-refresh'">刷新</div>
	<div class="menu-sep"></div>
	<div data-options="name:'tab_close_other_all',iconCls:'fa fa-window-close-o'">关闭其它功能</div>
    <div data-options="name:'tab_close_all',iconCls:'fa fa-window-close'">关闭全部功能</div>
</div>
