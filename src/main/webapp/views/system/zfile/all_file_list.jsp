<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">

/**
 * 删除文件
 */
function deleteLocalFile(zid,name){
	$.messager.confirm('信息提示','您确定删除 '+name+' 文件吗？',function(r){
		if (r){
			$.ajax({
				type : "get",
				url : 'deleteLocalFile',
				data:{zid:zid},
				success : function(data) {
					if(data.code=='SUCCESS'){
						alertMessager(''+data.msg);
						reload();
					}else{
						alertErrorMessager(''+data.msg);
					}
				},
				error: function (data) {
					alertErrorMessager('ajax错误：'+JSON.stringify(data));
				}
			});
		}
	});
}

</script>
</head>
<body>
<!-- 按钮区域 -->
	<div class="container-fluid mt-3 mb-3">
		<div class="row">
			<div class="col-12">
				<button type="button" class="btn btn-outline-info btn-lg" onclick="reload()">刷新列表</button> 
			</div>
		</div>
	</div>
	<!-- 文件列表 -->
	<div class="container-fluid">
			<div class="row d-flex align-items-center border-top border-bottom">
				<div class="col-md-1 border-right">
				</div>
				<div class="col-md-2 border-right">
					创建时间
				</div>
				<div class="col-md-2 border-right">
					文件名称
				</div>
				<div class="col-md-1 border-right">
					文件大小
				</div>
				<div class="col-md-6 border-right">
					访问地址 
				</div>
			</div>
		<c:forEach items="${filelist}" var="f">
			<div class="row d-flex align-items-center border-bottom">
				<div class="col-md-1 border-right">
					<button type="button" class="btn btn-danger" onclick="deleteLocalFile('${f.zid}','${f.name}')">删 除</button>
				</div>
				<div class="col-md-2 border-right">
					${f.create_time}
				</div>
				<div class="col-md-2 border-right text-wrap" style="word-break:break-all">
					<a href="${f.filepath}" target="_blank">${f.name}</a>
				</div>
				<div class="col-md-1 border-right">
					${f.size}
				</div>
				<div class="col-md-6 border-right text-wrap" style="word-break:break-all">
					${f.filepath}
				</div>
			</div>
		</c:forEach>
	</div>
</body>
</html>