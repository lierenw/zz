/********************移动端JS******************************************************************/
function OpenMain(){
	window.location.href='main';
}
function OpenFunction(){
	window.location.href='main_mobile_function';
}
function OpenUser(){
	window.location.href='main_mobile_user';
}
/******************框架层方法**********************************************************/
/**
 * 退出系统
 */
function UserExit(){
	$.messager.confirm('信息提示','您确定要退出系统吗？',function(r){
		if (r){
			if(ws!=null){
				//发送下线通知
				var userid = '${zuser.zid}';
				var msgObj = {
						fromuserid : userid,
						command_type:'user_off_line'
				};
				//发送消息
				ws.send(JSON.stringify(msgObj));
			}
			//退出系统
			window.location.href='UserExit';
		}
	});
}



/**
 * 获取表格指字列的内容
 * @param TableId 表ID
 * @param ColumnId 列ID
 * @returns 字符串  例：0001,0002
 */
function getTableColumn(TableId,ColumnId){
	var result = '';
	var rows = $('#'+TableId).datagrid('getSelections');
	for (var i = 0; i < rows.length; i++) {
		if(i!=rows.length-1){
			result = result + rows[i][ColumnId] + ',';
		}else{
			result = result + rows[i][ColumnId];
		}
	}
	return result;
}

/**
 * 判读是否系统字段
 * @param TableId
 * @returns
 */
function isSystemColumn(TableId){
	var result = false;
	var rows = $('#'+TableId).datagrid('getSelections');
	for (var i = 0; i < rows.length; i++) {
		var columnid = rows[i]['column_id'];
		if(columnid=='zid' ||
				columnid=='number' ||
				columnid=='name' ||
				columnid=='seq' ||
				columnid=='create_user' ||
				columnid=='create_time' ||
				columnid=='update_user' ||
				columnid=='update_time' ||
				columnid=='orgid' ||
				columnid=='zversion' ||
				columnid=='remarks'){
			result = true;
		}
	}
	return result;
}

/**
 * 判断是否为空
 * @param str
 * @returns {Boolean}
 */
function isNull(str){
	if(str == '' || str == null || typeof(str) == "undefined" || str == "undefined" || str == "null" || str == null){
		return true;
	}else{
		return false;
	}
}
/**
 * 判断是否不为空
 * @param str
 * @returns {Boolean}
 */
function isNotNull(str){
	if(str != '' && str != null && typeof(str) != "undefined" && str != "undefined" && str != "null" && str != null){
		return true;
	}else{
		return false;
	}
}

/**
 *获取日期
 * @returns
 */
function getDate(){
	var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate(); //获取当前日
    return year + "-" + mon + "-" + date;
}

/**
 * 获取时间
 * @returns
 */
function getTime(){
	var myDate = new Date;
    var h = myDate.getHours();//获取当前小时数(0-23)
    var m = myDate.getMinutes();//获取当前分钟数(0-59)
    var s = myDate.getSeconds();//获取当前秒
    return h + ":" + m + ":" + s;
}

/**
 * 获取日期时间
 * @returns
 */
function getDateTime(){
	var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate(); //获取当前日
    var h = myDate.getHours();//获取当前小时数(0-23)
    var m = myDate.getMinutes();//获取当前分钟数(0-59)
    var s = myDate.getSeconds();//获取当前秒
    return year + "-" + mon + "-" + date+" "+ h + ":" + m + ":" + s;
}

function alert_info(message){
	$.messager.alert('信息提示',message);
}

function alert_error(message){
	$.messager.alert('错误提示',message,'error');
}

function alert_warning(message){
	$.messager.alert('警告提示',message,'warning');
}

function openLoading(){
	try {
		if (typeof(eval("parent.openLoading")) == "function") {
			parent.openLoading();
		}else{
			$('#loading_windows').window('open');
		}
	} catch(e) {
	}
}
function closedLoading(){
	try {
		if (typeof(eval("parent.closedLoading")) == "function") {
			parent.closedLoading();
		}else{
			$('#loading_windows').window('close');
		}
	} catch(e) {
	}
}

function alertMessager(info){
	try {
		if (typeof(eval("alertMessager")) == "function") {
			parent.alertMessager(info);
		}else{
			alert_info(info);
		}
	} catch(e) {
	}
}

function alertErrorMessager(error_info){
	try {
		if (typeof(eval("alertErrorMessager")) == "function") {
			parent.alertErrorMessager(error_info);
		}else{
			alert_error(error_info);
		}
	} catch(e) {
	}
}




/**
 * 生成10位随机数
 * @returns
 */
function newid(){ 
	var i=Math.random()*(999999999-100000999)+100000999; 
	var j=parseInt(i,13);
	return j;
} 

/**
 * 获取所有选中多选框的值
 * @param ColunmId
 * @returns
 */
function getCheckedValue(KeyColunmId,ValueColunmId){
	var strgetSelectValue="";
	var getSelectValueMenbers = $("input[name='"+ValueColunmId+"']:checked").each(function(j) {     
		if (j >= 0) {        
			strgetSelectValue += $(this).val() + ","    
		}
	});
	if(strgetSelectValue.substr(strgetSelectValue.length - 1)==","){
		$('#'+KeyColunmId).val(strgetSelectValue.substr(0,strgetSelectValue.length - 1));
	}else{
		$('#'+KeyColunmId).val(strgetSelectValue);
	}

}

/**
 * 根据CodeId获取Code对象
 * @param CodeId
 * @returns
 */
function getCode(CodeId){
	var returnvalue;
	$.ajax({
		type : "GET",
		async:false,
		url : 'getCodeJson',
		data:{CodeId:CodeId},
		success : function(data) {
			if(data.code=='SUCCESS'){
				returnvalue = data.data;
			}else{
				alertErrorMessager('根据CodeId获取Code对象错误：'+data.data);
				returnvalue = '';
			}
		}
	});
	return returnvalue;
}

/**
 * 获取Z5显示值
 * @param tableColumnId
 * @returns
 */
function GetZ5DesplayValue(tableId,tdColumnId,tdColumnValue){
	var returnvalue;
	$.ajax({
		type : "GET",
		async:false,
		url : 'Z5DesplayValueJson',
		data:{tableId:tableId,tdColumnId:tdColumnId,tdColumnValue:tdColumnValue},
		success : function(data) {
			if(data.code=='SUCCESS'){
				returnvalue = data.data;
			}else{
				alertErrorMessager(data.data);
				returnvalue = '';
			}
		}
	});
	return returnvalue;
}

function GetZ5DesplayValueR(reportid,tdColumnId,tdColumnValue){
	var returnvalue;
	$.ajax({
		type : "GET",
		async:false,
		url : 'Z5DesplayValueJsonR',
		data:{reportid:reportid,tdColumnId:tdColumnId,tdColumnValue:tdColumnValue},
		success : function(data) {
			if(data.code=='SUCCESS'){
				returnvalue = data.data;
			}else{
				alertErrorMessager(data.data);
				returnvalue = '';
			}
		}
	});
	return returnvalue;
}


/**
 * 设置信息到Cookie
 * @param CookieId
 * @param key 
 * @param value
 * @returns
 */
function setCookie(CookieId,Value){
	$.cookie(CookieId, null, {path: '/'}); 
	$.cookie(CookieId, Value, {expires: 999,path: '/'});
}
/**
 * 获取Cookie中的信息
 * @param CookieId
 * @returns
 */
function getCookie(CookieId){
	var cookievalue = $.cookie(CookieId);
	var decodeURICookie = decodeURIComponent(cookievalue,true);
	return decodeURICookie;
}

/**
 * Form序列化
 * @returns
 */
function FormSerialize(){
	var str = $("#select_form").serialize();
	var CookieId = 'list?tableId='+$("#tableId_id").val();
	//加号换成空格
	str = str.replace(/\+/g, "");
	setCookie(CookieId,str);
}

/**
 * 列表页面-设置快速查询框高度
 */
function initSearchBoxHeight(){
	var divheight = $('#search_div').outerHeight(true);
	if(divheight>38){
		$(".easyui-layout").layout('panel','north').panel("resize",{height: divheight+50});
		$(".easyui-layout").layout("resize");
	}
}
function initSearchBoxHeightR(){
	var divheight = $('#search_div').outerHeight(true);
	if(divheight>38){
		$(".easyui-layout").layout('panel','north').panel("resize",{height: divheight+15});
		$(".easyui-layout").layout("resize");
	}
}

/******************框架层方法**********************************************************/


/******************表单方法**********************************************************/
/**
 * 清空列表快速查询form中的内容
 */
function list_form_reset(){
	$('#search_div input').val('');
	$('#search_div select').val('');
	$("#search_div input[type=checkbox]").prop('checked',false);
	$("#search_div input[type=radio]").prop('checked',false);
	
}

/**
 * 列表快速查询功能
 */
function list_simple_query(){
	document.getElementById('select_form').submit();
}

/**
 * 准备查询条件
 * @param pagenum
 * @returns
 */
function getQueryTerms(pagenum){

	//添加分页页号
	if(pagenum!=null && pagenum!=''){ 
		$("#pagenum_id").val(pagenum); 
	}else{
		$("#pagenum_id").val(1); 
	}

	//便利条件Table
	var QueryTermsJson = [];
	var trList = $("#QueryTable tbody").children("tr")
	for (var i=0;i<trList.length;i++) {
		var row = {};
		var tdArr = trList.eq(i).find("td");

		var ComparisonColumnIdValue = tdArr.find("[name='ComparisonColumnId']").val();
		row.ComparisonColumnId = ComparisonColumnIdValue;

		var ComparisonTypeValue = tdArr.find("[name='ComparisonType']").val();
		row.ComparisonType = ComparisonTypeValue;

		var ComparisonValueObj = tdArr.find("[name='ComparisonValue']");
		var ComparisonValueValue = '';
		if(ComparisonValueObj[0].type == 'radio'){
			ComparisonValueValue = tdArr.find("[name='ComparisonValue']:checked").val();
		}else{
			ComparisonValueValue = ComparisonValueObj.val();
		}
		row.ComparisonValue = ComparisonValueValue;

		var ConnectorTypeValue = tdArr.find("[name='ConnectorType']").val();
		row.ConnectorType = ConnectorTypeValue;

		QueryTermsJson.push(row);
	}
	var QueryTermsJsonString = JSON.stringify(QueryTermsJson);

	//设置其它条件
	$("#query_terms_id").val(QueryTermsJsonString);
}

/**
 * 查询方法
 * @param page_num
 */
function form_query(pagenum){ 

	//获取查询条件
	getQueryTerms(pagenum);

	//保存新的查询条件
	FormSerialize();

	//查询
	document.getElementById('select_form').submit();
}

/**
 * 打开查询窗口
 * @param custon_select_action 自定义查询处理Action
 * @returns
 */
function openSelectWindows(){
	$("#select_windows").window("open");
}

/**
 * 查询方法
 * @param page_num
 */
function Z5Query(){ 
	//获取综合查询条件，赋值给Form中的变量
	var Z5QueryParameters = $('#Z5QueryParametersInput').val();
	$('#Z5QueryParameters').val(Z5QueryParameters);

	//查询
	document.getElementById('select_form').submit();
}
/**
 * Z5页面查询框回事事件
 * @returns
 */
function Z5QueryOnkeydown(){ 
	if(event.keyCode==13) {
		Z5Query();
	}
}


/**
 * 清空Form方法
 */
function form_clear(){
	$('#QueryTable tbody').empty();//清空当前条件
	$("#orderby_id").val('');//清空当前排序字段
	$("#orderby_pattern_id").val('');//清空当前排序模式
}

/**
 * 刷新列表
 */
function reload(){
	window.location.reload();
}


/**
 * 刷新列表
 * @returns
 */
function RefreshList(){

	var pagenum = $("#pagenum_id").val();

	//获取查询条件
	getQueryTerms(pagenum);

	//保存新的查询条件
	FormSerialize();

	document.getElementById('select_form').submit();
}

/**
 * 列表排序
 * @param columnId
 * @returns
 */
function SortList(columnId){
	//获取原排序字段与排序模式
	var old_orderby_id = $("#orderby_id").val();
	var old_orderby_pattern_id = $("#orderby_pattern_id").val();

	//点相同字段
	if(columnId==old_orderby_id){
		if(old_orderby_pattern_id=='desc' || old_orderby_pattern_id==''){
			$("#orderby_pattern_id").val('asc');
		}else{
			$("#orderby_pattern_id").val('desc');
		}
	}else{//新的排序字段
		$("#orderby_id").val(columnId);
		$("#orderby_pattern_id").val('asc');
	}

	var pagenum = $("#pagenum_id").val();

	//获取查询条件
	getQueryTerms(pagenum);

	//保存新的查询条件
	FormSerialize();

	document.getElementById('select_form').submit();
}

/**
 * 刷新编辑页面
 * @returns
 */
function RefreshEdit(){
	var tableId = $("#tableId_id").val();
	var zid = $("#zid_id").val();
	if(isNotNull(tableId) && isNotNull(zid)){
		window.location.href='edit?zid='+zid+'&tableId='+tableId;
	}else{
		alertMessager('编辑页面参数为空，刷新失败');
	}

}

/**
 * 导出Excel
 * @returns
 */
function ExportExcel(){
	$.messager.confirm('信息提示','您确定要导出Excel吗？',function(r){
		if (r){
			//获取页面类型
			var PageType = $("#PageType").val();

			//修改FormAction
			$("#select_form").attr('action',"export_excel");

			//提交form
			document.getElementById('select_form').submit();

			//改回原formAction
			$("#select_form").attr('action',PageType);
		}
	});

}

/**
 * 设置每页显示行数
 * @param rowcount
 * @returns
 */
function setRowCount(rowcount){
	if(isNotNull(rowcount)){
		$("#rowcount_id").val(rowcount);
		$("#pagenum_id").val("1");
		RefreshList();
	}
}

/**
 * 根据页号，跳转到指定分页
 * @returns
 */
function JumpRow(){
	var jump_rownum = $("#JumpRownumId").val();
	if(isNotNull(jump_rownum)){
		$("#pagenum_id").val(jump_rownum);
		RefreshList();
	}
}

/**
 * 新增页面打开
 * @returns
 */
function add(){
	var tableId = $("#tableId_id").val();
	window.location.href='add?tableId='+tableId;
}

/**
 * 修改页面打开
 * @returns
 */
function edit(){
	var zids = getTableColumn('MainTable','zid');
	var zids_array = zids.split(",");
	if(zids_array.length==1 && zids_array[0]!='' && zids_array[0]!=null){
		var tableId = $("#tableId_id").val();
		window.location.href='edit?zid='+zids_array[0]+'&tableId='+tableId;
	}else{
		alertMessager('请选择一条记录修改');
	}
}

/**
 * 查看页面打开
 * @returns
 */
function look(){
	var zids = getTableColumn('MainTable','zid');
	var zids_array = zids.split(",");
	if(zids_array.length==1 && zids_array[0]!='' && zids_array[0]!=null){
		var tableId = $("#tableId_id").val();
		window.location.href='look?zid='+zids_array[0]+'&tableId='+tableId;
	}else{
		alertMessager('请选择一条记录查看');
	}
}

/**
 * 卡片组进入编辑页面
 * @param obj
 * @returns
 */
function list_card_edit(obj){
	var zid = obj.value;
	if(isNotNull(zid)){
		var tableId = $("#tableId_id").val();
		window.location.href='edit?zid='+zid+'&tableId='+tableId;
	}else{
		alertMessager('请选择一条记录修改');
	}
}
/**
 * 卡片组删除方法
 * @param obj
 * @returns
 */
function list_card_delete(obj){
	var tableId = $("#tableId_id").val();
	var zid = obj.value;
	if(isNull(zid)){
		alertMessager('请选择要删除的记录');
	}else{
		$.messager.confirm('信息提示','您确定要删除该记录吗？',function(r){
			if (r){
				openLoading();
				
				$.ajax({
					type : "get",
					url : 'delete',
					data:{zids:zid,tableId:tableId},
					success : function(data) {
						if(data.code=='SUCCESS'){
							alertMessager('成功删除记录');
							RefreshList();
						}else{
							alertErrorMessager(''+data.msg);
						}
					},
					error: function (data) {
						alertErrorMessager('ajax错误：'+JSON.stringify(data));
					},
					complete: function () {
						//执行完成
						closedLoading();
					}
				});

			}
		});
	}
}


/**
 * 卡片组保存方法
 * @param obj
 * @returns
 */
function list_card_save(obj){
	var zid = obj.value;
	if(isNotNull(zid)){
		var tableId = $("#tableId_id").val();
		var form = new FormData($("#"+zid+"_form")[0]);
		form.append("tableId", tableId);
		form.append("zid", zid);
		openLoading();
		$.ajax({
			type : "get",
			url : "update",
			data:form,
			async: false,//同步请求
			processData: false,//是否序列化 data
			contentType: false,
			success : function(data) {
				//执行完成
				closedLoading();
				if(data.code=='SUCCESS'){
					alertMessager('保存成功');
				}else{
					alertErrorMessager(''+data.msg);
				}
			},
			error: function (data) {
				//执行完成
				closedLoading();
				alertErrorMessager('ajax错误：'+JSON.stringify(data));
			}
		});
	}
}

/**
 * 双击行，进入编辑状态
 * @param index
 * @param field
 * @param value
 * @returns
 */
function DblClickList(index,field,value){
	var list_dblclick = $("#list_dblclick_id").val();
	if(list_dblclick=='1'){
		edit();//编辑
	}else{
		look();//查看
	}
	
}
/**
 * 双击行[明细表]，进入编辑状态
 * @param index
 * @param field
 * @param value
 * @returns
 */
function DblClickDetailList(index,field,value){
	var tableId = $('#DetailTableTab').tabs('getSelected').panel('options').id;
	var list_dblclick = $("#"+tableId+"_list_dblclick_id").val();
	if(list_dblclick=='1'){
		updateDetail();//编辑
	}else{
		lookDetail();//查看
	}
}




/**
 * 保存
 * @returns
 */
function SaveForm(custom_url){
	$("#ButtonHR").hide();//隐藏按钮行
	var editType = $('#PageType').val();
	if(editType == 'add'){
		insert(custom_url);
	}else if(editType == 'edit'){
		update(custom_url);
	}else{
		alertErrorMessager('PageType参数为空');
	}
	$("#ButtonHR").show();//显示按钮行
}

/**
 * 保存并新增
 * @returns
 */
function SaveAndAddForm(custom_url){
	$("#ButtonHR").hide();//隐藏按钮行
	var editType = $('#PageType').val();
	if(editType == 'add'){
		insertAndAdd(custom_url);
	}else if(editType == 'edit'){
		updateAndAdd(custom_url);
	}else{
		alertErrorMessager('PageType参数为空');
	}
	$("#ButtonHR").show();//显示按钮行
}

/**
 * 保存关返回
 * @returns
 */
function SaveAndRetrunForm(custom_url){
	$("#ButtonHR").hide();//隐藏按钮行
	var editType = $('#PageType').val();
	if(editType == 'add'){
		insertAndRetrun(custom_url);
	}else if(editType == 'edit'){
		updateAndRetrun(custom_url);
	}else{
		alertErrorMessager('PageType参数为空');
	}
	$("#ButtonHR").show();//显示按钮行
}


/**
 * 保存-插入
 * @returns
 */
function insert(custom_url){
	var form = new FormData($("#main_form")[0]);
	var ajaxurl = 'insert';
	if(!isNull(custom_url)){
		ajaxurl = custom_url;
	}
	$.ajax({
		type : "post",
		url : ajaxurl,
		data:form,
		async: false,//同步请求
		processData: false,//是否序列化 data
		contentType: false,
		beforeSend: function(){
			openLoading();
		},
		success : function(data) {
			if(data.code=='SUCCESS'){
				alertMessager('保存成功');
				var zid = data.data;
				if(zid!='' && zid!=null){
					var tableId = $("#tableId_id").val();
					window.location.href='edit?zid='+zid+'&tableId='+tableId;
				}
			}else{
				alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		},
		complete: function () {
			closedLoading();
		}
	});
}

/**
 * 保存-更新
 * @returns
 */
function update(custom_url){
	var form = new FormData($("#main_form")[0]);
	var ajaxurl = 'update';
	if(!isNull(custom_url)){
		ajaxurl = custom_url;
	}
	$.ajax({
		type : "post",
		url : ajaxurl,
		data:form,
		async: false,//同步请求
		processData: false,//是否序列化 data
		contentType: false,
		beforeSend: function(){
			openLoading();
		},
		success : function(data) {
			if(data.code=='SUCCESS'){
				alertMessager('保存成功');
				var zid = data.data;
				if(zid!='' && zid!=null){
					var tableId = $("#tableId_id").val();
					window.location.href='edit?zid='+zid+'&tableId='+tableId;
				}
			}else{
				alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		},
		complete: function () {
			//执行完成
			closedLoading();
		}
	});
}

/**
 * 新增并新增
 * @returns
 */
function insertAndAdd(custom_url){
	var form = new FormData($("#main_form")[0]);
	var ajaxurl = 'insert';
	if(!isNull(custom_url)){
		ajaxurl = custom_url;
	}
	$.ajax({
		type : "post",
		url : ajaxurl,
		data:form,
		async: false,//同步请求
		processData: false,//是否序列化 data
		contentType: false,
		beforeSend: function(){
			openLoading();
		},
		success : function(data) {
			if(data.code=='SUCCESS'){
				alertMessager('保存成功');
				//var zid = data.data;
				var tableId = $("#tableId_id").val();
				var url = 'add?tableId='+tableId;
				var pid = $("#pid_id").val();
				if(!isNull(pid)){
					url = url+'&pid='+pid;
				}
				window.location.href = url;
			}else{
				alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		},
		complete: function () {
			//执行完成
			closedLoading();
		}
	});
}

/**
 * 更新并新增
 * @returns
 */
function updateAndAdd(custom_url){
	var form = new FormData($("#main_form")[0]);
	var ajaxurl = 'update';
	if(!isNull(custom_url)){
		ajaxurl = custom_url;
	}
	$.ajax({
		type : "post",
		url : ajaxurl,
		data:form,
		async: false,//同步请求
		processData: false,//是否序列化 data
		contentType: false,
		beforeSend: function(){
			openLoading();
		},
		success : function(data) {
			if(data.code=='SUCCESS'){
				alertMessager('保存成功');
				//var zid = data.data;
				var tableId = $("#tableId_id").val();
				var url = 'add?tableId='+tableId;
				var pid = $("#pid_id").val();
				if(!isNull(pid)){
					url = url+'&pid='+pid;
				}
				window.location.href = url;
			}else{
				alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		},
		complete: function () {
			//执行完成
			closedLoading();
		}
	});
}

/**
 * 新增并返回
 * @returns
 */
function insertAndRetrun(custom_url){
	var form = new FormData($("#main_form")[0]);
	var ajaxurl = 'insert';
	if(!isNull(custom_url)){
		ajaxurl = custom_url;
	}
	$.ajax({
		type : "post",
		url : ajaxurl,
		data:form,
		async: false,//同步请求
		processData: false,//是否序列化 data
		contentType: false,
		beforeSend: function(){
			openLoading();
		},
		success : function(data) {
			if(data.code=='SUCCESS'){
				alertMessager('保存成功');
				//var zid = data.data;
				var tableId = $("#tableId_id").val();
				returnParentPage(tableId);
			}else{
				alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		},
		complete: function () {
			//执行完成
			closedLoading();
		}
	});
}

/**
 * 更新并返回
 * @returns
 */
function updateAndRetrun(custom_url){
	var form = new FormData($("#main_form")[0]);
	var ajaxurl = 'update';
	if(!isNull(custom_url)){
		ajaxurl = custom_url;
	}
	$.ajax({
		type : "post",
		url : ajaxurl,
		data:form,
		async: false,//同步请求
		processData: false,//是否序列化 data
		contentType: false,
		beforeSend: function(){
			openLoading();
		},
		success : function(data) {
			if(data.code=='SUCCESS'){
				alertMessager('保存成功');
				var tableId = $("#tableId_id").val();
				returnParentPage(tableId);
			}else{
				alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		},
		complete: function () {
			//执行完成
			closedLoading();
		}
	});
}

function isFunction(funName){
	var result = false;
	try {
		if (typeof(eval("parent.openLoading")) == "function") {
			result = true;
		}
	} catch(e) {
		
	}
	return result;
}

/**
 * 返回上级页面
 * @returns
 */
function returnParentPage(tableId){
	var editType = $('#PageType').val();
	var form = new FormData($("#main_form")[0]);
	if(isNull(tableId)){
		tableId = $('#tableId_id').val();
	}

	$.ajax({
		type : "post",
		url : "getParentTableId",
		data:form,
		async: false,//同步请求
		processData: false,//是否序列化 data
		contentType: false,
		beforeSend: function(){
			openLoading();
		},
		success : function(data) {
			if(data.code=="SUCCESS"){
				//获取上级表ID
				var parentTableId = data.data;
				//获取上级表数据ID
				var pid = $("#pid_id").val();
				if(editType == 'add' || editType == 'edit'){
					window.location.href='edit?zid='+pid+'&tableId='+parentTableId;
				}else{
					window.location.href='look?zid='+pid+'&tableId='+parentTableId;
				}
			}else{
				var url = 'list?tableId='+tableId;
				var str = getCookie(url);
				var strArray = str.split("&");
				if(isNotNull(str)){
					// 创建Form  
					var form = $('<form></form>');  
					// 设置属性  
					form.attr('action', url);  
					form.attr('method', 'get');
					form.attr('assept-charset','utf-8');
					form.attr('enctype','multipart/form-data');

					//添加属性
					for (var i = 0; i < strArray.length; i++) {
						var sd = strArray[i];
						var sdArray = sd.split("=");
						// 创建Input  
						var my_input = $('<input type="hidden" name="'+sdArray[0]+'" />');  
						my_input.attr('value', sdArray[1]);  
						// 附加到Form  
						form.append(my_input);
					}
					form.css('display','none');
					$(document.body).append(form);
					// 提交表单  
					form.submit();
				}else{
					window.location.href='list?tableId='+tableId;
				}
			}
		},
		error: function (data) {
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		},
		complete: function () {
			//执行完成
			closedLoading();
		}
	});
}



/**
 * 提交申请
 * @param tableId
 * @returns
 */
function oa_submit(){
	
	//调用保存
	//SaveForm();
	
	//parent.openLoading();
	var tableId = $('#tableId_id').val();
	var zid = $('#zid_id').val();
	//根据TableId获取流程列表
	$.ajax({
		type : "get",
		url : 'getOaWid',
		data:{tableId:tableId},
		async: true,//同步请求
		success : function(data) {
			closedLoading();
			if(data.code=='SUCCESS'){
				var z_workflowCount = data.data.length;
				if(z_workflowCount<1){
					alertMessager('该功能未设定工作流程');
				}else{
					if(z_workflowCount==1){
						//提交申请
						oasubmit(zid,data.data[0].zid,tableId);
					}else{
						//多条工作流程
						var listhtml = '';
						for(var i=0;i<z_workflowCount;i++){
							listhtml = listhtml + '<button type="button" class="btn btn-light btn-lg btn-block" onclick="oasubmit(\''+zid+'\',\''+data.data[i].zid+'\',\''+tableId+'\');">'+data.data[i].w_name+'</button>';
						}
						$('#oa_wid_windows').html(listhtml);
						$('#oa_wid_windows').window('open');
					}
				}
			}else{
				alertErrorMessager('获取流程列表失败：'+data.msg);
			}
		},
		error: function (data) {
			closedLoading();
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		}
	});



}

/**
 * 提交申请
 * @returns
 */
function oasubmit(zid,wid,tableId){
	//提交申请   开始=======================================
	if(isNotNull(zid) && isNotNull(wid) && isNotNull(tableId)){
		$.ajax({
			type : "get",
			url : 'oa_submit',
			data:{zid:zid,wid:wid,tableId:tableId},
			async: false,//同步请求
			success : function(data) {
				if(data.code=='SUCCESS'){
					alertMessager('提交成功');
					//返回列表页面
					returnParentPage(tableId);
				}else{
					alertErrorMessager('提交失败：'+data.msg);
				}
			},
			error: function (data) {
				alertErrorMessager('ajax错误：'+JSON.stringify(data));
			}
		});
	}else{
		alertErrorMessager('zid,wid,tableId 不能为空，无法提法流程');
	}

	//提交申请   结束=======================================
}




/**
 * 打印列表
 * @returns
 */
function printList(custom_url){
	if(isNull(custom_url)){
		custom_url = "print_list";
	}
	var tableId = $('#tableId_id').val();
	var zids = getTableColumn('MainTable','zid');
	if(zids==''){
		alertMessager('请选择要打印的记录');
	}else{
		$.messager.confirm('信息提示','您确定要打印这些记录吗？',function(r){
			if (r){
				openLoading();
				$.ajax({
					type : "get",
					url : custom_url,
					data:{zids:zids,tableId:tableId},
					success : function(data) {
						if(data.code=='SUCCESS' && isNotNull(data.data)){
							$("#select_form").print({
					        	globalStyles: true,
					        	mediaPrint: false,
					        	stylesheet: null,
					        	noPrintSelector: ".no-print",
					        	iframe: true,
					        	append: data.data,
					        	prepend: "",
					        	manuallyCopyFormValues: true,
					        	deferred: $.Deferred(),
					        	timeout: 750,
					        	title: null,
					        	doctype: '<!doctype html>'
							});
						}else{
							alertErrorMessager(''+data.msg);
						}
					},
					error: function (data) {
						alertErrorMessager('ajax错误：'+JSON.stringify(data));
					},
					complete: function () {
						//执行完成
						closedLoading();
					}
				});

			}
		});
	}
}

/**
 * 打印表单
 * @returns
 */
function printForm(){
	$.messager.confirm('信息提示','您确定打印表单吗？',function(r){
		if (r){
			var dHtml = "";
			var tabs = $('#DetailTableTab').tabs('tabs');
			for(var i=0;i<tabs.length;i++) {
				//获取子表
	            var dTableId = $('#DetailTableTab').tabs('getTab',i).panel('options').id;
	            if(isNotNull(dTableId)){
	            	//获取子表名称
	            	var dTableIdTitle = $('#DetailTableTab').tabs('getTab',i).panel('options').title;
	            	dHtml = dHtml+"<h2>"+dTableIdTitle+"</h2><table class='table table-bordered'><thead>";
	            	dHtml = dHtml+ "<tr>";
	            	//获取列头
	            	var heads = $("#"+dTableId+"_detail_table").datagrid('getColumnFields', false);
	            	var headArray = new Array();
	            	for (var heads_i in heads) {     
	                    //获取每一列的列名对象
	                    var col = $("#"+dTableId+"_detail_table").datagrid("getColumnOption", heads[heads_i]);
	                    //获取文本信息
	                    dHtml = dHtml+ "<th scope='col'>"+col.title.trim()+"</th>";
	                    headArray.push(col.field);
	                }   
	            	dHtml = dHtml+ "</tr>";
	            	dHtml = dHtml+ "</thead>";
	            	dHtml = dHtml+ "<tbody>";
	            	//获取所有数据行
	            	var rows = $('#'+dTableId+"_detail_table").datagrid('getRows');
	            	for (var rows_i = 0; rows_i < rows.length; rows_i++) {
	            		dHtml = dHtml+ "<tr>";
	            		//遍历所在列
	            		for(var head_i in headArray){
	            			dHtml = dHtml+ "<td>"+rows[rows_i][headArray[head_i]] + "</td>";
	            		}
	            		dHtml = dHtml+ "</tr>";
	            	}
	            	dHtml = dHtml+ "</tbody></table>";
	            }
			}
			
			dHtml = dHtml+ "<hr>";
			
			$("#main_form").print({
	        	globalStyles: true,
	        	mediaPrint: false,
	        	stylesheet: null,
	        	noPrintSelector: ".no-print",
	        	iframe: true,
	        	append: dHtml,
	        	prepend: "<h6>主键："+$('#zid_id').val()+"</h6>",
	        	manuallyCopyFormValues: true,
	        	deferred: $.Deferred(),
	        	timeout: 750,
	        	title: null,
	        	doctype: '<!doctype html>'
			});
			
		}
	});
	
}

/**
 * 删除数据方法
 * @param formId
 * @returns
 */
function remove(tableId){
	if(isNull(tableId)){
		tableId = $('#tableId_id').val();
	}

	var zids = getTableColumn('MainTable','zid');
	if(zids==''){
		alertMessager('请选择要删除的记录');
	}else{
		$.messager.confirm('信息提示','您确定要删除这些记录吗？',function(r){
			if (r){
				openLoading();
				$.ajax({
					type : "get",
					url : 'delete',
					data:{zids:zids,tableId:tableId},
					success : function(data) {
						if(data.code=='SUCCESS'){
							alertMessager('成功删除'+data.data+'条记录');
							RefreshList();
						}else{
							alertErrorMessager(''+data.msg);
						}
					},
					error: function (data) {
						alertErrorMessager('ajax错误：'+JSON.stringify(data));
					},
					complete: function () {
						//执行完成
						closedLoading();
					}
				});

			}
		});
	}


}

/**
 * 新增明细记录
 * @returns
 */
function addDetail(info){
	//获取当前激活Tab表编号
	var tableId = $('#DetailTableTab').tabs('getSelected').panel('options').id;
	var zid = $('#zid_id').val();
	if(!isNull(tableId) && !isNull(zid) ){
		window.location.href='addDetail?tableId='+tableId+'&pid='+zid+'&info='+info;
	}else{
		alertErrorMessager('Now tab TableId is null');
	}
}

/**
 * 修改明细记录
 * @returns
 */
function updateDetail(){
	//获取当前激活Tab表编号
	var tableId = $('#DetailTableTab').tabs('getSelected').panel('options').id;
	var zids = getTableColumn(tableId+'_detail_table','zid');
	var zids_array = zids.split(",");
	if(zids_array.length==1 && zids_array[0]!='' && zids_array[0]!=null){
		//主表ID
		var pid = $('#zid_id').val();
		window.location.href='edit?zid='+zids_array[0]+'&tableId='+tableId+'&pid='+pid;
	}else{
		alertMessager('请选择一条记录修改');
	}
}
/**
 * 查看明细记录
 * @returns
 */
function lookDetail(){
	//获取当前激活Tab表编号
	var tableId = $('#DetailTableTab').tabs('getSelected').panel('options').id;
	var zids = getTableColumn(tableId+'_detail_table','zid');
	var zids_array = zids.split(",");
	if(zids_array.length==1 && zids_array[0]!='' && zids_array[0]!=null){
		//主表ID
		var pid = $('#zid_id').val();
		window.location.href='look?zid='+zids_array[0]+'&tableId='+tableId+'&pid='+pid;
	}else{
		alertMessager('请选择一条记录修改');
	}
}
/**
 * 删除明细记录
 * @returns
 */
function removeDetail(){
	//获取当前激活Tab表编号
	var tableId = $('#DetailTableTab').tabs('getSelected').panel('options').id;
	var zids = getTableColumn(tableId+'_detail_table','zid');
	if(zids==''){
		alertMessager('请选择要删除的记录');
	}else{
		$.messager.confirm('信息提示','您确定要删除这些记录吗？',function(r){
			if (r){
				openLoading();
				$.ajax({
					type : "get",
					url : 'delete',
					data:{zids:zids,tableId:tableId},
					success : function(data) {
						if(data.code=='SUCCESS'){
							alertMessager('成功删除'+data.data+'条记录');
							var zid = $('#zid_id').val();
							var cTableId = $('#tableId_id').val();
							window.location.href='edit?zid='+zid+'&tableId='+cTableId;
						}else{
							alertErrorMessager(''+data.msg);
						}
					},
					error: function (data) {
						alertErrorMessager('ajax错误：'+JSON.stringify(data));
					},
					complete: function () {
						//执行完成
						closedLoading();
					}
				});

			}
		});
	}
}

/**
 * 打开Z5列表页面
 * @param TableColunmId 表名.字段名
 * isReport 是否报表页面调用 1为报表页面
 * @returns
 */
function Z5list(TableColunmId,ReturnKeyColumnId,ReturnValueColumnId,isReport){
	var width = 1000;
	var height = 600;
	var iTop = (window.screen.availHeight-30-height)/2;//获得窗口的垂直位置;
	var iLeft = (window.screen.availWidth-10-width)/2;//获得窗口的水平位置;
	var url = 'Z5list?TableColunmId='+TableColunmId+'&ReturnKeyColumnId='+ReturnKeyColumnId+'&ReturnValueColumnId='+ReturnValueColumnId+"&isReport="+isReport;
	window.open(url, '文件上传', 'width='+width+',height='+height+',location=no,menubar=no,status=no,toolbar=no, top='+iTop+', left='+iLeft);

}

/**
 * 清空Z5控件
 * @param TableColunmId
 * @param ReturnKeyColumnId
 * @param ReturnValueColumnId
 * @returns
 */
function Z5Clear(TableColunmId,ReturnKeyColumnId,ReturnValueColumnId){
	$("#"+ReturnKeyColumnId).val('');
	$("#"+ReturnValueColumnId).val('');
}

/**
 * Z5控件返回刷新方法
 * @param txtId
 * @param returnvalue
 */
function Z5listReturn(columnId,ReturnValue,DisplayValue,ReturnKeyColumnId,ReturnValueColumnId){
	if (typeof(ReturnValue) == "undefined" || typeof(DisplayValue) == "undefined") { 
		$("#"+ReturnKeyColumnId).val('');
		$("#"+ReturnValueColumnId).val('');
	}else{
		//给控件赋值
		$("#"+ReturnKeyColumnId).val(ReturnValue);
		$("#"+ReturnValueColumnId).val(DisplayValue);
	}   
	//调用onchange事件
	$("#"+ReturnValueColumnId).change();
}

/**
 * 打开文件上传页面
 * @param TableColunmId
 * @returns
 */
function uploadFile(ColunmId,zid){
	var width = 400;
	var height = 250;
	var iTop = (window.screen.availHeight-30-height)/2;//获得窗口的垂直位置;
	var iLeft = (window.screen.availWidth-10-width)/2;//获得窗口的水平位置;

	var url = 'openUploadFile?ColunmId='+ColunmId+'&zid='+zid;
	window.open(url, '文件上传', 'width='+width+',height='+height+',location=no,menubar=no,status=no,toolbar=no, top='+iTop+', left='+iLeft);

}

/**
 * 文件上传页面返回方法
 * @param inputId
 * @param returnvalue
 */
function OpenFileUploadReturn(txtId,returnvalue){
	$("#"+txtId+'_id').val(returnvalue);
}

/**
 * 打开页面向父页面传值
 * @param inputId
 * @param returnvalue
 */
function OpenPageReturnValue(txtId,returnvalue){
	$("#"+txtId).val('');
	$("#"+txtId).val(returnvalue);
}

/**
 * 文件下载(根据字段ID)
 * @param TableColunmId 字段ID
 * @returns
 */
function downloadFile(ColunmId){
	var url = $('#'+ColunmId+'_id').val();
	if(!isNull(url)){
		var $form = $('<form method="GET"></form>');
		$form.attr('action', url);
		$form.appendTo($('body'));
		$form.submit();
	}

}

/**
 * 删除服务器中的文件
 * @param TableColunmId 字段ID
 * @returns
 */
function deleteFile(ColunmId){
	var url = $('#'+ColunmId+'_id').val();
	if(!isNull(url)){
		openLoading();
		$.ajax({
			type : "get",
			url : "deleteFile",
		        //是否异步操作 false为同步操作 默认为true异步操作
		        async: false,
		        //参数
			data:{url:url},
			success : function(data) {
				closedLoading();
				$('#'+ColunmId+'_id').val('');
				if(data.code=='SUCCESS'){
					alertMessager(data.msg)
				}else{
					alertErrorMessager(data.msg);
				}
			},
			error: function (data) {
				closedLoading();
				alertErrorMessager("ajax错误："+JSON.stringify(data));
			}
		});
	}
}



/**
 * 图片预览(根据字段ID)
 * @param TableColunmId 字段ID
 * @returns
 */
function lookupImg(ColunmId){
	var url = $('#'+ColunmId+'_id').val();
	parent.alertImgWindows(url);
}

/**
 * 图片预览(根据URL直接查看图片)
 * @param url 图片URL
 * @returns
 */
function lookupImg2(url){
	parent.alertImgWindows(url);
}

/**
 * 上移
 * @returns
 */
function moveUp(){
	var PageType = $('#PageType').val();
	if(PageType=='list'){
		var row = $('#MainTable').datagrid('getSelected');
		if (row){
			var rowIndex = $('#MainTable').datagrid("getRowIndex",row);
			var zid = row.zid;
			var tableId = $("#tableId_id").val();
			var nextRow = $('#MainTable').datagrid('getData').rows[rowIndex-1];  
			var pagenum = $("#pagenum_id").val();
			var rowcount = $("#rowcount_id").val();
			if(!isNull(nextRow)){
				var nextzid = nextRow.zid;
				window.location.href='MoveDownOrMoveUp?zid='+zid+'&tableId='+tableId+'&PageType='+PageType+"&nextzid="+nextzid+"&pagenum="+pagenum+"&rowcount="+rowcount;
			}
		}
	}else if(PageType=='edit'){//编辑页面
		//获取当前激活Tab表编号
		var tableId = $('#DetailTableTab').tabs('getSelected').panel('options').id;
		var row = $('#'+tableId+'_detail_table').datagrid('getSelected');
		if (row){
			var rowIndex = $('#'+tableId+'_detail_table').datagrid("getRowIndex",row);
			var zid = row.zid;
			var nextRow = $('#'+tableId+'_detail_table').datagrid('getData').rows[rowIndex-1];  
			if(!isNull(nextRow)){
				var nextzid = nextRow.zid;
				window.location.href='MoveDownOrMoveUp?zid='+zid+'&tableId='+tableId+'&PageType='+PageType+"&nextzid="+nextzid;
			}
		}
	}else{
		alertErrorMessager('PageType Is Null');
	}
}

/**
 * 下移
 * @returns
 */
function moveDown(){
	var PageType = $('#PageType').val();
	if(PageType=='list'){
		var row = $('#MainTable').datagrid('getSelected');
		if (row){
			var rowIndex = $('#MainTable').datagrid("getRowIndex",row);
			var zid = row.zid;
			var tableId = $("#tableId_id").val();
			var nextRow = $('#MainTable').datagrid('getData').rows[rowIndex+1];  
			var pagenum = $("#pagenum_id").val();
			var rowcount = $("#rowcount_id").val();
			if(!isNull(nextRow)){
				var nextzid = nextRow.zid;
				window.location.href='MoveDownOrMoveUp?zid='+zid+'&tableId='+tableId+'&PageType='+PageType+"&nextzid="+nextzid+"&pagenum="+pagenum+"&rowcount="+rowcount;
			}
		}
	}else if(PageType=='edit'){//编辑页面
		//获取当前激活Tab表编号
		var tableId = $('#DetailTableTab').tabs('getSelected').panel('options').id;
		var row = $('#'+tableId+'_detail_table').datagrid('getSelected');
		if (row){
			var rowIndex = $('#'+tableId+'_detail_table').datagrid("getRowIndex",row);
			var zid = row.zid;
			var nextRow = $('#'+tableId+'_detail_table').datagrid('getData').rows[rowIndex+1];  
			if(!isNull(nextRow)){
				var nextzid = nextRow.zid;
				window.location.href='MoveDownOrMoveUp?zid='+zid+'&tableId='+tableId+'&PageType='+PageType+"&nextzid="+nextzid;
			}
		}
	}else{
		alertErrorMessager('PageType Is Null');
	}
}

/**
 * 默认选中行
 * @param zid
 * @returns
 */
function checkRowForCheckedZid(zid,pageType,TableId,TableTitle){
	if(pageType=='edit'){
		//编辑页面
		$('#DetailTableTab').tabs('select', TableTitle);
	}

	if(!isNull(zid)){
		var rows = $("#"+TableId).datagrid("getRows");
		for (var i = 0; i < rows.length; i++) {  
			if (rows[i]['zid'] == zid) {  
				$("#"+TableId).datagrid("checkRow",i);
				break;  
			}  
		}
	}

}
/**
 * 初始化HTML字段
 * @param ColumnId
 * @returns
 */
function InitHTMLColumn(ColumnId){
	UE.getEditor(ColumnId);
}
/**
 * 初始化源码编辑字段
 * @param ColumnId
 * @returns
 */
function InitCodeColumn(ColumnId,mode_type,height){
	var ColumnEditor = CodeMirror.fromTextArea(document.getElementById(ColumnId),{
		lineNumbers: true,//显示行号
		matchBrackets: true,//括号匹配
		smartIndent:true,//自动缩进是否开启
		theme:'eclipse', //编辑器主题
		mode:mode_type//编辑器语言
	});
	ColumnEditor.on("blur",function(){
		$('#'+ColumnId).val(ColumnEditor.getValue());
	});

	//默认高度
	if(isNull(height)){
		height = '300';
	}
	ColumnEditor.setSize('100%',height+'px');
}

/******************表单方法**********************************************************/


/******************查询域**********************************************************/
function AddQueryRom(){

	var ComparisonColumnIds = $("#ComparisonColumnIdsId").val();

	//获取Table
	var tb1 = $("#QueryTable");  
	var $tr = $("<tr></tr>");// 创建tr

	//添加列名
	var tdColumnId;
	var $td_ComparisonColumnId = $("<td></td>"); //ComparisonColumnId td
	var ComparisonColumnIdHTML = '	<select name="ComparisonColumnId" class="form-control form-control-sm" onchange="ComparisonColumnIdOnChange(this);">';
	var columnid = ComparisonColumnIds.split(",");
	for (var i = 0; i < columnid.length; i++) {
		var col = columnid[i].split("☆");
		ComparisonColumnIdHTML = ComparisonColumnIdHTML + '		<option value="'+col[0]+'">'+col[1]+'</option>';
		//设置列名成员变量，查询内容字段生成控件使用
		if(i==0){
			tdColumnId = col[0];
		}
	}
	ComparisonColumnIdHTML = ComparisonColumnIdHTML + '	</select>';
	$td_ComparisonColumnId.html(ComparisonColumnIdHTML); //html把序号放到了第一个td里面


	//添加比较符
	var $td_ComparisonType = $("<td></td>"); //ComparisonType td
	var ComparisonTypeHTML = '	<select name="ComparisonType" class="form-control form-control-sm">';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="1" selected="selected">等于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="2">包含</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="3">大于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="4">大于等于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="5">小于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="6">小于等于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="7">不等于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '	</select>';
	$td_ComparisonType.html(ComparisonTypeHTML);//html把序号放到了第一个td里面

	//添加查询内容字段[根据查询字段的类型创建 查询内容字段类型]
	var $td_ComparisonValue = $("<td></td>"); //ComparisonValue td
	var ComparisonValueHTML = CreateComparisonValueHTML(tdColumnId,'');
	$td_ComparisonValue.html(ComparisonValueHTML);//html把序号放到了第一个td里面

	//添加连接符
	var $td_ConnectorType = $("<td></td>"); //ComparisonValue td
	var ConnectorTypeHTML = '	<select name="ConnectorType" class="form-control form-control-sm">';
	ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="0" selected="selected">&nbsp;</option>';
	ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="1">并且</option>';
	ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="2">或者</option>';
	ConnectorTypeHTML = ConnectorTypeHTML + '	</select>';
	$td_ConnectorType.html(ConnectorTypeHTML);//html把序号放到了第一个td里面

	//添加删除按键
	var $td_DeleteButton = $('<td><button type="button" class="btn btn-light" onclick="DeleteQueryRom(this)"><i class="fa fa-trash"></i></button></td>');

	//添加TD到TR
	$tr.append($td_ComparisonColumnId);
	$tr.append($td_ComparisonType);
	$tr.append($td_ComparisonValue);
	$tr.append($td_ConnectorType);
	$tr.append($td_DeleteButton);

	//添加TR到TABLE
	tb1.append($tr);

}
function AddQueryRomR(){

	var ComparisonColumnIds = $("#ComparisonColumnIdsId").val();

	//获取Table
	var tb1 = $("#QueryTable");  
	var $tr = $("<tr></tr>");// 创建tr

	//添加列名
	var tdColumnId;
	var $td_ComparisonColumnId = $("<td></td>"); //ComparisonColumnId td
	var ComparisonColumnIdHTML = '	<select name="ComparisonColumnId" class="form-control form-control-sm" onchange="ComparisonColumnIdOnChangeR(this);">';
	var columnid = ComparisonColumnIds.split(",");
	for (var i = 0; i < columnid.length; i++) {
		var col = columnid[i].split("☆");
		ComparisonColumnIdHTML = ComparisonColumnIdHTML + '		<option value="'+col[0]+'">'+col[1]+'</option>';
		//设置列名成员变量，查询内容字段生成控件使用
		if(i==0){
			tdColumnId = col[0];
		}
	}
	ComparisonColumnIdHTML = ComparisonColumnIdHTML + '	</select>';
	$td_ComparisonColumnId.html(ComparisonColumnIdHTML); //html把序号放到了第一个td里面


	//添加比较符
	var $td_ComparisonType = $("<td></td>"); //ComparisonType td
	var ComparisonTypeHTML = '	<select name="ComparisonType" class="form-control form-control-sm">';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="1" selected="selected">等于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="2">包含</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="3">大于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="4">大于等于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="5">小于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="6">小于等于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="7">不等于</option>';
	ComparisonTypeHTML = ComparisonTypeHTML + '	</select>';
	$td_ComparisonType.html(ComparisonTypeHTML);//html把序号放到了第一个td里面

	//添加查询内容字段[根据查询字段的类型创建 查询内容字段类型]
	var $td_ComparisonValue = $("<td></td>"); //ComparisonValue td
	var ComparisonValueHTML = CreateComparisonValueHTMLR(tdColumnId,'');
	$td_ComparisonValue.html(ComparisonValueHTML);//html把序号放到了第一个td里面

	//添加连接符
	var $td_ConnectorType = $("<td></td>"); //ComparisonValue td
	var ConnectorTypeHTML = '	<select name="ConnectorType" class="form-control form-control-sm">';
	ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="0" selected="selected">&nbsp;</option>';
	ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="1">并且</option>';
	ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="2">或者</option>';
	ConnectorTypeHTML = ConnectorTypeHTML + '	</select>';
	$td_ConnectorType.html(ConnectorTypeHTML);//html把序号放到了第一个td里面

	//添加删除按键
	var $td_DeleteButton = $('<td><button type="button" class="btn btn-light" onclick="DeleteQueryRom(this)"><i class="fa fa-trash"></i></button></td>');

	//添加TD到TR
	$tr.append($td_ComparisonColumnId);
	$tr.append($td_ComparisonType);
	$tr.append($td_ComparisonValue);
	$tr.append($td_ConnectorType);
	$tr.append($td_DeleteButton);

	//添加TR到TABLE
	tb1.append($tr);

}
/**
 * 根据查询字段的类型创建 查询内容字段类型
 * @param tdColumnId 查询列字段ID
 * @returns
 */
function CreateComparisonValueHTML(tdColumnId,tdColumnValue){
	var ComparisonValueHTML = '';

	//获取所有字段JSON对象
	var ComparisonColumnIdsJson = JSON.parse($('#ComparisonColumnIdsJsonId').val());
	var tableId = $('#tableId_id').val();
	var column_type = ComparisonColumnIdsJson[tdColumnId].column_type;
	var p_code_id = ComparisonColumnIdsJson[tdColumnId].p_code_id;
	var z5_table = ComparisonColumnIdsJson[tdColumnId].z5_table;
	var z5_key = ComparisonColumnIdsJson[tdColumnId].z5_key;
	var z5_value = ComparisonColumnIdsJson[tdColumnId].z5_value;

	if(column_type=='0'){//文本
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='1'){//多行文本
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='2'){//数字
		ComparisonValueHTML = '<input name="ComparisonValue" type="number" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='3'){//文件
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='4'){//图片
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='5'){//多选
		var trNum = newid();
		ComparisonValueHTML = '<input name="ComparisonValue" id="'+tdColumnId+trNum+'_key" type="hidden" class="form-control form-control-sm" value=""/>';
		if(!isNull(p_code_id)){//如果不等于空
			var p_code = getCode(p_code_id);
			if(!isNull(p_code)){
				for (var i = 0; i < p_code.z_code_detail_list.length; i++) {
					ComparisonValueHTML = ComparisonValueHTML+'<div class="form-check float_left">';
					if(tdColumnValue==p_code.z_code_detail_list[i].z_key){
						ComparisonValueHTML = ComparisonValueHTML+ '<input name="ComparisonValue_'+trNum+'_name" id="'+tdColumnId+trNum+p_code.z_code_detail_list[i].z_key+'_value" onchange="getCheckedValue(\''+tdColumnId+trNum+'_key\',\'ComparisonValue_'+trNum+'_name\')" type="checkbox" value="'+p_code.z_code_detail_list[i].z_key+'" checked="checked">';
					}else{
						ComparisonValueHTML = ComparisonValueHTML+ '<input name="ComparisonValue_'+trNum+'_name" id="'+tdColumnId+trNum+p_code.z_code_detail_list[i].z_key+'_value" onchange="getCheckedValue(\''+tdColumnId+trNum+'_key\',\'ComparisonValue_'+trNum+'_name\')" type="checkbox" value="'+p_code.z_code_detail_list[i].z_key+'">';
					}
					ComparisonValueHTML = ComparisonValueHTML+'<label class="form-check-label" for="'+tdColumnId+trNum+p_code.z_code_detail_list[i].z_key+'_value">'+p_code.z_code_detail_list[i].z_value+'</label>';
					ComparisonValueHTML = ComparisonValueHTML+'</div>';
				}
			}else{
				ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
			}
		}else{
			ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
		}
	}else if(column_type=='6'){//单选
		if(!isNull(p_code_id)){//如果不等于空
			var p_code = getCode(p_code_id);
			if(!isNull(p_code)){
				for (var i = 0; i < p_code.z_code_detail_list.length; i++) {
					if(tdColumnValue==p_code.z_code_detail_list[i].z_key){
						ComparisonValueHTML = ComparisonValueHTML + '<input name="ComparisonValue" type="radio" value="'+p_code.z_code_detail_list[i].z_key+'" checked="checked">'+p_code.z_code_detail_list[i].z_value+' ';
					}else{
						ComparisonValueHTML = ComparisonValueHTML + '<input name="ComparisonValue" type="radio" value="'+p_code.z_code_detail_list[i].z_key+'">'+p_code.z_code_detail_list[i].z_value+' ';
					}
				}
			}else{
				ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
			}
		}else{ 
			ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
		}
	}else if(column_type=='7'){//下拉框
		if(!isNull(p_code_id)){//如果不等于空
			var p_code = getCode(p_code_id);
			if(!isNull(p_code)){
				ComparisonValueHTML = '<select name="ComparisonValue" class="form-control form-control-sm">';
				for (var i = 0; i < p_code.z_code_detail_list.length; i++) {
					if(tdColumnValue==p_code.z_code_detail_list[i].z_key){
						ComparisonValueHTML = ComparisonValueHTML + '<option value="'+p_code.z_code_detail_list[i].z_key+'" selected = "selected">'+p_code.z_code_detail_list[i].z_value+'</option>';
					}else{
						ComparisonValueHTML = ComparisonValueHTML + '<option value="'+p_code.z_code_detail_list[i].z_key+'">'+p_code.z_code_detail_list[i].z_value+'</option>';
					}
				}
				ComparisonValueHTML = ComparisonValueHTML + '</select>';
			}else{
				ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
			}
		}else{
			ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
		}
	}else if(column_type=='8'){//Z5
		var trNum = newid();
		ComparisonValueHTML = '<div class="input-group input-group-sm">';
		ComparisonValueHTML = ComparisonValueHTML + '	<input id="'+tdColumnId + trNum+'_key" name="ComparisonValue" type="hidden"  value="'+tdColumnValue+'"/>';
		var z5DesplayValue = GetZ5DesplayValue(tableId,tdColumnId,tdColumnValue);
		ComparisonValueHTML = ComparisonValueHTML + '	<input readonly="true" id="'+tdColumnId + trNum+'_display" onclick="Z5list(\''+tableId+'_'+tdColumnId+'\',\''+tdColumnId + trNum+'_key\',\''+tdColumnId + trNum+'_display\',0)" value="'+z5DesplayValue+'" type="text" class="form-control form-control-sm" />';
		ComparisonValueHTML = ComparisonValueHTML + '	<span class="input-group-append">';
		ComparisonValueHTML = ComparisonValueHTML + '		<button class="btn btn-light" onclick="Z5list(\''+tableId+'_'+tdColumnId+'\',\''+tdColumnId + trNum+'_key\',\''+tdColumnId + trNum+'_display\',0)" type="button"><i class="fa fa-search"></i></button>';
		ComparisonValueHTML = ComparisonValueHTML + '	</span>';
		ComparisonValueHTML = ComparisonValueHTML + '</div>';
	}else if(column_type=='9'){//日期
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="Wdate form-control form-control-sm" value="'+tdColumnValue+'" onFocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:\'yyyy-MM-dd\'})"/>';
	}else if(column_type=='10'){//日期时间
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="Wdate form-control form-control-sm" value="'+tdColumnValue+'" onFocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:\'yyyy-MM-dd HH:mm:ss\'})" />';
	}else{
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}

	return ComparisonValueHTML;
}


/**
 * 根据查询字段的类型创建 查询内容字段类型【报表】
 * @param tdColumnId 查询列字段ID
 * @returns
 */
function CreateComparisonValueHTMLR(tdColumnId,tdColumnValue){
	var ComparisonValueHTML = '';

	//获取所有字段JSON对象
	var ComparisonColumnIdsJson = JSON.parse($('#ComparisonColumnIdsJsonId').val());
	var tableId = $('#tableId_id').val();
	var column_type = ComparisonColumnIdsJson[tdColumnId].column_type;
	var p_code_id = ComparisonColumnIdsJson[tdColumnId].p_code_id;
	var z5_table = ComparisonColumnIdsJson[tdColumnId].z5_table;
	var z5_key = ComparisonColumnIdsJson[tdColumnId].z5_key;
	var z5_value = ComparisonColumnIdsJson[tdColumnId].z5_value;

	if(column_type=='0'){//文本
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='1'){//多行文本
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='2'){//数字
		ComparisonValueHTML = '<input name="ComparisonValue" type="number" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='3'){//文件
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='4'){//图片
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}else if(column_type=='5'){//多选
		var trNum = newid();
		ComparisonValueHTML = '<input name="ComparisonValue" id="'+tdColumnId+trNum+'_key" type="hidden" class="form-control form-control-sm" value=""/>';
		if(!isNull(p_code_id)){//如果不等于空
			var p_code = getCode(p_code_id);
			if(!isNull(p_code)){
				for (var i = 0; i < p_code.z_code_detail_list.length; i++) {
					ComparisonValueHTML = ComparisonValueHTML+'<div class="form-check float_left">';
					if(tdColumnValue==p_code.z_code_detail_list[i].z_key){
						ComparisonValueHTML = ComparisonValueHTML+ '<input name="ComparisonValue_'+trNum+'_name" id="'+tdColumnId+trNum+p_code.z_code_detail_list[i].z_key+'_value" onchange="getCheckedValue(\''+tdColumnId+trNum+'_key\',\'ComparisonValue_'+trNum+'_name\')" type="checkbox" value="'+p_code.z_code_detail_list[i].z_key+'" checked="checked">';
					}else{
						ComparisonValueHTML = ComparisonValueHTML+ '<input name="ComparisonValue_'+trNum+'_name" id="'+tdColumnId+trNum+p_code.z_code_detail_list[i].z_key+'_value" onchange="getCheckedValue(\''+tdColumnId+trNum+'_key\',\'ComparisonValue_'+trNum+'_name\')" type="checkbox" value="'+p_code.z_code_detail_list[i].z_key+'">';
					}
					ComparisonValueHTML = ComparisonValueHTML+'<label class="form-check-label" for="'+tdColumnId+trNum+p_code.z_code_detail_list[i].z_key+'_value">'+p_code.z_code_detail_list[i].z_value+'</label>';
					ComparisonValueHTML = ComparisonValueHTML+'</div>';
				}
			}else{
				ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
			}
		}else{
			ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
		}
	}else if(column_type=='6'){//单选
		if(!isNull(p_code_id)){//如果不等于空
			var p_code = getCode(p_code_id);
			if(!isNull(p_code)){
				for (var i = 0; i < p_code.z_code_detail_list.length; i++) {
					if(tdColumnValue==p_code.z_code_detail_list[i].z_key){
						ComparisonValueHTML = ComparisonValueHTML + '<input name="ComparisonValue" type="radio" value="'+p_code.z_code_detail_list[i].z_key+'" checked="checked">'+p_code.z_code_detail_list[i].z_value+' ';
					}else{
						ComparisonValueHTML = ComparisonValueHTML + '<input name="ComparisonValue" type="radio" value="'+p_code.z_code_detail_list[i].z_key+'">'+p_code.z_code_detail_list[i].z_value+' ';
					}
				}
			}else{
				ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
			}
		}else{
			ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
		}
	}else if(column_type=='7'){//下拉框
		if(!isNull(p_code_id)){//如果不等于空
			var p_code = getCode(p_code_id);
			if(!isNull(p_code)){
				ComparisonValueHTML = '<select name="ComparisonValue" class="form-control form-control-sm">';
				for (var i = 0; i < p_code.z_code_detail_list.length; i++) {
					if(tdColumnValue==p_code.z_code_detail_list[i].z_key){
						ComparisonValueHTML = ComparisonValueHTML + '<option value="'+p_code.z_code_detail_list[i].z_key+'" selected = "selected">'+p_code.z_code_detail_list[i].z_value+'</option>';
					}else{
						ComparisonValueHTML = ComparisonValueHTML + '<option value="'+p_code.z_code_detail_list[i].z_key+'">'+p_code.z_code_detail_list[i].z_value+'</option>';
					}
				}
				ComparisonValueHTML = ComparisonValueHTML + '</select>';
			}else{
				ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
			}
		}else{
			ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value=""/>';
		}
	}else if(column_type=='8'){//Z5
		var trNum = newid();
		ComparisonValueHTML = '<div class="input-group input-group-sm">';
		ComparisonValueHTML = ComparisonValueHTML + '	<input id="'+tdColumnId + trNum+'_key" name="ComparisonValue" type="hidden"  value="'+tdColumnValue+'"/>';
		var z5DesplayValue = GetZ5DesplayValueR(tableId,tdColumnId,tdColumnValue);
		ComparisonValueHTML = ComparisonValueHTML + '	<input readonly="true" id="'+tdColumnId + trNum+'_display" onclick="Z5list(\''+tableId+'_'+tdColumnId+'\',\''+tdColumnId + trNum+'_key\',\''+tdColumnId + trNum+'_display\',1)" value="'+z5DesplayValue+'" type="text" class="form-control form-control-sm" />';
		ComparisonValueHTML = ComparisonValueHTML + '	<span class="input-group-append">';
		ComparisonValueHTML = ComparisonValueHTML + '		<button class="btn btn-light" onclick="Z5list(\''+tableId+'_'+tdColumnId+'\',\''+tdColumnId + trNum+'_key\',\''+tdColumnId + trNum+'_display\',1)" type="button"><i class="fa fa-search"></i></button>';
		ComparisonValueHTML = ComparisonValueHTML + '	</span>';
		ComparisonValueHTML = ComparisonValueHTML + '</div>';
	}else if(column_type=='9'){//日期
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="Wdate form-control form-control-sm" value="'+tdColumnValue+'" onFocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:\'yyyy-MM-dd\'})"/>';
	}else if(column_type=='10'){//日期时间
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="Wdate form-control form-control-sm" value="'+tdColumnValue+'" onFocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:\'yyyy-MM-dd HH:mm:ss\'})" />';
	}else{
		ComparisonValueHTML = '<input name="ComparisonValue" type="text" class="form-control form-control-sm" value="'+tdColumnValue+'"/>';
	}

	return ComparisonValueHTML;
}

/**
 * 查询列值改变事件
 * @param selectobj 查询列this
 * @returns
 */
function ComparisonColumnIdOnChange(selectobj){
	//当前选择列的值
	var selectobjindex = selectobj.selectedIndex;

	//获取列ID
	var tdColumnId = selectobj.options[selectobjindex].value

	//根据查询字段的类型创建 查询内容字段类型
	var ComparisonValueHTML = CreateComparisonValueHTML(tdColumnId,'');

	//获取当前TR对象
	var trObj = selectobj.parentNode.parentNode;

	//生成新的控件
	trObj.children[2].innerHTML = ComparisonValueHTML;
}

/**
 * 查询列值改变事件【报表】
 * @param selectobj 查询列this
 * @returns
 */
function ComparisonColumnIdOnChangeR(selectobj){
	//当前选择列的值
	var selectobjindex = selectobj.selectedIndex;

	//获取列ID
	var tdColumnId = selectobj.options[selectobjindex].value

	//根据查询字段的类型创建 查询内容字段类型
	var ComparisonValueHTML = CreateComparisonValueHTMLR(tdColumnId,'');

	//获取当前TR对象
	var trObj = selectobj.parentNode.parentNode;

	//生成新的控件
	trObj.children[2].innerHTML = ComparisonValueHTML;
}

/**
 * 删除行
 * @param temp
 * @returns
 */
function DeleteQueryRom(temp){
	$(temp).parent().parent().remove();
}

/**
 * 保存表单查询条件
 * @returns
 */
function SaveQueryTerms(){
	var tableId = $("#tableId_id").val();
	//获取查询条件
	getQueryTerms('');
	var queryinfo = $("#query_terms_id").val();
	if(isNotNull(tableId)){
		if(isNotNull(queryinfo) && queryinfo!='[]'){

			$.messager.prompt('您确定要保存当前查询条件为常用条件吗？','请输入常用查询保存名称：',function(r){
				if (r){
					openLoading();
					$.ajax({
						type : "get",
						url : 'SaveQueryInfo',
						data:{queryinfo:queryinfo,tableId:tableId,name:r},
						success : function(data) {
							closedLoading();
							if(data.code=='SUCCESS'){
								//刷新列表
								RefreshQueryInfoList();
								//提示信息
								alertMessager('保存成功');
							}else{
								alert_error('保存失败：'+data.msg);
							}
						},
						error: function (data) {
							closedLoading();
							alert_error('保存失败：'+JSON.stringify(data));
						}
					});

				}
			});

		}else{
			alert_error('查询条件不能为空');
		}
	}else{
		alert_error('获取关联表ID不能为空');
	}
}

/**
 * 保存报表查询条件【报表】
 * @returns
 */
function SaveQueryTermsR(){
	var reportid = $("#tableId_id").val();
	//获取查询条件
	getQueryTerms('');
	var queryinfo = $("#query_terms_id").val();
	if(isNotNull(reportid)){
		if(isNotNull(queryinfo) && queryinfo!='[]'){

			$.messager.prompt('您确定要保存当前查询条件为常用条件吗？','请输入常用查询保存名称：',function(r){
				if (r){
					openLoading();
					$.ajax({
						type : "get",
						url : 'SaveQueryInfoR',
						data:{queryinfo:queryinfo,reportid:reportid,name:r},
						success : function(data) {
							closedLoading();
							if(data.code=='SUCCESS'){
								//刷新列表
								RefreshQueryInfoListR();
								//提示信息
								alertMessager('保存成功');
							}else{
								alert_error('保存失败：'+data.msg);
							}
						},
						error: function (data) {
							closedLoading();
							alert_error('保存失败：'+JSON.stringify(data));
						}
					});

				}
			});

		}else{
			alert_error('查询条件不能为空');
		}
	}else{
		alert_error('获取关联表ID不能为空');
	}
}




/**
 * 用户选中常用查询条件
 * @param index
 * @param row
 * @returns
 */
function SelectQueryInfoOnDblClickRow(index,row){
	if(isNotNull(row.queryinfo)){
		
		//清空当前查询条件
		$('#QueryTable tbody').empty();
		
		//获取常用查询条件中的条件
		var queryinfoJSON = JSON.parse(row.queryinfo);
		for(var y=0;y<queryinfoJSON.length;y++){

			var ComparisonColumnIds = $("#ComparisonColumnIdsId").val();

			//获取Table
			var tb1 = $("#QueryTable");  
			var $tr = $("<tr></tr>");// 创建tr

			//添加列名
			var tdColumnId;
			var $td_ComparisonColumnId = $("<td></td>"); //ComparisonColumnId td
			var ComparisonColumnIdHTML = '	<select name="ComparisonColumnId" class="form-control form-control-sm" onchange="ComparisonColumnIdOnChange(this);">';
			var columnid = ComparisonColumnIds.split(",");
			for (var i = 0; i < columnid.length; i++) {
				var col = columnid[i].split("☆");

				if(queryinfoJSON[y].ComparisonColumnId == col[0]){
					ComparisonColumnIdHTML = ComparisonColumnIdHTML + '		<option value="'+col[0]+'" selected = "selected">'+col[1]+'</option>';
				}else{
					ComparisonColumnIdHTML = ComparisonColumnIdHTML + '		<option value="'+col[0]+'">'+col[1]+'</option>';
				}

				//设置列名成员变量，查询内容字段生成控件使用
				if(i==0){
					tdColumnId = col[0];
				}
			}
			ComparisonColumnIdHTML = ComparisonColumnIdHTML + '	</select>';
			$td_ComparisonColumnId.html(ComparisonColumnIdHTML); //html把序号放到了第一个td里面


			//添加比较符
			var $td_ComparisonType = $("<td></td>"); //ComparisonType td
			var ComparisonTypeHTML = '	<select name="ComparisonType" class="form-control form-control-sm">';

			if(queryinfoJSON[y].ComparisonType=='1'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="1" selected="selected">等于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="1">等于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='2'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="2" selected="selected">包含</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="2">包含</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='3'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="3" selected="selected">大于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="3">大于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='4'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="4" selected="selected">大于等于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="4">大于等于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='5'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="5" selected="selected">小于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="5">小于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='6'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="6" selected="selected">小于等于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="6">小于等于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='7'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="7" selected="selected">不等于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="7">不等于</option>';
			}
			ComparisonTypeHTML = ComparisonTypeHTML + '	</select>';
			$td_ComparisonType.html(ComparisonTypeHTML);//html把序号放到了第一个td里面

			//添加查询内容字段[根据查询字段的类型创建 查询内容字段类型]
			var $td_ComparisonValue = $("<td></td>"); //ComparisonValue td
			var ComparisonValueHTML = CreateComparisonValueHTML(queryinfoJSON[y].ComparisonColumnId,queryinfoJSON[y].ComparisonValue);
			$td_ComparisonValue.html(ComparisonValueHTML);//html把序号放到了第一个td里面

			//添加连接符
			var $td_ConnectorType = $("<td></td>"); //ComparisonValue td
			var ConnectorTypeHTML = '	<select name="ConnectorType" class="form-control form-control-sm">';
			if(queryinfoJSON[y].ConnectorType=='0'){
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="0" selected="selected">&nbsp;</option>';
			}else{
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="0">&nbsp;</option>';
			}
			if(queryinfoJSON[y].ConnectorType=='1'){
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="1" selected="selected">并且</option>';
			}else{
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="1">并且</option>';
			}
			if(queryinfoJSON[y].ConnectorType=='2'){
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="2" selected="selected">或者</option>';
			}else{
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="2">或者</option>';
			}
			ConnectorTypeHTML = ConnectorTypeHTML + '	</select>';
			$td_ConnectorType.html(ConnectorTypeHTML);//html把序号放到了第一个td里面

			//添加删除按键
			var $td_DeleteButton = $('<td><button type="button" class="btn btn-light" onclick="DeleteQueryRom(this)"><i class="fa fa-trash"></i></button></td>');

			//添加TD到TR
			$tr.append($td_ComparisonColumnId);
			$tr.append($td_ComparisonType);
			$tr.append($td_ComparisonValue);
			$tr.append($td_ConnectorType);
			$tr.append($td_DeleteButton);

			//添加TR到TABLE
			tb1.append($tr);
		}
	}else{
		alert_error('获取数据出错，条件为空')
	}
}

/**
 * 用户选中常用查询条件【报表】
 * @param index
 * @param row
 * @returns
 */
function SelectQueryInfoOnDblClickRowR(index,row){
	if(isNotNull(row.queryinfo)){
		
		//清空当前查询条件
		$('#QueryTable tbody').empty();
		
		//获取常用查询条件中的条件
		var queryinfoJSON = JSON.parse(row.queryinfo);
		for(var y=0;y<queryinfoJSON.length;y++){

			var ComparisonColumnIds = $("#ComparisonColumnIdsId").val();

			//获取Table
			var tb1 = $("#QueryTable");  
			var $tr = $("<tr></tr>");// 创建tr

			//添加列名
			var tdColumnId;
			var $td_ComparisonColumnId = $("<td></td>"); //ComparisonColumnId td
			var ComparisonColumnIdHTML = '	<select name="ComparisonColumnId" class="form-control form-control-sm" onchange="ComparisonColumnIdOnChange(this);">';
			var columnid = ComparisonColumnIds.split(",");
			for (var i = 0; i < columnid.length; i++) {
				var col = columnid[i].split("☆");

				if(queryinfoJSON[y].ComparisonColumnId == col[0]){
					ComparisonColumnIdHTML = ComparisonColumnIdHTML + '		<option value="'+col[0]+'" selected = "selected">'+col[1]+'</option>';
				}else{
					ComparisonColumnIdHTML = ComparisonColumnIdHTML + '		<option value="'+col[0]+'">'+col[1]+'</option>';
				}

				//设置列名成员变量，查询内容字段生成控件使用
				if(i==0){
					tdColumnId = col[0];
				}
			}
			ComparisonColumnIdHTML = ComparisonColumnIdHTML + '	</select>';
			$td_ComparisonColumnId.html(ComparisonColumnIdHTML); //html把序号放到了第一个td里面


			//添加比较符
			var $td_ComparisonType = $("<td></td>"); //ComparisonType td
			var ComparisonTypeHTML = '	<select name="ComparisonType" class="form-control form-control-sm">';

			if(queryinfoJSON[y].ComparisonType=='1'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="1" selected="selected">等于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="1">等于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='2'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="2" selected="selected">包含</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="2">包含</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='3'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="3" selected="selected">大于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="3">大于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='4'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="4" selected="selected">大于等于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="4">大于等于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='5'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="5" selected="selected">小于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="5">小于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='6'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="6" selected="selected">小于等于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="6">小于等于</option>';
			}
			if(queryinfoJSON[y].ComparisonType=='7'){
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="7" selected="selected">不等于</option>';
			}else{
				ComparisonTypeHTML = ComparisonTypeHTML + '		<option value="7">不等于</option>';
			}
			ComparisonTypeHTML = ComparisonTypeHTML + '	</select>';
			$td_ComparisonType.html(ComparisonTypeHTML);//html把序号放到了第一个td里面

			//添加查询内容字段[根据查询字段的类型创建 查询内容字段类型]
			var $td_ComparisonValue = $("<td></td>"); //ComparisonValue td
			var ComparisonValueHTML = CreateComparisonValueHTMLR(queryinfoJSON[y].ComparisonColumnId,queryinfoJSON[y].ComparisonValue);
			$td_ComparisonValue.html(ComparisonValueHTML);//html把序号放到了第一个td里面

			//添加连接符
			var $td_ConnectorType = $("<td></td>"); //ComparisonValue td
			var ConnectorTypeHTML = '	<select name="ConnectorType" class="form-control form-control-sm">';
			if(queryinfoJSON[y].ConnectorType=='0'){
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="0" selected="selected">&nbsp;</option>';
			}else{
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="0">&nbsp;</option>';
			}
			if(queryinfoJSON[y].ConnectorType=='1'){
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="1" selected="selected">并且</option>';
			}else{
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="1">并且</option>';
			}
			if(queryinfoJSON[y].ConnectorType=='2'){
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="2" selected="selected">或者</option>';
			}else{
				ConnectorTypeHTML = ConnectorTypeHTML + '		<option value="2">或者</option>';
			}
			ConnectorTypeHTML = ConnectorTypeHTML + '	</select>';
			$td_ConnectorType.html(ConnectorTypeHTML);//html把序号放到了第一个td里面

			//添加删除按键
			var $td_DeleteButton = $('<td><button type="button" class="btn btn-light" onclick="DeleteQueryRom(this)"><i class="fa fa-trash"></i></button></td>');

			//添加TD到TR
			$tr.append($td_ComparisonColumnId);
			$tr.append($td_ComparisonType);
			$tr.append($td_ComparisonValue);
			$tr.append($td_ConnectorType);
			$tr.append($td_DeleteButton);

			//添加TR到TABLE
			tb1.append($tr);
		}
	}else{
		alert_error('获取数据出错，条件为空')
	}
}

/**
 * 删除常用查询条件
 * @param temp
 * @returns
 */
function DeleteQueryInfo(){
	var zid = getTableColumn('QueryInfoList','zid');
	if(isNull(zid)){
		alert_error("请选择要删除的常用查询条件");
	}else{
		$.messager.confirm('信息提示','您确定要删除该查询条件吗？',function(r){
			if (r){
				openLoading();
				$.ajax({
					type : "get",
					url : 'deleteQueryInfo',
					data:{zid:zid},
					success : function(data) {
						if(data.code=='SUCCESS'){
							//清空当前查询列表
							$('#QueryTable tbody').empty();
							//刷新常用查询条件列表
							RefreshQueryInfoList();
						}else{
							alertErrorMessager(''+data.msg);
						}
					},
					error: function (data) {
						alertErrorMessager('ajax错误：'+JSON.stringify(data));
					},
					complete: function () {
						//执行完成
						closedLoading();
					}
				});

			}
		});
	}

}


/**
 * 删除常用查询条件【报表】
 * @param temp
 * @returns
 */
function DeleteQueryInfoR(){
	var zid = getTableColumn('QueryInfoList','zid');
	if(isNull(zid)){
		alert_error("请选择要删除的常用查询条件");
	}else{
		$.messager.confirm('信息提示','您确定要删除该查询条件吗？',function(r){
			if (r){
				openLoading();
				$.ajax({
					type : "get",
					url : 'deleteQueryInfoR',
					data:{zid:zid},
					success : function(data) {
						if(data.code=='SUCCESS'){
							//清空当前查询列表
							$('#QueryTable tbody').empty();
							//刷新常用查询条件列表
							RefreshQueryInfoListR();
						}else{
							alertErrorMessager(''+data.msg);
						}
					},
					error: function (data) {
						alertErrorMessager('ajax错误：'+JSON.stringify(data));
					},
					complete: function () {
						//执行完成
						closedLoading();
					}
				});

			}
		});
	}

}
/**
 * 查询常用查询条件列表
 * @returns
 */
function RefreshQueryInfoList(){
	var tableid = $("#tableId_id").val();
	openLoading();
	$.ajax({
		type : "get",
		url : 'getQueryInfoList',
		data:{tableid:tableid},
		success : function(data) {
			closedLoading();
			if(data.code=='SUCCESS'){
				//刷新列表数据
				$("#QueryInfoList").datagrid('loadData', { total: 0, rows: data.data});
			}else{
				alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			closedLoading();
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		}
	});
}
/**
 * 查询常用查询条件列表[报表]
 * @returns
 */
function RefreshQueryInfoListR(){
	var reportid = $("#tableId_id").val();
	openLoading();
	$.ajax({
		type : "get",
		url : 'getQueryInfoListR',
		data:{reportid:reportid},
		success : function(data) {
			closedLoading();
			if(data.code=='SUCCESS'){
				//刷新列表数据
				$("#QueryInfoList").datagrid('loadData', { total: 0, rows: data.data});
			}else{
				alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			closedLoading();
			alertErrorMessager('ajax错误：'+JSON.stringify(data));
		}
	});
}

/******************查询域**********************************************************/



